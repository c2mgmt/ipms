﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using iPMS.Utility;
using OfficeOpenXml;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class PMSReportResultController : BaseController
    {
        // GET: PMSReportResult
        private String getRound = GetQuarter();
        private String getMonth = GetMonth();
        private static String GetQuarter() //แสดงค่าไตรมาส ณ ปัจจุบัน
        {
            DateTime date = DateTime.Now;
            if (date.Month >= 4 && date.Month <= 6)
                return "1";
            else if (date.Month >= 7 && date.Month <= 9)
                return "2";
            else if (date.Month >= 10 && date.Month <= 12)
                return "3";
            else
                return "4";
        }
        private static String GetMonth() //แสดงค่าเดือน ณ เดือนที่แล้ว
        {
            DateTime date = DateTime.Now;
            return (date.Month - 1).ToString();
        }
        public ActionResult Index()
        {
            return View();
        }


        // หน้าประเมิน ผชก.
        public ActionResult Assistant()
        {
            ViewBag.IsEdit = true;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            // เช็คสิทธิ์ admin
            if (!CheckPMSPermission(profileData.PMSPermissionCode, 3))// สิทธิ์ระดับ 3 ผู้มีสิทธิ์ที่ กบล กำหนด สามารถใช้ฟังก์ชันได้ทั้งหมด
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "0"; // ผชก.
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟก.2 (ผชก.(ก2))";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("Assistant", viewModel);
        }

        // หน้าของ ผชก.
        public ActionResult ViewAssistant()
        {
            ViewBag.IsEdit = false;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            // เช็คสิทธิ์ admin
            bool executive = CheckPermissionExecutive(profileData.PositionDescShort);
            if (!CheckPMSPermission(profileData.PMSPermissionCode, 3) // สิทธิ์ระดับ 3 ผู้มีสิทธิ์ที่ กบล กำหนด สามารถใช้ฟังก์ชันได้ทั้งหมด
                && !executive) // ผู้บริหาร ชก. รก. อก. รฝ. อฝ. ผชก.
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "0"; // ผชก.
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟก.2 (ผชก.(ก2))";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("ViewAssistant", viewModel);
        }
        // กฟจ. CEO
        public ActionResult viewCEO()
        {
            ViewBag.IsEdit = true;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "2"; // กฟจ.CEO
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            if (profileData.BaCode == "H000")
            {
                (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            }
            else
            {
                String TRSG = profileData.TRSG;
                (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection_By_TRSG(Year, PEAGroup, NameTimesAYear, Month,TRSG);
            }
 
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟจ.CEO";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("viewCEO", viewModel);
        }
        // หน้าประเมิน กฟจ. ceo
        public ActionResult EvaluateCEO()
        {
            ViewBag.IsEdit = true;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            // เช็คสิทธิ์ admin
            if (!CheckPMSPermission(profileData.PMSPermissionCode, 3))// สิทธิ์ระดับ 3 ผู้มีสิทธิ์ที่ กบล กำหนด สามารถใช้ฟังก์ชันได้ทั้งหมด
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "2"; // กฟจ.ceo
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟจ.CEO";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("EvaluateCEO", viewModel);
        }

        public ActionResult viewSizeLM()
        {
            ViewBag.IsEdit = true;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "3"; // กฟส.Size LM
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            if (profileData.BaCode == "H000")
            {
                (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            }
            else
            {
                String TRSG = profileData.TRSG;
                (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection_By_TRSG(Year, PEAGroup, NameTimesAYear, Month, TRSG);
            }

            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟส. Size L,M";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("viewSizeLM", viewModel);
        }
        // หน้าประเมิน กฟจ. LM
        public ActionResult EvaluateLM()
        {
            ViewBag.IsEdit = true;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            // เช็คสิทธิ์ admin
            if (!CheckPMSPermission(profileData.PMSPermissionCode, 3))// สิทธิ์ระดับ 3 ผู้มีสิทธิ์ที่ กบล กำหนด สามารถใช้ฟังก์ชันได้ทั้งหมด
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "3"; // กฟส L,M
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟส. Size L,M";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("EvaluateLM", viewModel);
        }

        public ActionResult viewSizeS()
        {
            ViewBag.IsEdit = true;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "4"; // กฟส.Size S
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            if (profileData.BaCode == "H000")
            {
                (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            }
            else
            {
                String TRSG = profileData.TRSG;
                (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection_By_TRSG(Year, PEAGroup, NameTimesAYear, Month, TRSG);
            }

            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟส. Size S";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("viewSizeS", viewModel);
        }
        // หน้าประเมิน กฟจ. S
        public ActionResult EvaluateS()
        {
            ViewBag.IsEdit = true;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            // เช็คสิทธิ์ admin
            if (!CheckPMSPermission(profileData.PMSPermissionCode, 3))// สิทธิ์ระดับ 3 ผู้มีสิทธิ์ที่ กบล กำหนด สามารถใช้ฟังก์ชันได้ทั้งหมด
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "4"; // กฟส S
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟส. Size S";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("EvaluateS", viewModel);
        }

        public ActionResult viewSizeXS()
        {
            ViewBag.IsEdit = true;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "5"; // กฟส.Size XS
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            if (profileData.BaCode == "H000")
            {
                (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            }
            else
            {
                String TRSG = profileData.TRSG;
                (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection_By_TRSG(Year, PEAGroup, NameTimesAYear, Month, TRSG);
            }

            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟส. Size XS";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("viewSizeXS", viewModel);
        }
        // หน้าประเมิน กฟจ. XS
        public ActionResult EvaluateXS()
        {
            ViewBag.IsEdit = true;
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            // เช็คสิทธิ์ admin
            if (!CheckPMSPermission(profileData.PMSPermissionCode, 3))// สิทธิ์ระดับ 3 ผู้มีสิทธิ์ที่ กบล กำหนด สามารถใช้ฟังก์ชันได้ทั้งหมด
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String PEAGroup = "5"; // กฟส XS
            String NameTimesAYear = "1"; // ประเมินทุกเดือน
            String Month = getMonth; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewPMSReportResult viewModel = new ViewPMSReportResult();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetSectionYear().ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetTimesAYear().ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Month);
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["Title_Page"] = "PMS กฟส. Size XS";
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Month;
            return View("EvaluateXS", viewModel);
        }

        // CRUD Assistant
        public PMSUpdateAssistantModel SaveFilePMS(PMSUpdateAssistantModel model,String Stage)
        {
            string uniquefile = null;
            String txtDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            if (Stage == "1") // ผู้พัฒนา และ ผู้ดูแลระบบ อัปโหลดไฟล์ รูปแบบการรายงาน อย่างเดียว
            {
                string ext = Path.GetExtension(model.PostedFileCSD.FileName);
                uniquefile = "CSD_" + txtDateTime + "_" + Guid.NewGuid() + ext;
                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["PathFilePMS"]), uniquefile);
                model.PostedFileCSD.SaveAs(path);
                model.FileName_CSD = uniquefile;
            }
            else if(Stage == "2") // อัปโหลดไฟล์ Score
            {
                string ext = Path.GetExtension(model.PostedFileScore.FileName);
                uniquefile = "Score_" + txtDateTime + "_" + Guid.NewGuid() + ext;
                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["PathFilePMS"]), uniquefile);
                model.PostedFileScore.SaveAs(path);
                model.FileName_Score = uniquefile;
            }
            else if(Stage == "3") // อัปโหลดไฟล์ Predict
            {
                string ext = Path.GetExtension(model.PostedFilePredict.FileName);
                uniquefile = "Predict_" + txtDateTime + "_" + Guid.NewGuid() + ext;
                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["PathFilePMS"]), uniquefile);
                model.PostedFilePredict.SaveAs(path);
                model.FileName_Predict = uniquefile;
            }
            return model;
        }
        [HttpPost]
        public ActionResult UpdateAssistant(PMSUpdateAssistantModel model)
        {
            String Status = "", Message = "";
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                }
                // เช็ค session expire
                //PredictLevels
                //if (model.Levels )

                decimal value;
                if (Decimal.TryParse(model.Levels, out value))// It's a decimal
                {
                    if (value >= 1 && value <= 5)
                    {
                        model.Levels = value.ToString();
                        Status = "1";
                    }
                    else
                    {
                        Status = "0";
                        Message = "กรุณาใส่ระดับให้ถูกต้อง ระดับจะต้องอยู่ระหว่าง 1-5";
                    }
                }
                else// No it's not.
                {
                    if (model.Levels == "n/a" || model.Levels == "N/A")
                    {
                        model.Levels = "N/A";
                        Status = "1";
                    }
                    else
                    {
                        Status = "0";
                        Message = "กรุณาใส่ระดับให้อยู่ในรูปแบบตัวเลข";
                    }
                }
                if (Status == "0")
                {
                    var myDataEr = new
                    {
                        Status = Status,
                        Message = Message
                    };
                    return Json(myDataEr);
                }
                decimal value2;
                if (Decimal.TryParse(model.PredictLevels, out value2))// It's a decimal
                {
                    if (value2 >= 1 && value2 <= 5)
                    {
                        model.PredictLevels = value2.ToString();
                        Status = "1";
                    }
                    else
                    {
                        Status = "0";
                        Message = "กรุณาใส่ระดับให้ถูกต้อง ระดับจะต้องอยู่ระหว่าง 1-5";
                    }
                }
                else// No it's not.
                {
                    if (model.PredictLevels == "n/a" || model.PredictLevels == "N/A")
                    {
                        model.PredictLevels = "N/A";
                        Status = "1";
                    }
                    else
                    {
                        Status = "0";
                        Message = "กรุณาใส่ระดับให้อยู่ในรูปแบบตัวเลข";
                    }
                }
                if (Status == "0")
                {
                    var myDataEr = new
                    {
                        Status = Status,
                        Message = Message
                    };
                    return Json(myDataEr);
                }

                List<PMSRawDataSectionModel> ModelLists;
                (ModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetRawDataSection_ALL_ByID(model.ID);

                // สิทธิ์ ผู้พัฒนา และ สิทธิ์ผู้ดูแลระบบ
                if (Mapper.MapStringToInteger(profileData.PMSPermissionCode) <= 10 &&
                    Mapper.MapStringToInteger(profileData.PMSPermissionCode) >= 0)
                {
                    if (model.PostedFileCSD == null && model.isCSD == "0") // ไม่มีไฟล์แนบเข้ามา
                    {
                        Status = "0";
                        Message = "กรุณาแนบไฟล์ รูปแบบการายงาน";
                    }
                    else // มีไฟล์แนบ
                    {
                        int countcheckData = checkUpdatePMS(model);
                        // countcheckData = 0 ลงคะแนน แทน คณะกรรม และ ผู้ให้คะแนน
                        // countcheckData = 4 ไม่ลงคะแนนให้ คณะกรรม และ ผู้ให้คะแนน
                        if (countcheckData == 0)
                        {
                            model.UpdateBy = profileData.Username;
                            model.UpdateName = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;
                            model.UpdateBy_CSD = profileData.Username;
                            model.UpdateName_CSD = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;
                            if (model.PostedFileCSD != null)
                            {
                                model = SaveFilePMS(model, "1");
                            }
                            else
                            {
                                if (model.checkCSD == "0") // ไม่เก็บไฟล์
                                {
                                    model.FileName_CSD = "";
                                }
                                else // เก็บไฟล์
                                {
                                    model.FileName_CSD = ModelLists[0].PEA[0].FileName_CSD;
                                }
                            }
                            if (model.PostedFileScore != null)
                            {
                                model = SaveFilePMS(model, "2");
                            }
                            else
                            {
                                if (model.checkScore == "0") // ไม่เก็บไฟล์
                                {
                                    model.FileName_Score = "";
                                }
                                else // เก็บไฟล์
                                {
                                    model.FileName_Score = ModelLists[0].PEA[0].FileName_Score;
                                }
                            }
                            if (model.PostedFilePredict != null)
                            {
                                model = SaveFilePMS(model, "3");
                            }
                            else
                            {
                                if (model.checkPredict == "0")
                                {
                                    model.FileName_Predict = "";
                                }
                                else // เก็บไฟล์
                                {
                                    model.FileName_Predict = ModelLists[0].PEA[0].FileName_Predict;
                                }
                            }
                            (Status, Message) = Repository.StoredProcedure.SpPMSReportResult.zPMS_spUpdateResult_Assistant_CSD_ALL(model);
                        }
                        else if (countcheckData == 4) // ผู้ดูแลระบบ อัปโหลดไฟล์ รูปแบบการรายงาน อย่างเดียว
                        {
                            model.UpdateBy_CSD = profileData.Username;
                            model.UpdateName_CSD = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;
                            if (model.PostedFileCSD != null)
                            {
                                model.UpdateBy_CSD = profileData.Username;
                                model.UpdateName_CSD = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;
                                model = SaveFilePMS(model, "1");
                            }
                            else
                            {
                                if (model.checkCSD == "0") // ไม่เก็บไฟล์
                                {
                                    model.FileName_CSD = "";
                                }
                                else // เก็บไฟล์
                                {
                                    model.FileName_CSD = ModelLists[0].PEA[0].FileName_CSD;
                                }
                            }
                            (Status, Message) = Repository.StoredProcedure.SpPMSReportResult.zPMS_spUpdateResult_Assistant_CSD_ALL(model);
                        }
                        else
                        {
                            Status = "0";
                            Message = "กรุณาใส่ข้อมูล ผล,ระดับ ของรายงานผลและคาดการณ์ ให้ครบถ้วน";
                        }

                        if(Status == "1") // 
                        {
                            String Log = "", Code = "";
                            List<String> Key = new List<string>();
                            (Key,Code,Log) = Repository.StoredProcedure.SpPMSReportResult.zPMS_KeyForUpdate_CSD_FileName_1_Get(model.ID);
                            for(int i = 0;i< Key.Count; i++)
                            {
                                (Code,Log) = Repository.StoredProcedure.SpPMSReportResult.zPMS_KeyForUpdate_CSD_FileName_2_Update(Key[i],model.FileName_CSD);
                                if(Code == "0")
                                {
                                    Status = "0";
                                    Message = Log;
                                    break;
                                }
                            }
                        }
                    }
                }
                // สิทธิ์ คณะกรรมการ 
                else if(Mapper.MapStringToInteger(profileData.PMSPermissionCode) == 20)
                {
                    int countcheckData = checkUpdatePMS(model);
                    if (countcheckData == 0)
                    {
                        model.UpdateBy = profileData.Username;
                        model.UpdateName = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;
                        if (model.PostedFileScore != null)
                        {
                            model = SaveFilePMS(model, "2");
                        }
                        else
                        {
                            if (model.checkScore == "0") // ไม่เก็บไฟล์
                            {
                                model.FileName_Score = "";
                            }
                            else // เก็บไฟล์
                            {
                                model.FileName_Score = ModelLists[0].PEA[0].FileName_Score;
                            }
                        }
                        if (model.PostedFilePredict != null)
                        {
                            model = SaveFilePMS(model, "3");
                        }
                        else
                        {
                            if (model.checkPredict == "0")
                            {
                                model.FileName_Predict = "";
                            }
                            else // เก็บไฟล์
                            {
                                model.FileName_Predict = ModelLists[0].PEA[0].FileName_Predict;
                            }
                        }
                        (Status, Message) = Repository.StoredProcedure.SpPMSReportResult.zPMS_spUpdateResult_Assistant_Assessor(model);
                    }
                    else
                    {
                        Status = "0";
                        Message = "กรุณาใส่ข้อมูล ผล,ระดับ ของรายงานผลและคาดการณ์ ให้ครบถ้วน";
                    }
                }
                // สิทธิ์ ผู้ให้คะแนน
                else if (Mapper.MapStringToInteger(profileData.PMSPermissionCode) == 30)
                {
                    String SelfCostCenter = "", CostCenterCode = "";
                    CostCenterCode = ModelLists[0].CostCenterCode.Substring(0, 7);
                    SelfCostCenter = profileData.CostCenterCode.Substring(0, 7);
                    if (SelfCostCenter == CostCenterCode)
                    {
                        int countcheckData = checkUpdatePMS(model);
                        if (countcheckData == 0)
                        {
                            model.UpdateBy = profileData.Username;
                            model.UpdateName = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;
                            if (model.PostedFileScore != null)
                            {
                                model = SaveFilePMS(model, "2");
                            }
                            else
                            {
                                if (model.checkScore == "0") // ไม่เก็บไฟล์
                                {
                                    model.FileName_Score = "";
                                }
                                else // เก็บไฟล์
                                {
                                    model.FileName_Score = ModelLists[0].PEA[0].FileName_Score;
                                }
                            }
                            if (model.PostedFilePredict != null)
                            {
                                model = SaveFilePMS(model, "3");
                            }
                            else
                            {
                                if (model.checkPredict == "0")
                                {
                                    model.FileName_Predict = "";
                                }
                                else // เก็บไฟล์
                                {
                                    model.FileName_Predict = ModelLists[0].PEA[0].FileName_Predict;
                                }
                            }
                            (Status, Message) = Repository.StoredProcedure.SpPMSReportResult.zPMS_spUpdateResult_Assistant_Assessor(model);
                        }
                        else
                        {
                            Status = "0";
                            Message = "กรุณาใส่ข้อมูล ผล,ระดับ ของรายงานผลและคาดการณ์ ให้ครบถ้วน";
                        }
                    }
                    else
                    {
                        Status = "0";
                        Message = "คุณไม่สามารถประเมินได้ เนื่องจากคุณไม่ได้รับผิดชอบในหัวข้อดังกล่าว โปรดติดต่อผู้ดูแล";
                    }
                }
                else
                {
                    Status = "0";
                    Message = "กรุณาเช็คสิทธิ์การใช้งาน โปรดติดต่อผู้ดูแล";
                }
                var myData2 = new
                {
                    Status = Status, // Get สำเร็จ
                    //Data = ModelLists,
                    Message = Message
                };
                return Json(myData2);
            }
            catch (Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }

        //ค้นหาข้อมูล
        [HttpPost]
        public ActionResult FilterYear(
            String Year,
            String PEAGroup,
            String NameTimesAYear,
            String Round)
        {
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                List<PMSRawDataSectionModel> ModelLists;
                String Status = "";
                String Message = "";

                if (PEAGroup != "0" && profileData.BaCode != "H000")
                {
                    String TRSG = profileData.TRSG;
                    (ModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                    zPMS_spGetRawDataSection_By_TRSG(Year, PEAGroup, NameTimesAYear, Round, TRSG);
                }
                else
                {
                    (ModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                    zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Round);
                }

                if (ModelLists.Count == 1 && ModelLists[0].ID == null && Status == "1")
                {
                    var myData3 = new
                    {
                        Status = "2", // Get สำเร็จ แต่หัวข้อกิจกรรมไม่มีในฐานข้อมูล
                        Message = "ไม่พบข้อมูลในฐานข้อมูล"
                    };
                    return Json(myData3);
                }

                var myData2 = new
                {
                    Status = "1", // Get สำเร็จ
                    Data = ModelLists,
                    Message = Message
                };
                return Json(myData2);
            }
            catch (Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }
        [HttpPost]
        public ActionResult ViewPerformanceResults(String ID)
        {
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire
                String Status = "", Message = "";

                List<PMSRawDataSectionModel> ModelLists;
                (ModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetRawDataSection_ALL_ByID(ID);

                var myData2 = new
                {
                    Status = Status,
                    Message = Message,
                    Data = ModelLists
                };
                return Json(myData2);
            }
            catch(Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }
        // view Chart
        [HttpPost]
        public ActionResult ViewOperation(String MastID,String Year,String Month,String PEAGroup)
        {
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire
                String Status = "", Message = "";

                viewChartModel ModelLists;
                (ModelLists, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetResult_viewChart(MastID, Year, Month, PEAGroup);

                if(ModelLists.PEAName.Count == 0)
                {
                    Status = "0";
                    Message = "กรุณาตรวจสอบเกณฑ์วัดผลการดำเนินงาน ระดับการไฟฟ้าดังกล่าวได้รับเกณฑ์การประเมินข้อนี้หรือไม่ หรือโปรดติดต่อผู้ดูแล";
                }

                var myData2 = new
                {
                    Status = Status,
                    Message = Message,
                    Data = ModelLists
                };
                return Json(myData2);
            }
            catch (Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }
        //Download Files
        [HttpPost]
        public ActionResult ExportToExcel(String ID)
        {
            byte[] fileData = null;
            using (var ms = new MemoryStream())
            {
                using (SLDocument sl = new SLDocument())
                {
                    String Status, Message = "";
                    SLStyle TextCenterStyle = new SLStyle();
                    TextCenterStyle.SetVerticalAlignment(DocumentFormat.OpenXml.Spreadsheet.VerticalAlignmentValues.Center);
                    TextCenterStyle.SetHorizontalAlignment(DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);

                    sl.SetCellValue("A1", "ลำดับ");
                    sl.SetCellValue("B1", "ประเภท");
                    sl.SetCellValue("C1", "เกณฑ์วัดผลการดำเนินงาน");
                    sl.SetCellValue("D1", "TRSG");
                    sl.SetCellValue("E1", "การไฟฟ้า");
                    sl.SetCellValue("F1", "ผล");
                    sl.SetCellValue("G1", "ระดับ");

                    List<PMSFilesModel> Data = new List<PMSFilesModel>();

                    (Data, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                        zPMS_spGetDownloadFiles_ByID(ID);

                    String a = Status;
                    if (Data.Count > 0)
                    {
                        int StartColumn = 1;
                        int EndColumn = 7;

                        int ColumnIndex = 1;
                        int ColumnTopicName = 2;
                        int ColumnCri_work = 3;
                        int ColumnTRSG = 4;
                        int ColumnPEAName = 5;
                        int ColumnScore = 6;
                        int ColumnLevel = 7;

                        int row = 2;
                        String CheckPlans = Data[0].Cri_work;
                        int FixRow = 2; // เริ่มต้นข้อมูล 
                        int StartRow = FixRow; // เริ่ม merge
                        int EndRow = FixRow; // จบ merge
                        int IsDuplicate = 0; // สถานะตอนนี้ซ้ำหรือไม่ซ้ำ
                        for (int indexData = 0; indexData < Data.Count; indexData++)
                        {
                            if (indexData == 0)
                            {
                                sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                sl.SetCellValue(row, ColumnTopicName, Data[indexData].TopicName);
                                sl.SetCellValue(row, ColumnCri_work, Data[indexData].Cri_work);
                                sl.SetCellValue(row, ColumnTRSG, Data[indexData].TRSG);
                                sl.SetCellValue(row, ColumnPEAName, Data[indexData].PEAName);
                            }
                            else if (CheckPlans == Data[indexData].Cri_work && indexData == Data.Count - 1)
                            {
                                EndRow = FixRow + 1; // รอบสุดท้าย ไม่ได้ + FixRow

                                sl.MergeWorksheetCells(StartRow, ColumnTopicName, EndRow, ColumnTopicName);
                                sl.MergeWorksheetCells(StartRow, ColumnCri_work, EndRow, ColumnCri_work);
                                sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                sl.SetCellValue(row, ColumnTRSG, Data[indexData].TRSG);
                                sl.SetCellValue(row, ColumnPEAName, Data[indexData].PEAName);

                                IsDuplicate = 0;
                            }
                            else if (CheckPlans == Data[indexData].Cri_work) // ข้อมูลซ้ำ ตั้งแต่ row ที่ 2 ขึนไป 
                            {
                                if (IsDuplicate == 0)
                                {
                                    StartRow = FixRow;
                                    IsDuplicate++;
                                }
                                sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                sl.SetCellValue(row, ColumnTRSG, Data[indexData].TRSG);
                                sl.SetCellValue(row, ColumnPEAName, Data[indexData].PEAName);
                                FixRow++;
                            }
                            else if (CheckPlans != Data[indexData].Cri_work)
                            {
                                CheckPlans = Data[indexData].Cri_work;
                                if (IsDuplicate == 0)
                                {
                                    sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                    sl.SetCellValue(row, ColumnTRSG, Data[indexData].TRSG);
                                    sl.SetCellValue(row, ColumnPEAName, Data[indexData].PEAName);
                                }
                                else
                                {
                                    EndRow = FixRow;

                                    sl.MergeWorksheetCells(StartRow, ColumnTopicName, EndRow, ColumnTopicName);
                                    sl.MergeWorksheetCells(StartRow, ColumnCri_work, EndRow, ColumnCri_work);
                                    sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                    sl.SetCellValue(row, ColumnTRSG, Data[indexData].TRSG);
                                    sl.SetCellValue(row, ColumnPEAName, Data[indexData].PEAName);
                                    IsDuplicate = 0;

                                    StartRow = FixRow + 1; // + กันเผื่อกรณีที่เจอตัวสุดท้ายแผนงานซ้ำกัน
                                }

                                FixRow++;
                            }
                            row++;
                        }
                        // merge all cells from rows 10 through 12, columns 4 through 6
                        // This is basically the cell range D10:F12
                        //SetCellStyle(int StartRowIndex, int StartColumnIndex, int EndRowIndex, int EndColumnIndex, SLStyle Style);
                        sl.SetCellStyle(1, StartColumn, 2 + Data.Count, EndColumn, TextCenterStyle);
                        sl.AutoFitColumn(StartColumn, EndColumn);
                        sl.AutoFitRow(1, 2 + Data.Count);

                        //sl.SetColumnWidth(2, 100);
                        //sl.SetColumnWidth(3, 100);
                        sl.SaveAs(ms);
                    }
                }
                fileData = ms.ToArray();
            }
            return File(fileData, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report.xlsx");
        }

        [HttpPost]
        public ActionResult UploadFiles(UploadFilesModel model)
        {
            // สร้างตัวแปร
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            String Message = "";
            String Status = "";
            String ID = "";
            String Cancel = "";
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                }
                // เช็ค session expire

                // เช็คสิทธิ์ การลงคะแนน
                if(Mapper.MapStringToInteger(profileData.PMSPermissionCode) >= 0 
                    && Mapper.MapStringToInteger(profileData.PMSPermissionCode) <= 20)
                {
                    Status = "1";
                }
                else if(Mapper.MapStringToInteger(profileData.PMSPermissionCode) == 30)
                {
                    String SelfCostCenter = "", CostCenterCode = "", CostCenterName = "";
                    (CostCenterCode, CostCenterName, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.zPMS_spGetCheckCostCenter(model.CheckID);
                    CostCenterCode = CostCenterCode.Substring(0, 7);
                    SelfCostCenter = profileData.CostCenterCode.Substring(0, 7);
                    if(SelfCostCenter == CostCenterCode)
                    {
                        Status = "1";
                    }
                    else
                    {
                        var myData = new
                        {
                            Status = "0", // Error
                            Message = "คุณไม่มีสิทธิ์ในการให้คะแนน หัวข้อนี้เป็นความรับผิดชอบของ " + CostCenterName + " หรือโปรดติดต่อผู้ดูแล",
                        };
                        return Json(myData);
                    }
                }
                else
                {
                    var myData = new
                    {
                        Status = "0", // Error
                        Message = "คุณไม่มีสิทธิ์ในการให้คะแนน โปรดติดต่อผู้ดูแล",
                    };
                    return Json(myData);
                }
                //

                List<String> ListScore = new List<String>();
                List<String> ListLevels = new List<String>();
                String Username = profileData.Username;
                String Name = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;
                int rows = 0;

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(model.PostedFile.InputStream))
                {
                    var worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    rows = 0;
                    //check error
                    for (int row = 2; row < rowCount; row++)
                    {
                        ++rows;
                        String Levels = worksheet.Cells[row, 7].Text;
                        var isNumeric = double.TryParse(Levels, out double d);

                        if (!isNumeric)
                        {
                            if (Levels.ToUpper() == "N/A")
                            {
                                Levels = Levels.ToUpper();
                            }
                            else
                            {
                                var myData = new // แสดงข้อความ เมื่อ กรอกคะแนนไม่ใช่ double และไม่ใช่ n/a
                                {
                                    Status = "0",
                                    Message = "เกิดข้อผิดพลาด กรุณาเช็ค 'คอลัมน์กรอกคะแนน' แถวที่ " + rows + "ให้ถูกต้อง",
                                };
                                return Json(myData);
                            }
                        }
                        else
                        {
                            if (Convert.ToDouble(Levels) > 5 || Convert.ToDouble(Levels) < 1)
                            {
                                var myData = new // ผลลัพธ์ เพื่ออัพเดท ตาราง
                                {
                                    Status = "0",
                                    Message = "เกิดข้อผิดพลาด กรุณาเช็คคะแนน กรุณาใส่คะแนน ระดับตั้งแต่ 1-5 แถวที่ " + rows,
                                };
                                return Json(myData);
                            }
                        }
                    }

                    //update
                    for (int row = 2; row < rowCount; row++)
                    {
                        String valScore = worksheet.Cells[row, 6].Text;
                        String valLevel = worksheet.Cells[row, 7].Text;
                        if(valScore.Length > 8)
                        {
                            valScore = valScore.Substring(0, 8);
                        }
                        if(valLevel.Length > 8)
                        {
                            valLevel = valLevel.Substring(0, 8);
                        }
                        var UploadFilesModel = new UploadFilesModel
                        {
                            ForeignSection = model.CheckID,//worksheet.Cells[row, 2].Text,
                            TRSG = worksheet.Cells[row, 4].Text,
                            Score = valScore,
                            Levels = valLevel,
                            UpdateBy = Username,
                            UpdateName = Name
                        };
                        ListScore.Add(valScore);
                        ListLevels.Add(valLevel);
                        (Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                        zPMS_spUpdateResult_By_UploadFiles(UploadFilesModel);
                        if (Status == "0")
                        {
                            var myData = new // ผลลัพธ์ เพื่ออัพเดท ตาราง
                            {
                                Status = Status,
                                Message = Message,
                            };
                            return Json(myData);
                        }
                    }
                }

                // save file
                string uniquefile = null;
                String txtDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                string ext = Path.GetExtension(model.PostedFile.FileName);
                uniquefile = "E_"+ model.CheckID + "_" + txtDateTime + "_" + Guid.NewGuid() + ext;
                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["PathFileUpload"]), uniquefile);
                model.PostedFile.SaveAs(path);
                // save file

                var myDataE = new // ผลลัพธ์
                {
                    Status = Status,
                    Message = "อัปโหลดคะแนนสำเร็จ จำนวน " + rows + " แถว",
                    ListScore = ListScore,
                    ListLevels = ListLevels
                };
                return Json(myDataE);
                // Save products to the database or process them as needed
                // For simplicity, we'll just pass the list to the view
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                var myData = new
                {
                    Status = "0", // Error
                    Message = ex.Message,
                };
                return Json(myData);
            }
        }

        [HttpPost]
        public ActionResult ReportExportToExcel(String Year, String PEAGroup, String NameTimesAYear, String Round)
        {
            byte[] fileData = null;
            using (var ms = new MemoryStream())
            {
                using (SLDocument sl = new SLDocument())
                {
                    String Status, Message = "";
                    SLStyle TextCenterStyle = new SLStyle();
                    TextCenterStyle.SetVerticalAlignment(DocumentFormat.OpenXml.Spreadsheet.VerticalAlignmentValues.Center);
                    TextCenterStyle.SetHorizontalAlignment(DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);

                    // merge all cells in the cell range A1:A2
                    sl.SetCellValue("A1", "ลำดับ");
                    sl.MergeWorksheetCells("A1", "A2");

                    sl.SetCellValue("B1", "ปี");
                    sl.MergeWorksheetCells("B1", "B2");
                    sl.SetCellValue("C1", "เดือน");
                    sl.MergeWorksheetCells("C1", "C2");

                    sl.SetCellValue("D1", "ประเภทงาน");
                    sl.MergeWorksheetCells("D1", "D2");

                    sl.SetCellValue("E1", "เกณฑ์วัดผลการดำเนินงาน");
                    sl.MergeWorksheetCells("E1", "E2");

                    sl.SetCellValue("F1", "หน่วยวัด");
                    sl.MergeWorksheetCells("F1", "F2");

                    sl.SetCellValue("G1", "ค่าเกณฑ์วัด");
                    sl.MergeWorksheetCells("G1", "K1");
                    sl.SetCellValue("G2", "1");
                    sl.SetCellValue("H2", "2");
                    sl.SetCellValue("I2", "3");
                    sl.SetCellValue("J2", "4");
                    sl.SetCellValue("K2", "5");

                    sl.SetCellValue("L1", "ผู้รายงาน");
                    sl.MergeWorksheetCells("L1", "L2");

                    List<PMSRawDataSectionModel> Data = new List<PMSRawDataSectionModel>();

                    (Data, Status, Message) = Repository.StoredProcedure.SpPMSReportResult.
                        zPMS_spGetRawDataSection(Year, PEAGroup, NameTimesAYear, Round);

                    String a = Status;
                    if (Data.Count > 0)
                    {
                        int CountPEA = Data[0].PEA.Count; // ใช้ในการ merge cell

                        int ColumnIndex = 1;
                        int ColumnYear = 2;
                        int ColumnMonth = 3;
                        int ColumnTopic = 4;
                        int ColumnWork = 5;
                        int ColumnUnit = 6;
                        int ColumnLv1 = 7;
                        int ColumnLv2 = 8;
                        int ColumnLv3 = 9;
                        int ColumnLv4 = 10;
                        int ColumnLv5 = 11;
                        int ColumnRes = 12;

                        int startScore = 13;
                        int startLevels = 14;

                        int moveScore = startScore;
                        int moveLevels = startLevels;

                        for (int i = 0; i < Data[0].PEA.Count; i++)
                        {
                            sl.SetCellValue(1, moveScore, Data[0].PEA[i].PEA_OfficeNAME); // RowIndex, ColumnIndex, Data)
                            sl.MergeWorksheetCells(1, moveScore, 1, moveLevels); // StartRowIndex, StartColumnIndex, EndRowIndex, EndColumnIndex
                            
                            sl.SetCellValue(2, moveScore, "ผล"); // RowIndex, ColumnIndex, Data)
                            sl.SetCellValue(2, moveLevels, "ระดับ"); // RowIndex, ColumnIndex, Data)

                            moveScore += 2;
                            moveLevels += 2;
                        }

                        int row = 3;
                        int column = ColumnRes;

                        int FixRow = 3; // เริ่มต้นข้อมูล 
                        int StartRow = FixRow; // เริ่ม merge
                        int EndRow = FixRow; // จบ merge

                        for (int indexData = 0; indexData < Data.Count; indexData++)
                        {
                            moveScore = startScore;
                            moveLevels = startLevels;
                            sl.SetCellValue(row, ColumnIndex, indexData + 1);
                            sl.SetCellValue(row, ColumnYear, Data[indexData].Year);
                            sl.SetCellValue(row, ColumnMonth, Data[indexData].Month);
                            sl.SetCellValue(row, ColumnTopic, Data[indexData].TopicName);
                            sl.SetCellValue(row, ColumnWork, Data[indexData].Cri_work);
                            sl.SetCellValue(row, ColumnUnit, Data[indexData].Cri_Unit);
                            if(Data[indexData].IsAllTheSame == "1")
                            {
                                sl.SetCellValue(row, ColumnLv1, Data[indexData].Cri_lv1);
                                sl.MergeWorksheetCells(row, ColumnLv1, row, ColumnLv5); // StartRowIndex, StartColumnIndex, EndRowIndex, EndColumnIndex
                            }
                            else
                            {
                                sl.SetCellValue(row, ColumnLv1, Data[indexData].Cri_lv1);
                                sl.SetCellValue(row, ColumnLv2, Data[indexData].Cri_lv2);
                                sl.SetCellValue(row, ColumnLv3, Data[indexData].Cri_lv3);
                                sl.SetCellValue(row, ColumnLv4, Data[indexData].Cri_lv4);
                                sl.SetCellValue(row, ColumnLv5, Data[indexData].Cri_lv5);
                            }
                            sl.SetCellValue(row, ColumnRes, Data[indexData].Cri_res);
                            // คะแนนแต่ละการไฟฟ้า
                            for (int indexPEA = 0; indexPEA < Data[0].PEA.Count; indexPEA++)
                            {
                                sl.SetCellValue(row, moveScore, Data[indexData].PEA[indexPEA].Score);
                                sl.SetCellValue(row, moveLevels, Data[indexData].PEA[indexPEA].Levels);
                                moveScore += 2;
                                moveLevels += 2;
                            }
                            row++;
                        }

                        // merge all cells from rows 10 through 12, columns 4 through 6
                        // This is basically the cell range D10:F12

                        sl.SetCellStyle(1, 1, 2 + Data.Count, column + (Data[0].PEA.Count*2), TextCenterStyle);
                        sl.AutoFitColumn(1, column + (Data[0].PEA.Count*2));
                        sl.AutoFitRow(1, 2 + Data.Count);
                        //sl.SetColumnWidth(2, 100);
                        //sl.SetColumnWidth(3, 100);
                        sl.SaveAs(ms);
                    }
                }
                fileData = ms.ToArray();
            }
            return File(fileData, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report.xlsx");
        }
    }
}