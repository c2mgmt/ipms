﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class PerformanceAnalysisController : BaseController
    {
        // GET: PerformanceAnalysis
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            String ID = null; 
            String TypeID = "3"; // แผนปฏิบัติ
            String CabinetID = "2"; // วิเคราะห์ผลดำเนินการ
            String TopicID = null; 
            String Year = null;
            String PEAID = null; 
            String TimesAYearID = "2"; // ทุกไตรมาส 
            String Quarter = null; // ไตรมาสเท่าไหร่
            String Status = "";
            String Message = "";
            ViewPerformanceAnalysisModel viewModel = new ViewPerformanceAnalysisModel();

            viewModel.YearsModelLists = Repository.StoredProcedure.SpPerformanceAnalysis.SpGetDistinctYearAnalysis(CabinetID).ToList();
            //(viewModel.ArchiveModelLists, Status, Message) = Repository.StoredProcedure.SpPerformanceAnalysis
            //    .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, PEAID, TimesAYearID, Quarter);

            if (viewModel.YearsModelLists.Count == 0)
            {
                Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
                ViewData["Year"] = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
            }
            else
            {
                Year = viewModel.YearsModelLists[0].Year;
                ViewData["Year"] = viewModel.YearsModelLists[0].Year;
            }

            (viewModel.OverviewArchiveModelLists, Status, Message) = Repository.StoredProcedure.SpPerformanceAnalysis
                .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, "1", TimesAYearID, Quarter);

            (viewModel.PEAArchiveModelLists, Status, Message) = Repository.StoredProcedure.SpPerformanceAnalysis
                .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, "2", TimesAYearID, Quarter);

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult FilterYear(String Year)
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
            }
            // เช็ค session expire

            String ID = null;
            String TypeID = "3"; // แผนปฏิบัติ
            String CabinetID = "2"; // วิเคราะห์ผลดำเนินการ
            String TopicID = null;
            //String Year = null; ส่งค่ามาจาก frontend
            String PEAID = null;
            String TimesAYearID = "2"; // ทุกไตรมาส 
            String Quarter = null; // ไตรมาสเท่าไหร่
            String Status = "";
            String Message = "";
            List<RawDataArchiveModel> model;


            (model, Status, Message) = Repository.StoredProcedure.SpPerformanceAnalysis
                .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, PEAID, TimesAYearID, Quarter);

            if (model.Count == 1 && model[0].ID == null && Status == "1")
            {
                var myData3 = new
                {
                    Status = "2", // Get สำเร็จ แต่หัวข้อกิจกรรมไม่มีในฐานข้อมูล
                    Message = "ไม่พบข้อมูลในฐานข้อมูล"
                };
                return Json(myData3);
            }

            var myData2 = new
            {
                Status = "1", // Get สำเร็จ
                Data = model,
                Message = Message
            };
            return Json(myData2);
        }
    }
}