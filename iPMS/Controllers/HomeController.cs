﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            //Notification
            //String Status, Message;
            //IList<RawDataSectionModel> RawDataSectionModelLists;
            //(RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpBase.
            //    SpGetNotification(profileData.BaCode);
            //ViewData["Notification"] = RawDataSectionModelLists;
            //
            return View();
        }
        [HttpPost]
        public ActionResult SignOut()
        {
            try
            {
                //ResetConfig();
                this.Session["UserProfile"] = null;

                var request = HttpContext.Request;
                var baseUrl = $"{request.Url.Scheme}://{request.Url.Authority}{request.ApplicationPath.TrimEnd('/')}/";
                var returnUrl = baseUrl + "Login/Index";
                //string redirectUri = ConfigurationManager.AppSettings.Get("RedirectUri");
                return Redirect($"https://sso2.pea.co.th/realms/pea-users/protocol/openid-connect/logout?redirect_uri={returnUrl}");

                //return RedirectToAction("Index", "Login");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult About()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}