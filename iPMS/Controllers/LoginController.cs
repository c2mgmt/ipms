﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using iPMS.ServiceReference1;
using iPMS.ServiceReference2;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static iPMS.Models.KeyCloak.KEYCLOAK;

namespace iPMS.Controllers
{
    static class StringExtensions
    {
        public static IEnumerable<String> SplitInParts(this String s, Int32 partLength)
        {
            if (s == null)
                throw new ArgumentNullException(nameof(s));
            if (partLength <= 0)
                throw new ArgumentException("Part length has to be positive.", nameof(partLength));

            for (var i = 0; i < s.Length; i += partLength)
                yield return s.Substring(i, Math.Min(partLength, s.Length - i));
        }
    }

    public class LoginController : BaseController
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Authentication(string code)
        {
            var request = HttpContext.Request;
            var baseUrl = $"{request.Url.Scheme}://{request.Url.Authority}{request.ApplicationPath.TrimEnd('/')}/";
            var returnUrl = baseUrl + "Login/Authentication";
            KEYCLOAKServiceHelper Helper = new KEYCLOAKServiceHelper();
            if (string.IsNullOrEmpty(code))
            {
                return Redirect($"https://sso2.pea.co.th/realms/pea-users/protocol/openid-connect/auth?client_id=c2-ipms&redirect_uri={returnUrl}&response_type=code&scope=openid%20profile");
                //return Redirect($"https://sso2.pea.co.th/realms/pea-users/protocol/openid-connect/auth?client_id=c2-ipms&redirect_uri=https://localhost:44369/Login/Authentication&response_type=code&scope=openid%20profile&state=etsxstate");
            }
            else
            {
                String Status = "", Message = "";
                UserInfo userInfo = new UserInfo();

                String Code = Request.QueryString["code"].ToString();
                KeyResponse token = Helper.CallGetToken(code);
                userInfo = Helper.CallGetUserInfo(token.access_token);
                //userInfo = Helper.CallGetKeyRequest(Code);
                (Status, Message) = HrPlatform(userInfo.preferred_username);

                if (Status == "true")
                {
                    return RedirectToAction("Index", "Home");//, new { id = 10 });
                }
                else
                {
                    MessageManager.SetErrorMessage(Message);
                    return View();
                }
            }
        }

        // Login Version Old
        //[HttpPost]
        //public ActionResult Authentication(LoginModel viewModel)
        //{

        //    ServiceResponseOfLoginResult loginResult = IdmManager.GetLoginResult(viewModel.username, viewModel.password);
        //    EmployeeProfile employeeProfile = IdmManager.GetByEmployeeNo(viewModel.username);
        //    //loginResult.ResultObject.Result;
        //    //loginResult.ResponseMsg;
        //    //ViewBag.Id = viewModel.username;
        //    //ViewBag.Name = viewModel.password;

        //    var macAddr =
        //    (
        //        from nic in NetworkInterface.GetAllNetworkInterfaces()
        //        where nic.OperationalStatus == OperationalStatus.Up
        //        select nic.GetPhysicalAddress().ToString()
        //    ).FirstOrDefault();

        //    var parts = macAddr.SplitInParts(2);
        //    var mac = String.Join("-", parts);

        //    String getMac = Repository.StoredProcedure.SpBase.SpGetLazy(mac);

        //    //if (viewModel.password == "Nmkpt5*" && employeeProfile != null)
        //    //{
        //    //    //var options = new RestClientOptions("https://servicehub.pea.co.th:8443/")
        //    //    //{
        //    //    //    RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
        //    //    //};
        //    //    //var client = new RestClient(options);
        //    //    //RestRequest request = new RestRequest("get-employee-detail", Method.Post);
        //    //    ////request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

        //    //    //request.AddParameter("apikey", "0qp4gLzoCujysa9ifTSc3s586Akxsm8n", ParameterType.QueryString);
        //    //    //request.AddParameter("emp_id", viewModel.username, ParameterType.QueryString);

        //    //    //var response = client.Post(request);
        //    //    //Console.WriteLine(response.StatusCode);
        //    //    //dynamic json = JsonConvert.DeserializeObject(response.Content);

        //    //    //string TRSG_ = json["data"]["dataDetail"][0].pea_code;


        //    //    // เซตค่า Session
        //    //    var profileData = new UserProfileSessionData
        //    //    {
        //    //        Username = employeeProfile.Username,
        //    //        EmployeeId = employeeProfile.EmployeeId,
        //    //        TitleFullName = employeeProfile.TitleFullName,
        //    //        FirstName = employeeProfile.FirstName,
        //    //        LastName = employeeProfile.LastName,
        //    //        GroupId = employeeProfile.GroupId,
        //    //        Group = employeeProfile.Group,
        //    //        GenderCode = employeeProfile.GenderCode,
        //    //        Gender = employeeProfile.Gender,
        //    //        PositionCode = employeeProfile.PositionCode,
        //    //        PositionDescShort = employeeProfile.PositionDescShort,
        //    //        Position = employeeProfile.Position,
        //    //        LevelCode = employeeProfile.LevelCode,
        //    //        LevelDesc = employeeProfile.LevelDesc,
        //    //        NewOrganizationalCode = employeeProfile.NewOrganizationalCode,
        //    //        DepartmentShortName = employeeProfile.DepartmentShortName,
        //    //        DepartmentFullName = employeeProfile.DepartmentFullName,
        //    //        DepartmentSap = employeeProfile.DepartmentSap,
        //    //        DepartmentShort = employeeProfile.DepartmentShort,
        //    //        Department = employeeProfile.Department,
        //    //        RegionCode = employeeProfile.RegionCode,
        //    //        Region = employeeProfile.Region,
        //    //        SubRegionCode = employeeProfile.SubRegionCode,
        //    //        SubRegion = employeeProfile.SubRegion,
        //    //        CostCenterCode = employeeProfile.CostCenterCode,
        //    //        CostCenterName = employeeProfile.CostCenterName,
        //    //        Peacode = employeeProfile.Peacode,
        //    //        BaCode = employeeProfile.BaCode,
        //    //        BaName = employeeProfile.BaName,
        //    //        Email = employeeProfile.Email,

        //    //        Description = "ผู้ใช้งานทั่วไป",
        //    //        PMSDescription = "ผู้ใช้งานทั่วไป",

        //    //        IsAdmin = "0",
        //    //        PermissionCode = "9999",
        //    //        PMSPermissionCode = "9999",
        //    //        Mast_TRSG = "0",
        //    //        IsLogin = "1",
        //    //    };

        //    //    this.Session["UserProfile"] = profileData;

        //    //    return RedirectToAction("Index", "Home");
        //    //}
        //    if ((viewModel.password == "Nmkpt05*" || getMac != "") && employeeProfile != null) // by pass permission
        //    {
        //        int EmployeeId = Int32.Parse(employeeProfile.EmployeeId);

        //        //admin
        //        String Description = "ผู้ใช้งานทั่วไป";
        //        String PMSDescription = "ผู้ใช้งานทั่วไป";
        //        String isAdmin = "0";
        //        String isLogin = "1";
        //        String permissionCode = "9999";
        //        String PMSpermissionCode = "9999";
        //        String Mast_TRSG = "0";
        //        String TRSG = "";
        //        String IDPEA = "";
        //        String Status = "";
        //        List<PermissionModel> admin = new List<PermissionModel>();
        //        admin = Repository.StoredProcedure.SpBase.SpCheckPermission(EmployeeId.ToString());
        //        if (admin.Count != 0)
        //        {
        //            if (admin[0].PermissionCode != "")
        //            {
        //                isAdmin = "1";
        //                Description = admin[0].Description;
        //                permissionCode = admin[0].PermissionCode;
        //            }
        //            if (admin[0].PMSPermissionCode != "")
        //            {
        //                isAdmin = "1";
        //                PMSDescription = admin[0].PMSDescription;
        //                PMSpermissionCode = admin[0].PMSPermissionCode;
        //            }
        //            if (permissionCode == "40") // อก.
        //            {
        //                List<CostCenterModel> Cost = new List<CostCenterModel>();
        //                Cost = Repository.StoredProcedure.SpBase.SpCheckCostCenter(employeeProfile.CostCenterCode);
        //                if (Cost.Count == 0)
        //                {
        //                    MessageManager.SetErrorMessage("กรุณาตรวจสอบสิทธิ์การใช้งานให้ถูกต้อง หรือ ตรวจสอบศูนย์ต้นทุน");
        //                    return RedirectToAction("Index", "Login");
        //                }
        //                else
        //                {
        //                    Mast_TRSG = Cost[0].TRSG;
        //                }
        //                //Mast_TRSG = Cost[0].TRSG;
        //            }
        //            else if(permissionCode == "50") // ผจก.
        //            {
        //                List<CostCenterModel> Cost = new List<CostCenterModel>();
        //                Cost = Repository.StoredProcedure.SpBase.SpCheckCostCenterPEA(employeeProfile.CostCenterCode);
        //                if(Cost.Count == 0)
        //                {
        //                    MessageManager.SetErrorMessage("กรุณาตรวจสอบสิทธิ์การใช้งานให้ถูกต้อง หรือ ตรวจสอบศูนย์ต้นทุน");
        //                    return RedirectToAction("Index", "Login");
        //                }
        //                else
        //                {
        //                    Mast_TRSG = Cost[0].TRSG;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            isAdmin = "0";
        //        }

        //        if (employeeProfile.BaCode != "H000")
        //        {
        //            (Status, TRSG, IDPEA) = Repository.StoredProcedure.SpBase.spGetTRSG_By_CostCenterCode(employeeProfile.CostCenterCode.Substring(0, 7));
        //            if (Status == "0")
        //            {
        //                MessageManager.SetErrorMessage(TRSG);
        //                return RedirectToAction("Index", "Login");
        //            }
        //        }

        //        // เซตค่า Session
        //        var profileData = new UserProfileSessionData
        //        {
        //            Username = employeeProfile.Username,
        //            EmployeeId = employeeProfile.EmployeeId,
        //            TitleFullName = employeeProfile.TitleFullName,
        //            FirstName = employeeProfile.FirstName,
        //            LastName = employeeProfile.LastName,
        //            GroupId = employeeProfile.GroupId,
        //            Group = employeeProfile.Group,
        //            GenderCode = employeeProfile.GenderCode,
        //            Gender = employeeProfile.Gender,
        //            PositionCode = employeeProfile.PositionCode,
        //            PositionDescShort = employeeProfile.PositionDescShort,
        //            Position = employeeProfile.Position,
        //            LevelCode = employeeProfile.LevelCode,
        //            LevelDesc = employeeProfile.LevelDesc,
        //            NewOrganizationalCode = employeeProfile.NewOrganizationalCode,
        //            DepartmentShortName = employeeProfile.DepartmentShortName,
        //            DepartmentFullName = employeeProfile.DepartmentFullName,
        //            DepartmentSap = employeeProfile.DepartmentSap,
        //            DepartmentShort = employeeProfile.DepartmentShort,
        //            Department = employeeProfile.Department,
        //            RegionCode = employeeProfile.RegionCode,
        //            Region = employeeProfile.Region,
        //            SubRegionCode = employeeProfile.SubRegionCode,
        //            SubRegion = employeeProfile.SubRegion,
        //            CostCenterCode = employeeProfile.CostCenterCode,
        //            CostCenterName = employeeProfile.CostCenterName,
        //            Peacode = employeeProfile.Peacode,
        //            BaCode = employeeProfile.BaCode,
        //            BaName = employeeProfile.BaName,
        //            Email = employeeProfile.Email,

        //            Description = Description,
        //            PMSDescription = PMSDescription,

        //            IsAdmin = isAdmin,
        //            PermissionCode = permissionCode,
        //            PMSPermissionCode = PMSpermissionCode,
        //            Mast_TRSG = Mast_TRSG,
        //            IsLogin = isLogin,

        //            TRSG = TRSG,
        //            IDPEA = IDPEA,
        //        };

        //        this.Session["UserProfile"] = profileData;

        //        return RedirectToAction("Index", "Home");
        //    }
        //    if (loginResult.ResultObject.Result == true)
        //    {
        //        int EmployeeId = Int32.Parse(employeeProfile.EmployeeId);

        //        //admin
        //        String Description = "ผู้ใช้งานทั่วไป";
        //        String PMSDescription = "ผู้ใช้งานทั่วไป";
        //        String isAdmin = "0";
        //        String isLogin = "1";
        //        String permissionCode = "9999";
        //        String PMSpermissionCode = "9999";
        //        String Mast_TRSG = "0";
        //        String TRSG = "";
        //        String IDPEA = "";
        //        String Status = "";
        //        List<PermissionModel> admin = new List<PermissionModel>();
        //        admin = Repository.StoredProcedure.SpBase.SpCheckPermission(EmployeeId.ToString());
        //        if (admin.Count != 0)
        //        {
        //            if(admin[0].PermissionCode != "")
        //            {
        //                isAdmin = "1";
        //                Description = admin[0].Description;
        //                permissionCode = admin[0].PermissionCode;
        //            }
        //            if(admin[0].PMSPermissionCode != "")
        //            {
        //                isAdmin = "1";
        //                PMSDescription = admin[0].PMSDescription;
        //                PMSpermissionCode = admin[0].PMSPermissionCode;
        //            }

        //            if (permissionCode == "40") // อก.
        //            {
        //                List<CostCenterModel> Cost = new List<CostCenterModel>();
        //                Cost = Repository.StoredProcedure.SpBase.SpCheckCostCenter(employeeProfile.CostCenterCode);
        //                if (Cost.Count == 0)
        //                {
        //                    MessageManager.SetErrorMessage("กรุณาตรวจสอบสิทธิ์การใช้งานให้ถูกต้อง หรือ ตรวจสอบศูนย์ต้นทุน");
        //                    return RedirectToAction("Index", "Login");
        //                }
        //                else
        //                {
        //                    Mast_TRSG = Cost[0].TRSG;
        //                }
        //            }
        //            else if (permissionCode == "50") // ผจก.
        //            {
        //                List<CostCenterModel> Cost = new List<CostCenterModel>();
        //                Cost = Repository.StoredProcedure.SpBase.SpCheckCostCenterPEA(employeeProfile.CostCenterCode);
        //                if (Cost.Count == 0)
        //                {
        //                    MessageManager.SetErrorMessage("กรุณาตรวจสอบสิทธิ์การใช้งานให้ถูกต้อง หรือ ตรวจสอบศูนย์ต้นทุน");
        //                    return RedirectToAction("Index", "Login");
        //                }
        //                else
        //                {
        //                    Mast_TRSG = Cost[0].TRSG;
        //                }
        //            }
        //            //return RedirectToAction("Index", "Home");
        //        }
        //        else
        //        {
        //            isAdmin = "0";
        //            //MessageManager.SetErrorMessage("เกิดข้อผิดพลาด");
        //            //return RedirectToAction("Index", "Login");
        //        }

        //        if (employeeProfile.BaCode != "H000")
        //        {
        //            (Status, TRSG, IDPEA) = Repository.StoredProcedure.SpBase.spGetTRSG_By_CostCenterCode(employeeProfile.CostCenterCode.Substring(0, 7));
        //            if (Status == "0")
        //            {
        //                MessageManager.SetErrorMessage(TRSG);
        //                return RedirectToAction("Index", "Login");
        //            }
        //        }

        //        // เซตค่า Session
        //        var profileData = new UserProfileSessionData
        //        {
        //            Username = employeeProfile.Username,
        //            EmployeeId = employeeProfile.EmployeeId,
        //            TitleFullName = employeeProfile.TitleFullName,
        //            FirstName = employeeProfile.FirstName,
        //            LastName = employeeProfile.LastName,
        //            GroupId = employeeProfile.GroupId,
        //            Group = employeeProfile.Group,
        //            GenderCode = employeeProfile.GenderCode,
        //            Gender = employeeProfile.Gender,
        //            PositionCode = employeeProfile.PositionCode,
        //            PositionDescShort = employeeProfile.PositionDescShort,
        //            Position = employeeProfile.Position,
        //            LevelCode = employeeProfile.LevelCode,
        //            LevelDesc = employeeProfile.LevelDesc,
        //            NewOrganizationalCode = employeeProfile.NewOrganizationalCode,
        //            DepartmentShortName = employeeProfile.DepartmentShortName,
        //            DepartmentFullName = employeeProfile.DepartmentFullName,
        //            DepartmentSap = employeeProfile.DepartmentSap,
        //            DepartmentShort = employeeProfile.DepartmentShort,
        //            Department = employeeProfile.Department,
        //            RegionCode = employeeProfile.RegionCode,
        //            Region = employeeProfile.Region,
        //            SubRegionCode = employeeProfile.SubRegionCode,
        //            SubRegion = employeeProfile.SubRegion,
        //            CostCenterCode = employeeProfile.CostCenterCode,
        //            CostCenterName = employeeProfile.CostCenterName,
        //            Peacode = employeeProfile.Peacode,
        //            BaCode = employeeProfile.BaCode,
        //            BaName = employeeProfile.BaName,
        //            Email = employeeProfile.Email,

        //            Description = Description,
        //            PMSDescription = PMSDescription,

        //            IsAdmin = isAdmin,
        //            PermissionCode = permissionCode,
        //            PMSPermissionCode = PMSpermissionCode,
        //            Mast_TRSG = Mast_TRSG,
        //            IsLogin = isLogin,
        //        };

        //        this.Session["UserProfile"] = profileData;

        //        return RedirectToAction("Index", "Home");
        //    }
        //    else
        //    {
        //        MessageManager.SetErrorMessage(loginResult.ResponseMsg);
        //        return RedirectToAction("Index", "Login");
        //    }
        //}

        public ActionResult Bypass()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AuthenticationByPass(LoginModel viewModel)
        {
            String Status = "", Message = "";
            if (viewModel.password == "admin@1234")
            {
                (Status, Message) = HrPlatform(viewModel.username);

                if (Status == "true")
                {
                    return RedirectToAction("Index", "Home");//, new { id = 10 });
                }
                else
                {
                    MessageManager.SetErrorMessage(Message);
                    return RedirectToAction("Bypass", "Login");
                }
            }
            else
            {
                MessageManager.SetErrorMessage("รหัสผ่านไม่ถูกต้อง");
                return RedirectToAction("Bypass", "Login");
            }
        }

    }
}