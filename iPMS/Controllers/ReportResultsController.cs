﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using iPMS.ServiceReference2;
using iPMS.Utility;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class ReportResultsController : BaseController
    {
        // GET: ReportResults
        private String getRound = GetQuarter();
        private static String GetQuarter() //แสดงค่าไตรมาส ณ ปัจจุบัน
        {
            DateTime date = DateTime.Now;
            if (date.Month >= 4 && date.Month <= 6)
                return "1";
            else if (date.Month >= 7 && date.Month <= 9)
                return "2";
            else if (date.Month >= 10 && date.Month <= 12)
                return "3";
            else
                return "4";
        }

        public ActionResult Index()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            return View();
        }
        //public ActionResult SocietyAndEnvironment()
        //{
        //    // เช็ค session expire
        //    var profileData = this.Session["UserProfile"] as UserProfileSessionData;
        //    if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
        //    {
        //        MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
        //        return RedirectToAction("Index", "Login");
        //    }
        //    // เช็ค session expire
        //    String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
        //    String TypeID = "3"; // แผนปฏิบัติ
        //    String TopicID = "1"; // ด้านสังคม
        //    String PEAGroup = "2"; // กฟฟ ชั้น 1-3
        //    String NameTimesAYear = "2"; // วัดทุก 4 เดือน
        //    String Round = getRound; // ไตรมาสของเดือนปัจจุบัน
        //    String Status = "";
        //    String Message = "";
        //    ViewReportResults viewModel = new ViewReportResults();
        //    viewModel.YearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetSectionYear(TypeID).ToList();

        //    // ดึงปี ล่าสุด ของ DB
        //    if(viewModel.YearsModelLists.Count > 0)
        //    {
        //        if (viewModel.YearsModelLists[0].Year != "")
        //        {
        //            Year = viewModel.YearsModelLists[0].Year;
        //        }
        //    }

        //    // เช็ค ระดับการเข้าใช้งาน เพื่อ คัดแยกการให้ Filter กฟข
        //    if ((Mapper.MapStringToInteger(profileData.PermissionCode) >= 0 && Mapper.MapStringToInteger(profileData.PermissionCode) <= 30)
        //        || profileData.BaCode == "H000") // กรณี ถ้า BA ตัวเองเป็นเขต จะสามารถ Filter กฟข ได้
        //    {
        //        viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA().ToList();
        //    }
        //    else
        //    {
        //        viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA("1").ToList();
        //    }

        //    viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetTimesAYear(NameTimesAYear).ToList();

        //    (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpReportResult.
        //        SpGetRawDataSection(Year,TypeID,TopicID,PEAGroup, NameTimesAYear, Round);

        //    ViewData["TopicName"] = "ด้านสังคมและสิ่งแวดล้อม";
        //    ViewData["TopicID"] = TopicID;
        //    ViewData["Year"] = Year;
        //    ViewData["PEAGroup"] = PEAGroup;
        //    ViewData["NameTimesAYear"] = NameTimesAYear;
        //    ViewData["Round"] = Round;

        //    return View(viewModel);
        //}
        //public ActionResult Finance()
        //{
        //    // เช็ค session expire
        //    var profileData = this.Session["UserProfile"] as UserProfileSessionData;
        //    if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
        //    {
        //        MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
        //        return RedirectToAction("Index", "Login");
        //    }
        //    // เช็ค session expire
        //    String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
        //    String TypeID = "3"; // แผนปฏิบัติ
        //    String TopicID = "2"; // ด้านการเงิน
        //    String PEAGroup = "2"; // กฟฟ ชั้น 1-3
        //    String NameTimesAYear = "2"; // วัดทุก 4 เดือน
        //    String Round = getRound; // ไตรมาสของเดือนปัจจุบัน
        //    String Status = "";
        //    String Message = "";
        //    ViewReportResults viewModel = new ViewReportResults();
        //    viewModel.YearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetSectionYear(TypeID).ToList();

        //    // ดึงปี ล่าสุด ของ DB
        //    if (viewModel.YearsModelLists.Count > 0)
        //    {
        //        if (viewModel.YearsModelLists[0].Year != "")
        //        {
        //            Year = viewModel.YearsModelLists[0].Year;
        //        }
        //    }

        //    // เช็ค ระดับการเข้าใช้งาน เพื่อ คัดแยกการให้ Filter กฟข
        //    if ((Mapper.MapStringToInteger(profileData.PermissionCode) >= 0 && Mapper.MapStringToInteger(profileData.PermissionCode) <= 30)
        //        || profileData.BaCode == "H000") // กรณี ถ้า BA ตัวเองเป็นเขต จะสามารถ Filter กฟข ได้
        //    {
        //        viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA().ToList();
        //    }
        //    else
        //    {
        //        viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA("1").ToList();
        //    }

        //    viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetTimesAYear(NameTimesAYear).ToList();

        //    (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpReportResult.
        //        SpGetRawDataSection(Year, TypeID, TopicID, PEAGroup, NameTimesAYear, Round);

        //    ViewData["TopicName"] = "ด้านการเงิน";
        //    ViewData["TopicID"] = TopicID;
        //    ViewData["Year"] = Year;
        //    ViewData["PEAGroup"] = PEAGroup;
        //    ViewData["NameTimesAYear"] = NameTimesAYear;
        //    ViewData["Round"] = Round;

        //    return View(viewModel);
        //}
        public ActionResult Target()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String TypeID = "3"; // แผนปฏิบัติ
            String TopicID = "1"; // ด้านเป้าหมาย
            String PEAGroup = "2"; // กฟฟ ชั้น 1-3
            String NameTimesAYear = "2"; // วัดทุก 4 เดือน
            String Round = getRound; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewReportResults viewModel = new ViewReportResults();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetSectionYear(TypeID).ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }

            // เช็ค ระดับการเข้าใช้งาน เพื่อ คัดแยกการให้ Filter กฟข
            if ((Mapper.MapStringToInteger(profileData.PermissionCode) >= 0 && Mapper.MapStringToInteger(profileData.PermissionCode) <= 30)
                || profileData.BaCode == "H000") // กรณี ถ้า BA ตัวเองเป็นเขต จะสามารถ Filter กฟข ได้
            {
                viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA().ToList();
            }
            else
            {
                viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA("1").ToList();
            }

            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetTimesAYear(NameTimesAYear).ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpReportResult.
                SpGetRawDataSection(Year, TypeID, TopicID, PEAGroup, NameTimesAYear, Round);
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["TopicName"] = "ด้านเป้าหมาย";
            ViewData["TopicID"] = TopicID;
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Round;

            return View(viewModel);
        }
        public ActionResult Customer()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String TypeID = "3"; // แผนปฏิบัติ
            String TopicID = "2"; // ด้านลูกค้า
            String PEAGroup = "2"; // กฟฟ ชั้น 1-3
            String NameTimesAYear = "2"; // วัดทุก 4 เดือน
            String Round = getRound; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewReportResults viewModel = new ViewReportResults();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetSectionYear(TypeID).ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }

            // เช็ค ระดับการเข้าใช้งาน เพื่อ คัดแยกการให้ Filter กฟข
            if ((Mapper.MapStringToInteger(profileData.PermissionCode) >= 0 && Mapper.MapStringToInteger(profileData.PermissionCode) <= 30)
                || profileData.BaCode == "H000") // กรณี ถ้า BA ตัวเองเป็นเขต จะสามารถ Filter กฟข ได้
            {
                viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA().ToList();
            }
            else
            {
                viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA("1").ToList();
            }

            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetTimesAYear(NameTimesAYear).ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpReportResult.
                SpGetRawDataSection(Year, TypeID, TopicID, PEAGroup, NameTimesAYear, Round);
            if(viewModel.RawDataSectionModelLists.Count == 1)
            {
                if(viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["TopicName"] = "ด้านลูกค้า";
            ViewData["TopicID"] = TopicID;
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Round;

            return View(viewModel);
        }
        public ActionResult Internal()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String TypeID = "3"; // แผนปฏิบัติ
            String TopicID = "3"; // ด้านกระบวนการภายใน
            String PEAGroup = "2"; // กฟฟ ชั้น 1-3
            String NameTimesAYear = "2"; // วัดทุก 4 เดือน
            String Round = getRound; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewReportResults viewModel = new ViewReportResults();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetSectionYear(TypeID).ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }

            // เช็ค ระดับการเข้าใช้งาน เพื่อ คัดแยกการให้ Filter กฟข
            if ((Mapper.MapStringToInteger(profileData.PermissionCode) >= 0 && Mapper.MapStringToInteger(profileData.PermissionCode) <= 30)
                || profileData.BaCode == "H000") // กรณี ถ้า BA ตัวเองเป็นเขต จะสามารถ Filter กฟข ได้
            {
                viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA().ToList();
            }
            else
            {
                viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA("1").ToList();
            }

            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetTimesAYear(NameTimesAYear).ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpReportResult.
                SpGetRawDataSection(Year, TypeID, TopicID, PEAGroup, NameTimesAYear, Round);
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["TopicName"] = "ด้านกระบวนการภายใน";
            ViewData["TopicID"] = TopicID;
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Round;

            return View(viewModel);
        }
        public ActionResult LearningAndDevelopment()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String TypeID = "3"; // แผนปฏิบัติ
            String TopicID = "4"; // ด้านเรียนรู้และพัฒนา
            String PEAGroup = "2"; // กฟฟ ชั้น 1-3
            String NameTimesAYear = "2"; // วัดทุก 4 เดือน
            String Round = getRound; // ไตรมาสของเดือนปัจจุบัน
            String Status = "";
            String Message = "";
            ViewReportResults viewModel = new ViewReportResults();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetSectionYear(TypeID).ToList();

            // ดึงปี ล่าสุด ของ DB
            if (viewModel.YearsModelLists.Count > 0)
            {
                if (viewModel.YearsModelLists[0].Year != "")
                {
                    Year = viewModel.YearsModelLists[0].Year;
                }
            }

            // เช็ค ระดับการเข้าใช้งาน เพื่อ คัดแยกการให้ Filter กฟข
            if ((Mapper.MapStringToInteger(profileData.PermissionCode) >= 0 && Mapper.MapStringToInteger(profileData.PermissionCode) <= 30)
                || profileData.BaCode == "H000") // กรณี ถ้า BA ตัวเองเป็นเขต จะสามารถ Filter กฟข ได้
            {
                viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA().ToList();
            }
            else
            {
                viewModel.MastPEAModelLists = Repository.StoredProcedure.SpReportResult.SpGetMastPEA("1").ToList();
            }

            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetTimesAYear(NameTimesAYear).ToList();

            (viewModel.RawDataSectionModelLists, Status, Message) = Repository.StoredProcedure.SpReportResult.
                SpGetRawDataSection(Year, TypeID, TopicID, PEAGroup, NameTimesAYear, Round);
            if (viewModel.RawDataSectionModelLists.Count == 1)
            {
                if (viewModel.RawDataSectionModelLists[0].ID == null)
                {
                    viewModel.RawDataSectionModelLists.Clear();
                }
            }
            ViewData["TopicName"] = "ด้านเรียนรู้และพัฒนา";
            ViewData["TopicID"] = TopicID;
            ViewData["Year"] = Year;
            ViewData["PEAGroup"] = PEAGroup;
            ViewData["NameTimesAYear"] = NameTimesAYear;
            ViewData["Round"] = Round;

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult GetDataResult(String ID)
        {
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire
                String Status = "";
                String Message = "";
                List<ResultModel> ModelLists;
                (ModelLists, Status, Message) = Repository.StoredProcedure.SpReportResult.SpGetDataResult(ID);

                if(ModelLists.Count > 0 && ModelLists[0].UsernameAdmin == "" && ModelLists[0].UpdateAdmin != "")
                {
                    //EmployeeProfile employeeProfile = IdmManager.GetByEmployeeNo(ModelLists[0].UpdateAdmin);
                    UserProfileSessionData ResponseprofileData = new UserProfileSessionData();
                    (ResponseprofileData, Status, Message) = getHrPlatform(ModelLists[0].UpdateAdmin);
                    ModelLists[0].UsernameAdmin = ResponseprofileData.TitleFullName + ResponseprofileData.FirstName + " " + ResponseprofileData.LastName;
                }

                var myData2 = new
                {
                    Status = Status, // Get สำเร็จ
                    Data = ModelLists,
                    Message = Message,
                    DateTimeAdmin = ModelLists[0].DatetimeADmin.ToString("dd/MM/yyyy HH:mm:ss"),
                    DateTimeUser = ModelLists[0].DatetimeUser.ToString("dd/MM/yyyy HH:mm:ss"),
                };
                return Json(myData2);
            }
            catch (Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }

        [HttpPost]
        public ActionResult FilterYear(String Year, String PEAGroup,
            String NameTimesAYear, String Round,String TypeID, String TopicID)
        {
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                List<RawDataSectionModel> ModelLists;
                String Status = "";
                String Message = "";
                (ModelLists,Status,Message) = Repository.StoredProcedure.SpReportResult.
                    SpGetRawDataSection(Year,TypeID, TopicID, PEAGroup,NameTimesAYear ,Round);

                if (ModelLists.Count == 1 && ModelLists[0].ID == null && Status == "1")
                {
                    var myData3 = new
                    {
                        Status = "2", // Get สำเร็จ แต่หัวข้อกิจกรรมไม่มีในฐานข้อมูล
                        Message = "ไม่พบข้อมูลในฐานข้อมูล"
                    };
                    return Json(myData3);
                }

                var myData2 = new
                {
                    Status = "1", // Get สำเร็จ
                    Data = ModelLists,
                    Message = Message
                };
                return Json(myData2);
            }
            catch (Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }
        [HttpPost]
        public ActionResult Insert(ResultModel model)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
                //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
            }
            // เช็ค session expire

            // เช็คสถานะการลงคะแนน ณ ปัจจุบัน
            String ID, Cancel, Status, Message;
            String CheckAll = "0";
            String TypeID = "3";
            (ID, Cancel, Status, Message) = Repository.StoredProcedure.SpAdmin.SpGetCheckMastLock(TypeID, model.CheckYear, model.CheckTimesAYear, model.Round.ToString());
            if(ID != "")
            {
                var myData44 = new
                {
                    Status = "0",  // Error
                    Message = "ปิดระบบการลงคะแนน กรุณาติดต่อผู้ดูแล"
                };
                return Json(myData44);
            }
            //var myData55 = new
            //{
            //    Status = "0",  // Error
            //    Message = Message
            //};
            //return Json(myData55);
            //

            // Update สถานะคะแนน
            if (Mapper.MapStringToInteger(profileData.PermissionCode) <= 20 && Mapper.MapStringToInteger(profileData.PermissionCode) >= 0)
            {   // ให้คะแนนของ ผู้พัฒนา แอดมิน และ คณะทำงาน
                model.UpdateAdmin = profileData.Username;
                model.UsernameAdmin = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;

                if (model.CheckInsertAll == true) // เช็คลงคะแนนทั้งหมด
                {
                    CheckAll = "1";
                    (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdminAll(model); // Update Result ALL
                }
                else
                {
                    (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdmin(model); // Update Result
                }
                if (Status == "1") // Insert สำเร็จ
                {
                    DataSectionModel DataSectionResult = new DataSectionModel();
                    List<DataSectionModel> ListDataSectionResult = new List<DataSectionModel>();
                    if (model.CheckInsertAll == true) // เช็คลงคะแนนทั้งหมด
                    {
                        (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdminLogAll(model, controllerName, actionName, "Success", Message);
                        (ListDataSectionResult, Status, Message) = Repository.StoredProcedure.SpReportResult.SpGetDataSectionResultAll(model);
                    }
                    else
                    {
                        (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdminLog(model, controllerName, actionName, "Success", Message);
                        (DataSectionResult, Status, Message) = Repository.StoredProcedure.SpReportResult.SpGetDataSectionResult(model);
                    }
                    
                    int move = 0;
                    if (Mapper.MapStringToInteger(profileData.PermissionCode) <= 10 && Mapper.MapStringToInteger(profileData.PermissionCode) >= 0)
                    {   // admin มีถังขยะ และ All
                        move = 6;
                    }
                    else if (Mapper.MapStringToInteger(profileData.PermissionCode) <= 30 && Mapper.MapStringToInteger(profileData.PermissionCode) >= 20)
                    {   // มีฟังก์ชัน checkbox all
                        move = 5;
                    }
                    var myData = new // ผลลัพธ์ เพื่ออัพเดท ตาราง
                    {
                        Status = "1",
                        Message = Message,
                        Data = DataSectionResult,
                        DataAll = ListDataSectionResult,
                        IsAdmin = "1",
                        Move = move,
                        CheckAll = CheckAll
                };
                    return Json(myData);
                }
                else // error
                {
                    (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdminLog(model, controllerName, actionName, "Failed", Message);
                    var myData = new
                    {
                        Status = "0",  // Error
                        Message = Message
                    };
                    return Json(myData);
                }
            }
            else if (Mapper.MapStringToInteger(profileData.PermissionCode) == 30) // ให้คะแนนของ ผู้รับผิดชอบ
            {   
                String CostCenterCode, SelfCostCenter;
                model.UpdateAdmin = profileData.Username;
                (Status,CostCenterCode) = Repository.StoredProcedure.SpReportResult.SpGetGetCheckCostCenter(model.ForeignSection);
                if (Status == "1")
                {
                    SelfCostCenter = profileData.CostCenterCode;
                    CostCenterCode = CostCenterCode.Substring(0, 7);
                    SelfCostCenter = SelfCostCenter.Substring(0, 7);
                    if (SelfCostCenter == CostCenterCode || Mapper.MapStringToInteger(profileData.PermissionCode) == 1) // รหัสศูนย์ต้นทุน ตรงกับหัวข้อ
                    {
                        model.UsernameAdmin = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;

                        if (model.CheckInsertAll == true) // เช็คลงคะแนนทั้งหมด
                        {
                            CheckAll = "1";
                            (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdminAll(model); // Update Result ALL
                        }
                        else
                        {
                            (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdmin(model); // Update Result
                        }
                        
                        if(Status == "1") // Insert สำเร็จ
                        {
                            DataSectionModel DataSectionResult = new DataSectionModel(); ;
                            List<DataSectionModel> ListDataSectionResult = new List<DataSectionModel>();
                            if (model.CheckInsertAll == true) // เช็คลงคะแนนทั้งหมด
                            {
                                (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdminLogAll(model, controllerName, actionName, "Success", Message);
                                (ListDataSectionResult, Status, Message) = Repository.StoredProcedure.SpReportResult.SpGetDataSectionResultAll(model);
                            }
                            else
                            {
                                (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdminLog(model, controllerName, actionName, "Success", Message);
                                (DataSectionResult, Status, Message) = Repository.StoredProcedure.SpReportResult.SpGetDataSectionResult(model);

                            }

                            int move = 0;
                            if (Mapper.MapStringToInteger(profileData.PermissionCode) <= 10 && Mapper.MapStringToInteger(profileData.PermissionCode) >= 0)
                            { // admin มีถังขยะ และ All
                                move = 6;
                            }
                            else if (Mapper.MapStringToInteger(profileData.PermissionCode) <= 30 && Mapper.MapStringToInteger(profileData.PermissionCode) >= 20)
                            { // มีฟังก์ชัน checkbox all
                                move = 5;
                            }
                            var myData = new // ผลลัพธ์ เพื่ออัพเดท ตาราง
                            {
                                Status = "1",
                                Message = Message,
                                Data = DataSectionResult,
                                DataAll = ListDataSectionResult,
                                IsAdmin = "1",
                                Move = move,
                                CheckAll = CheckAll
                            };
                            return Json(myData);
                        }
                        else // error
                        {
                            (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByAdminLog(model, controllerName, actionName, "Failed", Message);
                            var myData = new
                            {
                                Status = "0",  // Error
                                Message = Message
                            };
                            return Json(myData);
                        }
                    }
                    else 
                    {
                        var myData = new
                        {
                            Status = "0", // Error
                            Message = "คุณไม่มีสิทธิ์ให้คะแนนหัวข้อกิจกรรมนี้"
                        };
                        return Json(myData);
                    }
                }
                else // ดึงข้อมูล Error
                {
                    var myData = new
                    {
                        Status = "0",  // Error
                        Message = CostCenterCode
                    };
                    return Json(myData);
                }
            }
            else // หน้างาน มีการชี้แจงเข้ามา
            {
                model.UpdateUser = profileData.Username;
                model.UsernameUser = profileData.TitleFullName + profileData.FirstName + " " + profileData.LastName;

                String BA, TRSG,SelfBA,SelfTRSG;
                (Status, BA,TRSG) = Repository.StoredProcedure.SpReportResult.SpGetGetCheckTRSG(model.ID);
                if (Status == "1")
                {
                    SelfBA = profileData.BaCode;
                    SelfTRSG = profileData.Peacode;
                    if (SelfBA == BA) // BA ตรงหรือไม่
                    {
                        (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByUser(model);
                        if (Status == "1") // Insert สำเร็จ
                        {
                            (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByUserLog(model, controllerName, actionName, "Success", Message);
                            DataSectionModel DataSectionResult;
                            String Mes = "";
                            (DataSectionResult, Status, Mes) = Repository.StoredProcedure.SpReportResult.SpGetDataSectionResult(model);
                            int move = 4;
                            var myData = new
                            {
                                Status = "1", 
                                Message = Message,
                                Data = DataSectionResult,
                                IsAdmin = "0",
                                Move = move
                            };
                            return Json(myData);
                        }
                        else // error
                        {
                            (Status, Message) = Repository.StoredProcedure.SpReportResult.SpUpdateResultsByUserLog(model, controllerName, actionName, "Failed", Message);
                            var myData = new
                            {
                                Status = "0",  // Error
                                Message = Message
                            };
                            return Json(myData);
                        }
                    }
                    else
                    {
                        var myData = new
                        {
                            Status = "0",  // Error
                            Message = "คุณไม่มีสิทธิ์คอมเมนต์นี้"
                        };
                        return Json(myData);
                    }
                }
                else // ดึงข้อมูล Error
                {
                    var myData = new
                    {
                        Status = "0",  // Error
                        Message = BA
                    };
                    return Json(myData);
                }
            }
        }
        [HttpPost]
        public ActionResult DeleteSection(String ID,String GroupSection)
        {
            try
            {
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire
                String Status, Message;
                (Status,Message) = Repository.StoredProcedure.SpReportResult.SpGetCheckSection(ID, GroupSection);
                if (Message == null || Message == "") // ไม่มีคะแนน
                {
                    (Status, Message) = Repository.StoredProcedure.SpReportResult.SpDeleteSection(ID, GroupSection, profileData.Username, controllerName, actionName, "Success", Message);
                    if(Status == "1")
                    {
                        var myData2 = new
                        {
                            Status = "1", // ลบ สำเร็จ
                            Message = Message,
                        };
                        return Json(myData2);
                    }
                    else
                    {
                        var myData2 = new
                        {
                            Status = Status, // ลบ ไม่สำเร็จ
                            Message = Message,
                        };
                        return Json(myData2);
                    }
                }
                else // มีคะแนน ลบไม่ได้
                {
                    var myData2 = new
                    {
                        Status = "0", // ลบไม่สำเร็จ
                        Message = "ไม่สามารถลบได้ เนื่องจากมีการให้คะแนนไปแล้ว ถ้าต้องการกรุณาติดต่อผู้พัฒนา",
                    };
                    return Json(myData2);
                }
            }
            catch (Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(String Year, String PEAGroup, String NameTimesAYear, String Round, String TypeID, String TopicID)
        {
            byte[] fileData = null;
            using (var ms = new MemoryStream())
            {
                using (SLDocument sl = new SLDocument())
                {
                    String Status, Message = "";
                    SLStyle TextCenterStyle = new SLStyle();
                    TextCenterStyle.SetVerticalAlignment(DocumentFormat.OpenXml.Spreadsheet.VerticalAlignmentValues.Center);
                    TextCenterStyle.SetHorizontalAlignment(DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);

                    sl.SetCellValue("A1", "ลำดับ");

                    // merge all cells in the cell range A1:A2
                    sl.MergeWorksheetCells("A1", "A2");

                    sl.SetCellValue("B1", "ส่วนงาน");
                    sl.MergeWorksheetCells("B1", "B2");

                    sl.SetCellValue("C1", "แผนงาน");
                    sl.MergeWorksheetCells("C1", "C2");

                    sl.SetCellValue("D1", "กิจกรรม");
                    sl.MergeWorksheetCells("D1", "D2");

                    sl.SetCellValue("E1", "ผู้ประเมิน");
                    sl.MergeWorksheetCells("E1", "E2");

                    List<RawDataSectionModel> Data = new List<RawDataSectionModel>();

                    (Data, Status, Message) = Repository.StoredProcedure.SpReportResult.
                        SpGetRawDataSection(Year, TypeID, TopicID, PEAGroup, NameTimesAYear, Round);

                    String a = Status;
                    if (Data.Count > 0)
                    {
                        int CountPEA = Data[0].PEA.Count; // ใช้ในการ merge cell

                        int ColumnIndex = 1;
                        int ColumnSegment = 2;
                        int ColumnPlans = 3;
                        int ColumnSectionName = 4;
                        int ColumnAssessorDepartment = 5;
                        int Columnperformance = 6;

                        sl.SetCellValue(1, Columnperformance, "ผลการดำเนินงาน"); // RowIndex, ColumnIndex, Data)
                        sl.MergeWorksheetCells(1, Columnperformance, 1, Columnperformance + CountPEA); // StartRowIndex, StartColumnIndex, EndRowIndex, EndColumnIndex

                        for (int i = 0; i < Data[0].PEA.Count; i++)
                        {
                            sl.SetCellValue(2, Columnperformance + i, Data[0].PEA[i].PEA_OfficeNAME); // RowIndex, ColumnIndex, Data)
                        }

                        int row = 3;
                        int column = Columnperformance;
                        String CheckPlans = Data[0].Plans;

                        int FixRow = 3; // เริ่มต้นข้อมูล 
                        int StartRow = FixRow; // เริ่ม merge
                        int EndRow = FixRow; // จบ merge
                        int IsDuplicate = 0; // สถานะตอนนี้ซ้ำหรือไม่ซ้ำ

                        for (int indexData = 0; indexData < Data.Count; indexData++)
                        {
                            if (indexData == 0)
                            {
                                sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                sl.SetCellValue(row, ColumnSegment, Data[indexData].SegmentName);
                                sl.SetCellValue(row, ColumnPlans, Data[indexData].Plans);
                                sl.SetCellValue(row, ColumnSectionName, Data[indexData].SectionName);
                                sl.SetCellValue(row, ColumnAssessorDepartment, Data[indexData].AssessorDepartment);
                            }
                            else if (CheckPlans == Data[indexData].Plans && indexData == Data.Count - 1)
                            {
                                EndRow = FixRow + 1; // รอบสุดท้าย ไม่ได้ + FixRow

                                sl.MergeWorksheetCells(StartRow, ColumnPlans, EndRow, ColumnPlans);
                                sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                sl.SetCellValue(row, ColumnSegment, Data[indexData].SegmentName);
                                sl.SetCellValue(row, ColumnSectionName, Data[indexData].SectionName);
                                sl.SetCellValue(row, ColumnAssessorDepartment, Data[indexData].AssessorDepartment);

                                IsDuplicate = 0;
                            }
                            else if (CheckPlans == Data[indexData].Plans) // ข้อมูลซ้ำ ตั้งแต่ row ที่ 2 ขึนไป 
                            {
                                if (IsDuplicate == 0)
                                {
                                    StartRow = FixRow;
                                    IsDuplicate++;
                                }
                                sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                sl.SetCellValue(row, ColumnSegment, Data[indexData].SegmentName);
                                sl.SetCellValue(row, ColumnSectionName, Data[indexData].SectionName);
                                sl.SetCellValue(row, ColumnAssessorDepartment, Data[indexData].AssessorDepartment);
                                FixRow++;
                            }
                            else if (CheckPlans != Data[indexData].Plans)
                            {
                                CheckPlans = Data[indexData].Plans;
                                if (IsDuplicate == 0)
                                {
                                    sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                    sl.SetCellValue(row, ColumnSegment, Data[indexData].SegmentName);
                                    sl.SetCellValue(row, ColumnPlans, Data[indexData].Plans);
                                    sl.SetCellValue(row, ColumnSectionName, Data[indexData].SectionName);
                                    sl.SetCellValue(row, ColumnAssessorDepartment, Data[indexData].AssessorDepartment);
                                }
                                else
                                {
                                    EndRow = FixRow;

                                    sl.MergeWorksheetCells(StartRow, ColumnPlans, EndRow, ColumnPlans);

                                    sl.SetCellValue(row, ColumnIndex, indexData + 1);
                                    sl.SetCellValue(row, ColumnSegment, Data[indexData].SegmentName);
                                    sl.SetCellValue(row, ColumnPlans, Data[indexData].Plans);
                                    sl.SetCellValue(row, ColumnSectionName, Data[indexData].SectionName);
                                    sl.SetCellValue(row, ColumnAssessorDepartment, Data[indexData].AssessorDepartment);
                                    IsDuplicate = 0;

                                    StartRow = FixRow + 1; // + กันเผื่อกรณีที่เจอตัวสุดท้ายแผนงานซ้ำกัน
                                }

                                FixRow++;
                            }
                            // คะแนนแต่ละการไฟฟ้า
                            for (int indexPEA = 0; indexPEA < Data[0].PEA.Count; indexPEA++)
                            {
                                sl.SetCellValue(row, column + indexPEA, Data[indexData].PEA[indexPEA].Result);
                            }
                            row++;
                        }

                        // merge all cells from rows 10 through 12, columns 4 through 6
                        // This is basically the cell range D10:F12

                        sl.SetCellStyle(1, 1, 2 + Data.Count, column + Data[0].PEA.Count, TextCenterStyle);
                        sl.AutoFitColumn(1, column + Data[0].PEA.Count);
                        sl.AutoFitRow(1, 2 + Data.Count);

                        //sl.SetColumnWidth(2, 100);
                        //sl.SetColumnWidth(3, 100);
                        sl.SaveAs(ms);
                    }
                }
                fileData = ms.ToArray();
            }
            return File(fileData, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report.xlsx");
        }
    }
}