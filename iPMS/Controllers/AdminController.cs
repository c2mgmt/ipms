﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using iPMS.ServiceReference2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class AdminController : BaseController
    {
        // GET: Admin
        public ActionResult Index()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            // เช็คสิทธิ์ admin
            if (!CheckPermission(profileData.PermissionCode,1))// สิทธิ์ระดับผู้พัฒนาและผู้ดูแลระบบเท่านั้น
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin

            // สร้าง Object ViewAdmin เพื่อส่งค่าไปให้ View แสดงผล
            ViewAdmin viewModel = new ViewAdmin();
            viewModel.PermissionAdminModelLists = Repository.StoredProcedure.SpAdmin.SpGetAdminPermission().ToList();
            viewModel.PermissionRoleModelLists = Repository.StoredProcedure.SpAdmin.SpGetAdminPermissionRole(profileData.PermissionCode).ToList();
            viewModel.PMSPermissionRoleModelLists = Repository.StoredProcedure.SpAdmin.SpGetAdminPMSPermissionRole(profileData.PermissionCode).ToList();
            return View(viewModel);
            // สร้าง Object ViewAdmin เพื่อส่งค่าไปให้ View แสดงผล
        }

        public ActionResult ManagementSystem()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            // เช็คสิทธิ์ admin
            if (!CheckPermission(profileData.PermissionCode, 1))// สิทธิ์ระดับผู้พัฒนาและผู้ดูแลระบบเท่านั้น
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin

            // สร้าง Object ViewAdmin เพื่อส่งค่าไปให้ View แสดงผล
            ViewManagementSystem viewModel = new ViewManagementSystem();
            viewModel.TypeModelLists = Repository.StoredProcedure.SpDataManagement.spGetType_2().ToList();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpAdmin.SpGetSectionYear().ToList();
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetTimesAYear().ToList();
            viewModel.MastLockModelLists = Repository.StoredProcedure.SpAdmin.SpGetTableMastLock().ToList();

            return View(viewModel);
            // สร้าง Object ViewAdmin เพื่อส่งค่าไปให้ View แสดงผล
        }

        // ค้นหารหัสพนักงาน
        [HttpPost]
        public ActionResult ResponseData(String UserId)
        {
            // สร้างตัวแปร
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                // สร้าง Object เก็บข้อมูลพนักงาน
                //EmployeeProfile employeeProfile = IdmManager.GetByEmployeeNo(UserId);
                String Status = "", Message = "";
                UserProfileSessionData ResponseprofileData = new UserProfileSessionData();
                (ResponseprofileData, Status, Message) = getHrPlatform(UserId);

                if (ResponseprofileData != null) // ถ้ามีข้อมูลพนักงานคนนั้น
                {
                    // เก็บข้อมูลไว้ในตัวแปร profileData
                    //var ResponseprofileData = new UserProfileSessionData
                    //{
                    //    Username = employeeProfile.Username,
                    //    EmployeeId = employeeProfile.EmployeeId,
                    //    TitleFullName = employeeProfile.TitleFullName,
                    //    FirstName = employeeProfile.FirstName,
                    //    LastName = employeeProfile.LastName,
                    //    GroupId = employeeProfile.GroupId,
                    //    Group = employeeProfile.Group,
                    //    GenderCode = employeeProfile.GenderCode,
                    //    Gender = employeeProfile.Gender,
                    //    PositionCode = employeeProfile.PositionCode,
                    //    PositionDescShort = employeeProfile.PositionDescShort,
                    //    Position = employeeProfile.Position,
                    //    LevelCode = employeeProfile.LevelCode,
                    //    LevelDesc = employeeProfile.LevelDesc,
                    //    NewOrganizationalCode = employeeProfile.NewOrganizationalCode,
                    //    DepartmentShortName = employeeProfile.DepartmentShortName,
                    //    DepartmentFullName = employeeProfile.DepartmentFullName,
                    //    DepartmentSap = employeeProfile.DepartmentSap,
                    //    DepartmentShort = employeeProfile.DepartmentShort,
                    //    Department = employeeProfile.Department,
                    //    RegionCode = employeeProfile.RegionCode,
                    //    Region = employeeProfile.Region,
                    //    SubRegionCode = employeeProfile.SubRegionCode,
                    //    SubRegion = employeeProfile.SubRegion,
                    //    CostCenterCode = employeeProfile.CostCenterCode,
                    //    CostCenterName = employeeProfile.CostCenterName,
                    //    Peacode = employeeProfile.Peacode,
                    //    BaCode = employeeProfile.BaCode,
                    //    BaName = employeeProfile.BaName,
                    //    Email = employeeProfile.Email,
                    //};
                    // ส่งค่าไปให้ Ajax
                    var myData = new
                    {
                        Status = "1", // ค้นหาเจอ
                        Data = ResponseprofileData,
                    };
                    return Json(myData);
                    //return Json(ResponseprofileData);
                }
                else // ถ้าไม่เจอรหัสพนักงาน ให้ส่งค่ากลับไป ว่าไม่พบ
                {
                    var myData = new
                    {
                        Status = "0", // ไม่มีรหัสพนักงาน
                        Data = "ไม่พบรหัสพนักงานคนดังกล่าว",
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                LogModel Log = new LogModel(UserId, controllerName, actionName, "Failed", ex.Message, "","","");
                Repository.StoredProcedure.SpBase.SpInsertLog(Log);
                return Json(ex.Message);
            }
        }
        // Insert และ Update
        [HttpPost]
        public ActionResult Insert(PermissionModel model)
        {
            // สร้างตัวแปร
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string mesPermissionCode = model.PermissionCode;
            string mesPMSPermissionCode = model.PMSPermissionCode;
            String Status = "";
            String Message = "";
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                // ดึงข้อมูลและเก็บใส่ model 
                //EmployeeProfile employeeProfile = IdmManager.GetByEmployeeNo(model.UserId);
                UserProfileSessionData ResponseprofileData = new UserProfileSessionData();
                (ResponseprofileData, Status, Message) = getHrPlatform(model.UserId);

                model.TitleName = ResponseprofileData.TitleFullName;
                model.FirstName = ResponseprofileData.FirstName;
                model.LastName = ResponseprofileData.LastName;
                model.BA = ResponseprofileData.BaCode;
                model.CostCenterCode = ResponseprofileData.CostCenterCode;
                model.CreateBy = profileData.Username;
                model.UpdateBy = profileData.Username;
                // ดึงข้อมูลและเก็บใส่ model 

                //เช็คในฐานข้อมูล ว่ารหัสพนักงานดังกล่าวมีในระบบหรือไม่ ถ้าไม่มีทำการ Insert ถ้ามีให้ Update
                List<PermissionModel> admin = new List<PermissionModel>();
                admin = Repository.StoredProcedure.SpBase.SpCheckPermission(model.UserId);
                //เช็คในฐานข้อมูล ว่ารหัสพนักงานดังกล่าวมีในระบบหรือไม่ ถ้าไม่มีทำการ Insert ถ้ามีให้ Update
                if (admin.Count == 0) // ไม่มีในฐานข้อมูล
                {
                    //เรียกใช้ฟังก์ชันให้ Insert สิทธิ์ ลงฐานข้อมูล
                    Message = Repository.StoredProcedure.SpAdmin.SpInsertAdminPermission(model);
                    //เรียกใช้ฟังก์ชัน ทำการดึง Key ที่พึ่งสร้างของบุคคลนั้นส่งไปหน้า View
                    admin = Repository.StoredProcedure.SpBase.SpCheckPermission(model.UserId);
                    //เรียกใช้ฟังก์ชันให้ Insert Log ลงฐานข้อมูล
                    LogModel Log = new LogModel(model.UserId, controllerName, actionName,"Success","เพิ่มสิทธิ์สำเร็จ", model.CreateBy, mesPermissionCode,mesPMSPermissionCode);
                    Repository.StoredProcedure.SpBase.SpInsertLog(Log);

                    var myData = new
                    {
                        Status = "1", // Insert สำเร็จ
                        Message = Message,
                        Key = admin[0].Id,
                    };
                    return Json(myData);
                }
                else // ถ้ามีในฐานข้อมูล ให้ทำการอัปเดต
                {
                    if(admin[0].PermissionCode == "1") // เช็คสิทธิ์ผู้พัฒนา
                    {
                        Message = "ไม่สามารถเปลี่ยนแปลงสิทธิ์ผู้พัฒนาระบบได้";
                        var myData = new
                        {
                            Status = "0", // ไม่สามารถ Update ได้ เพราะเป็นสิทธิ์ผู้พัฒนา
                            Message = Message,
                        };
                        return Json(myData);
                    }
                    else // ถ้าไม่ใช่สิทธิ์ผู้พัฒนา ให้ทำการอัปเดต
                    {
                        //เรียกใช้ฟังก์ชันให้ Update สิทธิ์ ลงฐานข้อมูล
                        model.Id = admin[0].Id;
                        Message = Repository.StoredProcedure.SpAdmin.SpUpdateAdminPermission(model);
                        //เรียกใช้ฟังก์ชันให้ Insert Log ลงฐานข้อมูล
                        LogModel Log = new LogModel(model.UserId, controllerName, actionName, "Success", "อัปเดตสิทธิ์สำเร็จ", model.UpdateBy,mesPermissionCode,mesPMSPermissionCode);
                        Repository.StoredProcedure.SpBase.SpInsertLog(Log);
                        var myData = new
                        {
                            Status = "2", // Update สำเร็จ
                            Message = Message,
                            BA = model.BA
                        };
                        return Json(myData);
                    }
                }
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                LogModel Log = new LogModel(model.UserId, controllerName, actionName, "Failed", ex.Message, model.CreateBy,mesPermissionCode,mesPMSPermissionCode);
                Repository.StoredProcedure.SpBase.SpInsertLog(Log);
                return Json(ex.Message);
            }
        }
        //Delete
        [HttpPost]
        public ActionResult Delete(PermissionModel model)
        {
            // สร้างตัวแปร
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string mesPermissionCode = model.PermissionCode;
            string mesPMSPermissionCode = model.PMSPermissionCode;
            String Message = "";
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                // เก็บข้อมูลรหัสพนักงานคนที่ทำการลบ
                model.DeleteBy = profileData.Username;

                //เช็คในฐานข้อมูล ว่ารหัสพนักงานดังกล่าวมีสิทธิ์เป็นผู้พัฒนาระบบหรือป่าว
                List<PermissionModel> admin = new List<PermissionModel>();
                admin = Repository.StoredProcedure.SpBase.SpCheckPermission(model.UserId);
                //เช็คในฐานข้อมูล ว่ารหัสพนักงานดังกล่าวมีสิทธิ์เป็นผู้พัฒนาระบบหรือป่าว

                if (admin[0].PermissionCode == "1") // เช็คสิทธิ์ผู้พัฒนา
                {
                    var myData = new
                    {
                        Status = "0",
                        Message = "ไม่สามารถลบสิทธิ์ผู้พัฒนาระบบได้",
                    };
                    return Json(myData);
                }
                else // ถ้าสิทธิ์ไม่ใช่ผู้พัฒนาระบบ ให้ทำการลบได้
                {
                    //เรียกใช้ฟังก์ชันให้ Delete สิทธิ์ ลงฐานข้อมูล
                    Message = Repository.StoredProcedure.SpAdmin.SpDeleteAdminPermission(model);
                    //เรียกใช้ฟังก์ชันให้ Insert Log ลงฐานข้อมูล
                    LogModel Log = new LogModel(model.UserId, controllerName, actionName, "Success", "ลบสิทธิ์สำเร็จ", model.DeleteBy, mesPermissionCode, mesPMSPermissionCode);
                    Repository.StoredProcedure.SpBase.SpInsertLog(Log);

                    var myData = new
                    {
                        Status = "1", // ลบสำเร็จ
                        Message = Message,
                    };
                    return Json(myData);
                }
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                LogModel Log = new LogModel(model.UserId, controllerName, actionName, "Failed", ex.Message, model.DeleteBy, mesPermissionCode, mesPMSPermissionCode);
                Repository.StoredProcedure.SpBase.SpInsertLog(Log);
                return Json(ex.Message);
            }
        }

        // ManagetmentSystem onchange การค้นหา
        [HttpPost]
        public ActionResult FilterYear(String TypeCode)
        {
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                String TimeAYear = null;
                if(TypeCode == "3")
                {
                    TimeAYear = "2";
                }
                List<SectionYearModel> YearsModelLists = Repository.StoredProcedure.SpAdmin.SpGetSectionYear(TypeCode).ToList();
                List<TimesAYearsModel> TimesAYearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetTimesAYear(TimeAYear).ToList();
                    
                var myData2 = new
                {
                    Status = "1", // 
                    Year = YearsModelLists,
                    TimesAYears = TimesAYearsModelLists
                };
                return Json(myData2);
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                var myData = new
                {
                    Status = "0", 
                    Message = ex.Message
                };
                return Json(myData);
            }
        }

        [HttpPost]
        public ActionResult InsertMastLock(String TypeID,String Year,String TimesAYear,String Round)
        {
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire
                String ID,Cancel,Status,Message;
                (ID, Cancel, Status, Message) = Repository.StoredProcedure.SpAdmin.SpGetCheckMastLock(TypeID,Year,TimesAYear,Round);

                if(Status == "1") // Update
                {
                    if(Cancel == "0") // กรณีที่เจอ insert การปิดระบบซ้ำ 
                    {
                        Status = "0";
                        Message = "ปิดการลงคะแนนช่วงดังกล่าวแล้ว กรุณาเช็คอีกครั้ง";
                    }
                    else if(Cancel == "1") // แก้ไขข้อมูลที่ถูกยกเลิกไปแล้วให้กับมาใช้ใหม่
                    {
                        (Status, Message) = Repository.StoredProcedure.SpAdmin.SpUpdateMastLock(ID,profileData.Username);
                    }
                }
                else if(Status == "2") // Insert
                {
                    (Status, Message) = Repository.StoredProcedure.SpAdmin.SpInsertMastLock(TypeID, Year, TimesAYear, Round,profileData.Username);
                }
                var myData2 = new
                {
                    Status = Status, // error
                    Message = Message
                };
                return Json(myData2);
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                var myData = new
                {
                    Status = "0", 
                    Message = ex.Message
                };
                return Json(myData);
            }
        }

        [HttpPost]
        public ActionResult DeleteMastLock(String ID)
        {
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire
                String Status, Message;

                (Status, Message) = Repository.StoredProcedure.SpAdmin.SpDeleteMastLock(ID, profileData.Username);

                var myData2 = new
                {
                    Status = Status, // error
                    Message = Message
                };
                return Json(myData2);
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                var myData = new
                {
                    Status = "0", // 
                    Message = ex.Message
                };
                return Json(myData);
            }
        }
    }
}