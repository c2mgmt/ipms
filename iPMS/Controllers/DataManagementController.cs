﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using iPMS.ServiceReference2;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class DataManagementController : BaseController
    {
        // GET: DataManagement
        public ActionResult Index()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            return View();
        }
        public ActionResult Activity()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            // เช็คสิทธิ์ admin
            if (!CheckPermission(profileData.PermissionCode, 1))// สิทธิ์ระดับ 1
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin

            ViewActivityDataManagement viewModel = new ViewActivityDataManagement();
            viewModel.CostCenterModelLists = Repository.StoredProcedure.SpDataManagement.SpGetCostCenter().ToList();
            viewModel.TypeModelLists = Repository.StoredProcedure.SpDataManagement.SpGetType("3").ToList(); //Fix แผนปฏิบัติ
            viewModel.TopicModelLists = Repository.StoredProcedure.SpDataManagement.SpGetTopic("3").ToList();
            viewModel.ActivityModelLists = Repository.StoredProcedure.SpDataManagement.SpGetActivity().ToList();
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpDataManagement.SpGetTimesAYear("2").ToList(); //Fix ไตรมาส
            viewModel.MastPEAModelLists = Repository.StoredProcedure.SpDataManagement.SpGetMastPEA().ToList();
            viewModel.MastSegmentModelLists = Repository.StoredProcedure.SpDataManagement.SpGetSegment().ToList();


            List<YearsModel> result = new List<YearsModel>();
            int CurrYear = DateTime.Now.Year;
            YearsModel model = new YearsModel();
            model.Year = CurrYear.ToString();
            result.Add(model);
            //for (int i = -1;i < 2; i++)
            //{
            //    YearsModel model = new YearsModel();
            //    model.Year = (CurrYear + i).ToString();
            //    result.Add(model);
            //}
            viewModel.YearsModelLists = result.ToList();
            return View(viewModel);
        }
        // Insert และ Update
        [HttpPost]
        public ActionResult CreateMastActivity(ActivityModel model)
        {
            // สร้างตัวแปร
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            String Message = "";
            String PrimaryKey = "";
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                // Insert Mast ใหม่
                if(model.ID == null) {
                    string uniquefile = null;
                    if (model.PostedFile != null) // Insert แบบมีไฟล์แนบ
                    {
                        var fileNamePDF = model.PostedFile.FileName;
                        string[] fileextension = fileNamePDF.Split('.');
                        string FileName = "";
                        for (int i = 0; i < fileextension.Length; i++)
                        {
                            if (i + 1 == fileextension.Length)
                            {
                                break;
                            }
                            FileName = FileName + fileextension[i];
                        }

                        uniquefile = "MA_" + FileName.Replace("'","").Replace("\"","") + DateTime.Now.ToString("yyyyMMdd_hh_mm_ss") + ".pdf";
                        model.TargetFile = uniquefile;
                        var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["PathFilePDFMastAct"]), uniquefile);
                        model.PostedFile.SaveAs(path);

                        model.CreateBy = profileData.Username;

                        PrimaryKey = Repository.StoredProcedure.SpDataManagement.SpInsertMastActivity(model);
                        //String StatusCreate = Repository.StoredProcedure.SpDataManagement.SpInsertMastActivity(model);
                    }
                    else // Insert แบบไม่มีไฟล์แนบ
                    {
                        model.CreateBy = profileData.Username;

                        PrimaryKey = Repository.StoredProcedure.SpDataManagement.SpInsertMastActivity(model);

                        //String StatusCreate = Repository.StoredProcedure.SpDataManagement.SpInsertMastActivity(model);
                    }

                    List<ActivityModel> Data = Repository.StoredProcedure.SpDataManagement.SpGetActivity(null, null, PrimaryKey).ToList();
                    var myDatas = new
                    {
                        Status = "1", // Insert สำเร็จ
                        Message = "เพิ่มข้อมูลสำเร็จ",
                        Data = Data,
                    };
                    return Json(myDatas);
                }
                else // Update Mast เงื่อนไขคือ ID มีค่าส่งกลับมา
                {
                    if (model.PostedFile != null) // Update แบบมีไฟล์แนบ
                    {
                        String NewFileName = "";
                        if(model.TargetFile == null)
                        {
                            var fileNamePDF = model.PostedFile.FileName;
                            string[] fileextension = fileNamePDF.Split('.');
                            string FileName = "";
                            for (int i = 0; i < fileextension.Length; i++)
                            {
                                if (i + 1 == fileextension.Length)
                                {
                                    break;
                                }
                                FileName = FileName + fileextension[i];
                            }

                            NewFileName = "MA_" + FileName.Replace("'", "").Replace("\"", "") + DateTime.Now.ToString("yyyyMMdd_hh_mm_ss") + ".pdf";
                            model.TargetFile = NewFileName;
                        }
                        var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["PathFilePDFMastAct"]), model.TargetFile);
                        model.PostedFile.SaveAs(path);

                        model.UpdateBy = profileData.Username;

                        Message = Repository.StoredProcedure.SpDataManagement.SpUpdateMastActivity(model);
                        //String StatusCreate = Repository.StoredProcedure.SpDataManagement.SpInsertMastActivity(model);
                    }
                    else // Update แบบไม่มีไฟล์แนบ
                    {
                        model.UpdateBy = profileData.Username;

                        Message = Repository.StoredProcedure.SpDataManagement.SpUpdateMastActivity(model);

                        //String StatusCreate = Repository.StoredProcedure.SpDataManagement.SpInsertMastActivity(model);
                    }
                    List<ActivityModel> Data = Repository.StoredProcedure.SpDataManagement.SpGetActivity(null, null, model.ID).ToList();
                    var myDatas = new
                    {
                        Status = "2", // Update สำเร็จ
                        Message = Message,
                        Data = Data,
                    };
                    return Json(myDatas);

                }
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                //LogModel Log = new LogModel(model.UserId, controllerName, actionName, "Failed", ex.Message, model.CreateBy);
                //Repository.StoredProcedure.SpBase.SpInsertLog(Log);
                var myData = new
                {
                    Status = "0", // Error
                    Message = ex.Message,
                };
                return Json(myData);
            }
        }
        [HttpPost]
        public ActionResult GetRunning(String TypeCode,String TopicCode)
        {
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                List<ActivityModel> ActivityModelLists = Repository.StoredProcedure.SpDataManagement.SpGetActivity(TypeCode, TopicCode).ToList();
                int Count = ActivityModelLists.Count;
                var myData2 = new
                {
                    Status = "1", // Get สำเร็จ
                    Data = Count
                };
                return Json(myData2);
            }
            catch(Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }
        [HttpPost]
        public ActionResult GetMastAct(String ID)
        {
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                List<ActivityModel> ActivityModelLists = Repository.StoredProcedure.SpDataManagement.SpGetActivity(null, null,ID).ToList();
                var myData2 = new
                {
                    Status = "1", // Get สำเร็จ
                    Message = "ดึงข้อมูลสำเร็จ",
                    Data = ActivityModelLists
                };
                return Json(myData2);
            }
            catch (Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }
        [HttpPost]
        public ActionResult DeleteMastAct(String ID)
        {
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                String Message = Repository.StoredProcedure.SpDataManagement.SpDeleteMastActivity(ID, profileData.Username);
                var myData2 = new
                {
                    Status = "1", // ลบ สำเร็จ
                    Message = Message,
                };
                return Json(myData2);
            }
            catch (Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }

        [HttpPost]
        public ActionResult CreateSection(SectionModel model) // โยนหัวข้อเข้าการไฟฟ้า
        {
            // สร้างตัวแปร
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            String Message = "";
            String PrimaryKey = "";
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                // Update Code Version 2024 02 27
                string[] authorsList = model.GroupPEAGroup[0].Split(',');
                model.GroupPEAGroup = new List<string>(authorsList);
                String CheckStatus = "";
                List<String> InsertGroup = new List<string>();
                List<String> RepeatedlyGroup = new List<string>();
                for (int index = 0; index < model.GroupPEAGroup.Count; index++)
                {
                    // เช็ค Section ซ้ำหรือไม่
                    List<SectionModel> Doublecheck = Repository.StoredProcedure.SpDataManagement.SpGetDoubleCheckSection(model,model.GroupPEAGroup[index]);
                    if (Doublecheck.Count == 0) // ไม่ซ้ำ เตรียมการบันทึกลง Database
                    {
                        InsertGroup.Add(model.GroupPEAGroup[index]); // เพิ่มลง Array เพื่อนำค่าไปแจ้งผู้ใช้งานบนหน้าเว็บ
                        //ดึงค่าจากมาสเตอร์ มาลง โมเดล และไป บันทึกลง Table Section 
                        model = Repository.StoredProcedure.SpDataManagement.SpSetSection(model);
                        model.CreateBy = profileData.Username;

                        //ดึง จำนวนรอบการวนลูป -> 1
                        String IDTimeAYears = Mapper.MapDoubleToInteger(model.NameTimesAYear).ToString();
                        List<TimesAYearsModel> TimesAYearsModelLists = Repository.StoredProcedure.SpDataManagement.SpGetTimesAYear(IDTimeAYears).ToList();
                        int TimeLoop = Mapper.MapStringToInteger(TimesAYearsModelLists[0].TimesAYear);

                        //ดึงการไฟฟ้า ที่่ต้องทำการสร้างตารางให้คะแนน -> 2
                        //String TypePEA = model.PEAGroup;
                        String TypePEA = model.GroupPEAGroup[index];
                        List<PEAOfficeModel> PEAOfficeModelLists = Repository.StoredProcedure.SpDataManagement.SpGetPEAOffice(TypePEA).ToList();

                        //สร้าง Header Section และดึง GroupKey
                        String Status = "";
                        String SectionKey = "";
                        String GroupSectionKey = "";
                        model.GroupSection = "";
                        model.ModeScore = "1"; // Mode 1 คือ การให้คะแนนจะมีลำดับเป็น P, NT , N/A
                        for (int i = 1; i <= TimeLoop; i++)
                        {
                            model.Round = i;
                            (Status, SectionKey, GroupSectionKey) = Repository.StoredProcedure.SpDataManagement.SpInsertSection(model, model.GroupPEAGroup[index]);
                            if (Status == "0")
                            {
                                break;
                            }
                            model.GroupSection = GroupSectionKey;
                            for (int j = 0; j < PEAOfficeModelLists.Count; j++)
                            {
                                ResultModel score = new ResultModel();
                                score.ForeignSection = SectionKey;
                                score.Round = model.Round; // รอบที่เท่าไหร่
                                score.TRSG = PEAOfficeModelLists[j].TRSG;
                                Repository.StoredProcedure.SpDataManagement.SpInsertResult(score);
                            }
                        }
                        if (Status == "0") // Error
                        {
                            var myDatasError = new
                            {
                                Status = Status, // Error 
                                Message = SectionKey,
                                //Data = Data,
                            };
                            return Json(myDatasError);
                        }
                        // ยังไม่สมบูรณ์ การแสดงข้อความ

                        // สร้างตาราง ผู้บริหาร
                        List<MgtConfirmModel> CheckMgtModelLists = new List<MgtConfirmModel>();
                        (CheckStatus, Message, CheckMgtModelLists) = Repository.StoredProcedure.SpDataManagement.SpCheckMgtConfirm(model.Year);
                        if (CheckStatus == "1")
                        {
                            if (CheckMgtModelLists.Count == 0)
                            {
                                String Min = "1"; // ปรับโครงสร้างใหม่ เพิ่มเติม กฟจ. 2024 02 27
                                String Max = "5"; // 
                                String TimesAYearID = model.NameTimesAYear.ToString();
                                List<PEAOfficeModel> GroupPEAModelLists = Repository.StoredProcedure.SpDataManagement.SpGetPEAGroupOffice(Min, Max);
                                MgtConfirmModel PackModel = new MgtConfirmModel();
                                PackModel.Year = model.Year;
                                PackModel.TimesAYearID = TimesAYearID;
                                for (int i = 1; i <= TimeLoop; i++)
                                {
                                    if (CheckStatus == "0")
                                    {
                                        break;
                                    }
                                    PackModel.Round = i;
                                    for (int List = 0; List < GroupPEAModelLists.Count; List++)
                                    {
                                        PackModel.TRSG = GroupPEAModelLists[List].TRSG;
                                        (CheckStatus, Message) = Repository.StoredProcedure.SpDataManagement.SpInsertMgtConfirm(PackModel);
                                    }
                                }
                                //var myDatas = new
                                //{
                                //    Status = CheckStatus, // Insert สำเร็จ
                                //    Message = Message,
                                //    //Data = Data,
                                //};
                                //return Json(myDatas);
                            }
                        }
                        else
                        {
                            var myDatasError = new
                            {
                                Status = CheckStatus, // Error 
                                Message = Message,
                            };
                            return Json(myDatasError);
                        }
                        //
                    }
                    else // ซ้ำ
                    {
                        RepeatedlyGroup.Add(model.GroupPEAGroup[index]); // เพิ่มลง Array เพื่อนำค่าไปแจ้งผู้ใช้งานบนหน้าเว็บ
                        //var CheckmyData = new
                        //{
                        //    Status = "3", // Error 
                        //    Message = "ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมีข้อมูลนี้ในระบบแล้ว",
                        //    //Data = Data,
                        //};
                        //return Json(CheckmyData);
                    }
                }
                if(model.GroupPEAGroup.Count == RepeatedlyGroup.Count)
                {
                    CheckStatus = "3";
                    Message = "ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมีข้อมูลนี้ในระบบหมดแล้ว";
                }
                var myDatas2 = new
                {
                    Status = CheckStatus, // Insert สำเร็จ
                    Message = Message,
                    InsertGroup = InsertGroup,
                    RepeatedlyGroup = RepeatedlyGroup,
                    //Data = Data,
                };
                return Json(myDatas2);

                // END Update Code Version 2024 02 27


                // เช็ค Section ซ้ำหรือไม่ --Version เก่า
                //List<SectionModel> Doublecheck = Repository.StoredProcedure.SpDataManagement.SpGetDoubleCheckSection(model);
                //if (Doublecheck.Count > 0)
                //{
                //    var CheckmyData = new
                //    {
                //        Status = "3", // Error 
                //        Message = "ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมีข้อมูลนี้ในระบบแล้ว",
                //        //Data = Data,
                //    };
                //    return Json(CheckmyData);
                //}

                ////ดึงค่าจากมาสเตอร์ มาลง โมเดล และไป บันทึกลง Table Section 
                //model = Repository.StoredProcedure.SpDataManagement.SpSetSection(model);
                //model.CreateBy = profileData.Username;

                ////ดึง จำนวนรอบการวนลูป -> 1
                //String IDTimeAYears = Mapper.MapDoubleToInteger(model.NameTimesAYear).ToString();
                //List<TimesAYearsModel> TimesAYearsModelLists = Repository.StoredProcedure.SpDataManagement.SpGetTimesAYear(IDTimeAYears).ToList();
                //int TimeLoop = Mapper.MapStringToInteger(TimesAYearsModelLists[0].TimesAYear);

                ////ดึงการไฟฟ้า ที่่ต้องทำการสร้างตารางให้คะแนน -> 2
                //String TypePEA = model.PEAGroup;
                //List<PEAOfficeModel> PEAOfficeModelLists = Repository.StoredProcedure.SpDataManagement.SpGetPEAOffice(TypePEA).ToList();

                ////สร้าง Header Section และดึง GroupKey
                //String Status = "";
                //String SectionKey = "";
                //String GroupSectionKey = "";
                //model.ModeScore = "1"; // Mode 1 คือ การให้คะแนนจะมีลำดับเป็น P, NT , N/A
                //for(int i = 1;i <= TimeLoop;i++)
                //{
                //    model.Round = i;
                //    (Status, SectionKey, GroupSectionKey) = Repository.StoredProcedure.SpDataManagement.SpInsertSection(model);
                //    if(Status == "0")
                //    {
                //        break;
                //    }
                //    model.GroupSection = GroupSectionKey;
                //    for (int j = 0; j < PEAOfficeModelLists.Count; j++)
                //    {
                //        ResultModel score = new ResultModel();
                //        score.ForeignSection = SectionKey;
                //        score.Round = model.Round; // รอบที่เท่าไหร่
                //        score.TRSG = PEAOfficeModelLists[j].TRSG;
                //        Repository.StoredProcedure.SpDataManagement.SpInsertResult(score);
                //    }
                //}
                //if(Status == "0") // Error
                //{
                //    var myDatasError = new
                //    {
                //        Status = Status, // Error 
                //        Message = SectionKey,
                //        //Data = Data,
                //    };
                //    return Json(myDatasError); 
                //}
                //// ยังไม่สมบูรณ์ การแสดงข้อความ

                //// สร้างตาราง ผู้บริหาร
                //String CheckStatus = "";
                //List<MgtConfirmModel> CheckMgtModelLists = new List<MgtConfirmModel>();
                //(CheckStatus, Message, CheckMgtModelLists) = Repository.StoredProcedure.SpDataManagement.SpCheckMgtConfirm(model.Year);
                //if(CheckStatus == "1")
                //{
                //    if(CheckMgtModelLists.Count == 0)
                //    {
                //        String Min = "1"; // ปรับโครงสร้างใหม่ เพิ่มเติม กฟจ. 2024 02 27
                //        String Max = "5"; // 
                //        String TimesAYearID = model.NameTimesAYear.ToString();
                //        List<PEAOfficeModel> GroupPEAModelLists = Repository.StoredProcedure.SpDataManagement.SpGetPEAGroupOffice(Min,Max);
                //        MgtConfirmModel PackModel = new MgtConfirmModel();
                //        PackModel.Year = model.Year;
                //        PackModel.TimesAYearID = TimesAYearID;
                //        for (int i = 1;i <= TimeLoop; i++)
                //        {
                //            if(CheckStatus == "0")
                //            {
                //                break;
                //            }
                //            PackModel.Round = i;
                //            for(int List = 0;List < GroupPEAModelLists.Count; List++)
                //            {
                //                PackModel.TRSG = GroupPEAModelLists[List].TRSG;
                //                (CheckStatus, Message) = Repository.StoredProcedure.SpDataManagement.SpInsertMgtConfirm(PackModel);
                //            }
                //        }
                //        var myDatas = new
                //        {
                //            Status = CheckStatus, // Insert สำเร็จ
                //            Message = Message,
                //            //Data = Data,
                //        };
                //        return Json(myDatas);
                //    }
                //}
                //else
                //{
                //    var myDatasError = new
                //    {
                //        Status = CheckStatus, // Error 
                //        Message = Message,
                //    };
                //    return Json(myDatasError);
                //}
                //

                //var myDatas2 = new
                //{
                //    Status = CheckStatus, // Insert สำเร็จ
                //    Message = Message,
                //    //Data = Data,
                //};
                //return Json(myDatas2);
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                //LogModel Log = new LogModel(model.UserId, controllerName, actionName, "Failed", ex.Message, model.CreateBy);
                //Repository.StoredProcedure.SpBase.SpInsertLog(Log);
                var myData = new
                {
                    Status = "0", // Error
                    Message = ex.Message,
                };
                return Json(myData);
            }
        }

        // Archive การอัปโหลดไฟล์เอกสาร
        public ActionResult Archive()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            // เช็คสิทธิ์ admin
            if (!CheckPermission(profileData.PermissionCode, 2))// สิทธิ์ระดับ 2
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin

            String TypeID = "3"; // แผนงานปฏิบัติ
            String TimesAYear = "2"; // ทุกไตรมาส

            ViewArchiveDataManagement viewModel = new ViewArchiveDataManagement();

            viewModel.ArchiveModelLists = Repository.StoredProcedure.SpDataManagement.SpGetArchive().ToList();
            viewModel.TypeModelLists = Repository.StoredProcedure.SpDataManagement.SpGetType(TypeID).ToList();
            viewModel.CabinetCategoryModelLists = Repository.StoredProcedure.SpDataManagement.SpGetCabinetCategory(TypeID).ToList();
            viewModel.TopicModelLists = Repository.StoredProcedure.SpDataManagement.SpGetTopic("3").ToList();
            viewModel.PEAAnalyticsModelLists = Repository.StoredProcedure.SpDataManagement.SpGetPEAAnalytics().ToList();
            viewModel.NameProvincePEAModelLists = Repository.StoredProcedure.SpDataManagement.SpGetProvincialPEAOffice().ToList();
            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpDataManagement.SpGetTimesAYear(TimesAYear).ToList();


            List<YearsModel> result = new List<YearsModel>();
            int CurrYear = DateTime.Now.Year;
            YearsModel model = new YearsModel();
            model.Year = CurrYear.ToString();
            result.Add(model);
            //for (int i = -1;i < 2; i++)
            //{
            //    YearsModel model = new YearsModel();
            //    model.Year = (CurrYear + i).ToString();
            //    result.Add(model);
            //}
            viewModel.YearsModelLists = result.ToList();
            return View(viewModel);
        }
        // Insert และ Update

        public String SaveFileAP(ArchiveModel model)
        {
            string uniquefile = null;
            var fileNamePDF = model.PostedFile.FileName;
            string[] fileextension = fileNamePDF.Split('.');
            string FileName = "";
            for (int i = 0; i < fileextension.Length; i++)
            {
                if (i + 1 == fileextension.Length)
                {
                    break;
                }
                FileName = FileName + fileextension[i];
            }
            if (model.CabinetID == "1")
            {
                uniquefile = "นิยามแผนปฏิบัติการ" + model.Year + model.TopicID;
                uniquefile += "." + fileextension[fileextension.Length - 1];
            }
            else
            {
                uniquefile = "วิเคราะห์ผลดำเนินงาน" + model.Year + model.PEAID + model.PEAOfficeID + model.TimesAYearID + model.Quarter;
                uniquefile += "." + fileextension[fileextension.Length - 1];
            }
            model.FileName = uniquefile;
            var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["PathFileArchive"]), uniquefile);
            model.PostedFile.SaveAs(path);
            return uniquefile;
        }

        [HttpPost]
        public ActionResult UploadArchive(ArchiveModel model)
        {
            // สร้างตัวแปร
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            String Message = "";
            String Status = "";
            String PrimaryKey = "";
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                }
                // เช็ค session expire

                // เช็ค เป็นหัวข้อไหน เพื่อจะส่งพารามิเตอร์ถูก
                List<ArchiveModel> IsNew = new List<ArchiveModel>();
                
                // Insert Mast ใหม่
                if (model.ID == null)
                {
                    if (model.CabinetID == "1")
                    {
                        IsNew = Repository.StoredProcedure.SpDataManagement
                        .SpGetArchive(null, model.TypeID, model.CabinetID
                        , model.TopicID, model.Year, null
                        , null, null).ToList();
                    }
                    else if (model.CabinetID == "2")
                    {
                        IsNew = Repository.StoredProcedure.SpDataManagement
                        .SpGetArchive(null, model.TypeID, model.CabinetID
                        , null, model.Year, model.PEAID, model.PEAOfficeID
                        , model.TimesAYearID, model.Quarter).ToList();
                    }

                    if (IsNew.Count == 0) { // Insert ใหม่
                        string uniquefile = null;
                        if (model.PostedFile != null) // Insert แบบมีไฟล์แนบ
                        {
                            uniquefile = SaveFileAP(model);
                            model.FileName = uniquefile;
                            model.CreateBy = profileData.Username;


                            if (model.CabinetID == "1") // แผนปฏิบัติ
                                (Status, Message) = Repository.StoredProcedure.SpDataManagement.SpInsertArchiveAP(model);
                            else if (model.CabinetID == "2") //วิเคราะห์
                                (Status, Message) = Repository.StoredProcedure.SpDataManagement.SpInsertArchiveAnalytics(model);


                            var myData = new
                            {
                                Status = Status, // Insert สำเร็จ
                                Message = Message,
                            };
                            return Json(myData);
                        }
                        else // ไม่มีไฟล์แนบ
                        {
                            var myData = new
                            {
                                Status = "0", // Error ไม่มีไฟล์
                                Message = "ไม่พบไฟล์เอกสารที่แนบมา กรุณาลองใหม่อีกครั้ง",
                            };
                            return Json(myData);
                        }
                    }
                    else // insert แต่มีข้อมูลแล้ว ให้ทำการ update
                    {
                        string uniquefile = null;
                        if (model.PostedFile != null) // update แบบมีไฟล์แนบ
                        {
                            uniquefile = SaveFileAP(model);
                            model.ID = IsNew[0].ID;
                            model.FileName = uniquefile;
                            model.UpdateBy = profileData.Username;

                            if(model.CabinetID == "1")
                                (Status, Message) = Repository.StoredProcedure.SpDataManagement.SpUpdateArchiveAP(model);
                            else if(model.CabinetID == "2")
                                (Status, Message) = Repository.StoredProcedure.SpDataManagement.SpUpdateArchiveAnalytics(model);


                            var myData = new
                            {
                                ID = model.ID,
                                FileName = model.FileName,
                                Status = Status, // Insert สำเร็จ
                                Message = Message,
                            };
                            return Json(myData);
                        }
                        else // ไม่มีไฟล์แนบ
                        {
                            var myData = new
                            {
                                Status = "0", // Error ไม่มีไฟล์
                                Message = "ไม่พบไฟล์เอกสารที่แนบมา กรุณาลองใหม่อีกครั้ง",
                            };
                            return Json(myData);
                        }
                    }
                }
                else // Update Mast เงื่อนไขคือ ID มีค่าส่งกลับมา
                {
                    string uniquefile = null;
                    if (model.PostedFile != null) // update แบบมีไฟล์แนบ
                    {
                        IsNew = Repository.StoredProcedure.SpDataManagement.SpGetArchive(model.ID).ToList();
                        IsNew[0].PostedFile = model.PostedFile;
                        uniquefile = SaveFileAP(IsNew[0]);
                        model.FileName = uniquefile;
                        model.UpdateBy = profileData.Username;

                        if (IsNew[0].CabinetID == "1")
                            (Status, Message) = Repository.StoredProcedure.SpDataManagement.SpUpdateArchiveAP(model);
                        else if (IsNew[0].CabinetID == "2")
                            (Status, Message) = Repository.StoredProcedure.SpDataManagement.SpUpdateArchiveAnalytics(model);

                        var myData = new
                        {
                            ID = model.ID,
                            FileName = model.FileName,
                            CabinetID = IsNew[0].CabinetID,
                            Status = Status, // Insert สำเร็จ
                            Message = Message,
                        };
                        return Json(myData);
                    }
                    else // ไม่มีไฟล์แนบ
                    {
                        var myData = new
                        {
                            Status = "0", // Error ไม่มีไฟล์
                            Message = "ไม่พบไฟล์เอกสารที่แนบมา กรุณาลองใหม่อีกครั้ง",
                        };
                        return Json(myData);
                    }
                }
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                //LogModel Log = new LogModel(model.UserId, controllerName, actionName, "Failed", ex.Message, model.CreateBy);
                //Repository.StoredProcedure.SpBase.SpInsertLog(Log);
                var myData = new
                {
                    Status = "0", // Error
                    Message = ex.Message,
                };
                return Json(myData);
            }
        }

        [HttpPost]
        public ActionResult DeleteArchive(String ID)
        {
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                String Message = Repository.StoredProcedure.SpDataManagement.SpDeleteArchive(ID, profileData.Username);
                var myData2 = new
                {
                    Status = "1", // ลบ สำเร็จ
                    Message = Message,
                };
                return Json(myData2);
            }
            catch (Exception ex)
            {
                var myData2 = new
                {
                    Status = "0", // Error
                    Message = ex.Message
                };
                return Json(myData2);
            }
        }

    }
}