﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class ExecutiveController : BaseController
    {
        // GET: Executive
        private String getRound = GetQuarter();
        private static String GetQuarter() //แสดงค่าไตรมาส ณ ปัจจุบัน
        {
            DateTime date = DateTime.Now;
            if (date.Month >= 4 && date.Month <= 6)
                return "1";
            else if (date.Month >= 7 && date.Month <= 9)
                return "2";
            else if (date.Month >= 10 && date.Month <= 12)
                return "3";
            else
                return "4";
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Acknowledge()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            // เช็คสิทธิ์ admin และ ผู้บริหาร
            if (!CheckPermission(profileData.PermissionCode, 4))// สิทธิ์ระดับ 4 ผู้บริหาร
            {
                MessageManager.SetErrorMessage("ระดับสิทธิ์ของคุณไม่เพียงพอที่จะใช้ฟังก์ชันดังกล่าว");
                return RedirectToAction("Index", "Home");
            }
            // เช็คสิทธิ์ admin และ ผู้บริหาร

            // ประกาศตัวแปร
            String Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน
            String TypeID = "3"; // แผนปฏิบัติ
            String TimesAYearID = "2"; // ไตรมาส
            String Round = getRound; // ไตรมาสของเดือนปัจจุบัน
            ViewExecutiveAckModel viewModel = new ViewExecutiveAckModel();
            viewModel.YearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetSectionYear(TypeID).ToList();

            // ดึงปี ล่าสุด ของ DB
            if(viewModel.YearsModelLists.Count > 0)
            {
                Year = viewModel.YearsModelLists[0].Year;
            }

            viewModel.TimesAYearsModelLists = Repository.StoredProcedure.SpReportResult.SpGetTimesAYear(TimesAYearID).ToList();

            viewModel.DirectorModelLists = Repository.StoredProcedure.SpExecutive.SpGetMgtDirector(Year, TimesAYearID, Round).ToList(); 

            viewModel.PEAModelLists = Repository.StoredProcedure.SpExecutive.SpGetMgtConfirm(Year, TimesAYearID, Round).ToList();

            List<MgtCountingResultModel> result99 = Repository.StoredProcedure.SpExecutive.SpGetCountingResult99(Year, TimesAYearID, Round).ToList();
            List<MgtCountingResultModel> result1 = Repository.StoredProcedure.SpExecutive.SpGetCountingResult1(Year, TimesAYearID, Round).ToList();

            int countIndex = 0;
            int countMaxIndex = result99.Count;
            for (int index = 0;index < viewModel.PEAModelLists.Count; index++)
            {
                for(int direct = 0;direct < viewModel.DirectorModelLists.Count; direct++)
                {
                    MgtCountingResultModel store = new MgtCountingResultModel();
                    if (countIndex == countMaxIndex)
                    {
                        store.ResultNull = "0";
                        store.Counting99 = "0";
                        store.Total = "0";
                        viewModel.PEAModelLists[index].score.Add(store);
                    }
                    else if(viewModel.DirectorModelLists[direct].TRSG == result99[countIndex].MastTRSG
                        && viewModel.PEAModelLists[index].TRSG == result99[countIndex].TRSG)
                    {
                        store.ID = result99[countIndex].ID;
                        store.CostCenterName = result99[countIndex].CostCenterName;
                        store.MastTRSG = result99[countIndex].MastTRSG;
                        store.BA = result99[countIndex].BA;
                        store.TRSG = result99[countIndex].TRSG;
                        store.PEA_OfficeNAME = result99[countIndex].PEA_OfficeNAME;
                        store.ResultNull = result99[countIndex].ResultNull;
                        store.Counting99 = result99[countIndex].Counting99;
                        store.Total = result99[countIndex].Total;
                        viewModel.PEAModelLists[index].score.Add(store);
                        countIndex++;
                    }
                    else
                    {
                        store.ResultNull = "0";
                        store.Counting99 = "0";
                        store.Total = "0";
                        viewModel.PEAModelLists[index].score.Add(store);
                    }
                }
            }

            countIndex = 0;
            countMaxIndex = result1.Count;
            for (int index = 0; index < viewModel.PEAModelLists.Count; index++)
            {
                for (int direct = 0; direct < viewModel.DirectorModelLists.Count; direct++)
                {
                    if(countIndex == countMaxIndex)
                    {
                        viewModel.PEAModelLists[index].score[direct].Counting = "0";
                    }
                    else if (viewModel.DirectorModelLists[direct].TRSG == result1[countIndex].MastTRSG
                        && viewModel.PEAModelLists[index].score[direct].TRSG == result1[countIndex].TRSG)
                    {
                        viewModel.PEAModelLists[index].score[direct].Counting = result1[countIndex].Counting;
                        countIndex++;
                    }
                    else
                    {
                        viewModel.PEAModelLists[index].score[direct].Counting = "0";
                    }
                }
            }

            ViewData["Year"] = Year;
            ViewData["NameTimesAYear"] = TimesAYearID;
            ViewData["Round"] = Round;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult FilterYear(String Year, String NameTimesAYear, String Round)
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
            }
            // เช็ค session expire

            try
            {
                ViewExecutiveAckModel viewModel = new ViewExecutiveAckModel();
                viewModel.DirectorModelLists = Repository.StoredProcedure.SpExecutive.SpGetMgtDirector(Year, NameTimesAYear, Round).ToList();
                viewModel.PEAModelLists = Repository.StoredProcedure.SpExecutive.SpGetMgtConfirm(Year, NameTimesAYear, Round).ToList();

                List<MgtCountingResultModel> result99 = Repository.StoredProcedure.SpExecutive.SpGetCountingResult99(Year, NameTimesAYear, Round).ToList();
                List<MgtCountingResultModel> result1 = Repository.StoredProcedure.SpExecutive.SpGetCountingResult1(Year, NameTimesAYear, Round).ToList();

                int countIndex = 0;
                int countMaxIndex = result99.Count;
                for (int index = 0; index < viewModel.PEAModelLists.Count; index++)
                {
                    for (int direct = 0; direct < viewModel.DirectorModelLists.Count; direct++)
                    {
                        MgtCountingResultModel store = new MgtCountingResultModel();
                        if (countIndex == countMaxIndex)
                        {
                            store.ResultNull = "0";
                            store.Counting99 = "0";
                            store.Total = "0";
                            viewModel.PEAModelLists[index].score.Add(store);
                        }
                        else if (viewModel.DirectorModelLists[direct].TRSG == result99[countIndex].MastTRSG)
                        {
                            store.ID = result99[countIndex].ID;
                            store.CostCenterName = result99[countIndex].CostCenterName;
                            store.MastTRSG = result99[countIndex].MastTRSG;
                            store.BA = result99[countIndex].BA;
                            store.TRSG = result99[countIndex].TRSG;
                            store.PEA_OfficeNAME = result99[countIndex].PEA_OfficeNAME;
                            store.ResultNull = result99[countIndex].ResultNull;
                            store.Counting99 = result99[countIndex].Counting99;
                            store.Total = result99[countIndex].Total;
                            viewModel.PEAModelLists[index].score.Add(store);
                            countIndex++;
                        }
                        else
                        {
                            store.ResultNull = "0";
                            store.Counting99 = "0";
                            store.Total = "0";
                            viewModel.PEAModelLists[index].score.Add(store);
                        }
                    }
                }

                countIndex = 0;
                countMaxIndex = result1.Count;
                for (int index = 0; index < viewModel.PEAModelLists.Count; index++)
                {
                    for (int direct = 0; direct < viewModel.DirectorModelLists.Count; direct++)
                    {
                        if (countIndex == countMaxIndex)
                        {
                            viewModel.PEAModelLists[index].score[direct].Counting = "0";
                        }
                        else if (viewModel.DirectorModelLists[direct].TRSG == result1[countIndex].MastTRSG)
                        {
                            viewModel.PEAModelLists[index].score[direct].Counting = result1[countIndex].Counting;
                            countIndex++;
                        }
                        else
                        {
                            viewModel.PEAModelLists[index].score[direct].Counting = "0";
                        }
                    }
                }
                var myData = new
                {
                    Status = "1",
                    Message = "ดึงข้อมูลสำเร็จ",
                    Data = viewModel,
                    Column = "3"
                };
                return Json(myData);

            }
            catch(Exception e)
            {
                var myData = new
                {
                    Status = "0",
                    Message = e.Message
                };
                return Json(myData);
            }
        }
        [HttpPost]
        public ActionResult GetDataResult(String Year,String NameTimesAYear,String Round,String TRSG, String CostCenterID)
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
            }
            // เช็ค session expire

            String Status, Message = "";
            List<ExecutiveSummaryModel> model;
            (model, Status, Message) = Repository.StoredProcedure.SpExecutive.SpGetExecutiveSummary(Year,NameTimesAYear
            ,Round,TRSG,CostCenterID);

            if (Status == "1")
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message,
                    Data = model
                };
                return Json(myData);
            }
            else
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message
                };
                return Json(myData);
            }
        }


        [HttpPost]
        public ActionResult confirmMgtDirector(String ID, String TRSG, String Year, String NameTimesAYear, String Round)
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
            }
            // เช็ค session expire

            // เช็ค ช่วงเวลาในการกดยืนยัน
            String IDLock, Cancel, Status, Message = "";
            String TypeID = "3";
            (IDLock, Cancel, Status, Message) = Repository.StoredProcedure.SpAdmin.SpGetCheckMastLock(TypeID, Year, NameTimesAYear, Round);
            if(Status == "0")
            {
                var myData44 = new
                {
                    Status = "0",  // Error
                    Message = Message
                };
                return Json(myData44);
            }
            if (IDLock == "")
            {
                var myData44 = new
                {
                    Status = "0",  // Error
                    Message = "ยังไม่สามารถยืนยันผลดำเนินการได้ กรุณารอจนถึงปิดการให้คะแนนก่อน"
                };
                return Json(myData44);
            }
            //เช็ค ช่วงเวลาในการกดยืนยัน

            //เช็ค มีหัวข้อกิจกรรมที่ยังไม่ได้รับการประเมินหรือไม่

            String CheckDetCountingResultNull, MessageError;
            (CheckDetCountingResultNull, MessageError) = Repository.StoredProcedure.SpExecutive.SpDetCountingResultNull(Year, NameTimesAYear, Round,profileData.Mast_TRSG);

            if(CheckDetCountingResultNull != "0" && MessageError == "0")
            {
                var myData = new
                {
                    Status = "0",  // Error
                    Message = "มีหัวข้อกิจกรรมบางข้อยังไม่ได้รับการประเมิน กรุณาประเมินให้เสร็จก่อน"
                };
                return Json(myData);
            }
            if(MessageError == "1")
            {
                var myData = new
                {
                    Status = "0",  // Error
                    Message = MessageError
                };
                return Json(myData);
            }
            //เช็ค มีหัวข้อกิจกรรมที่ยังไม่ได้รับการประเมินหรือไม่

            (Status, Message) = Repository.StoredProcedure.SpExecutive.SpUpdateMgtManager(ID, profileData.Username,"0");
            List<MgtConfirmModel> data = Repository.StoredProcedure.SpExecutive.SpGetMgtConfirmByID(ID);
            if (Status == "1")
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message,
                    Column = "3",
                    Data = data
                };
                return Json(myData);
            }
            else
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message
                };
                return Json(myData);
            }
        }
        [HttpPost]
        public ActionResult confirmMgtManager(String ID, String TRSG, String Year, String NameTimesAYear, String Round, String Refuse,String MessageReq)
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
            }
            // เช็ค session expire

            // เช็ค ช่วงเวลาในการกดยืนยัน
            String IDLock, Cancel, Status, Message = "";
            String TypeID = "3";
            (IDLock, Cancel, Status, Message) = Repository.StoredProcedure.SpAdmin.SpGetCheckMastLock(TypeID, Year, NameTimesAYear, Round);
            if (Status == "0")
            {
                var myData44 = new
                {
                    Status = "0",  // Error
                    Message = Message
                };
                return Json(myData44);
            }
            if (IDLock == "")
            {
                var myData44 = new
                {
                    Status = "0",  // Error
                    Message = "ยังไม่สามารถยืนยันผลดำเนินการได้ กรุณารอจนถึงปิดการให้คะแนนก่อน"
                };
                return Json(myData44);
            }
            //เช็ค ช่วงเวลาในการกดยืนยัน

            (Status, Message) = Repository.StoredProcedure.SpExecutive.SpUpdateMgtManager(ID,profileData.Username,Refuse,MessageReq);
            List<MgtConfirmModel> data = Repository.StoredProcedure.SpExecutive.SpGetMgtConfirmByID(ID);

            if (Status == "1")
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message,
                    Column = "3",
                    Data = data
                };
                return Json(myData);
            }
            else
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message
                };
                return Json(myData);
            }
        }
        [HttpPost]
        public ActionResult confirmDelete(String ID, String TRSG)
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
            }
            // เช็ค session expire

            String Status, Message = "";
            (Status, Message) = Repository.StoredProcedure.SpExecutive.SpUpdateMgtByAdmin(ID);

            if (Status == "1")
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message,
                    Column = "3"
                };
                return Json(myData);
            }
            else
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message
                };
                return Json(myData);
            }
        }
        [HttpPost]
        public ActionResult confirmDeleteManager(String ID, String TRSG)
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
            }
            // เช็ค session expire

            String Status, Message = "";
            (Status, Message) = Repository.StoredProcedure.SpExecutive.SpUpdateMgtByAdmin(ID);

            if (Status == "1")
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message,
                    Column = "3"
                };
                return Json(myData);
            }
            else
            {
                var myData = new
                {
                    Status = Status,
                    Message = Message
                };
                return Json(myData);
            }
        }
    }
}