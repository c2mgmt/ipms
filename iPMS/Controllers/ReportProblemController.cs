﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class ReportProblemController : BaseController
    {
        // GET: ReportProblem
        public ActionResult Index()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            ViewReportProblemModel model = new ViewReportProblemModel();

            model.SectionModelLists = Repository.StoredProcedure.SpReportProblem.SpGetReportProblemSection();

            ViewData["Department"] = profileData.DepartmentShort.Substring(0, 4);

            return View(model);
        }
        public ActionResult ProblemStatus()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            ViewReportProblemModel model = new ViewReportProblemModel();

            if(profileData.PermissionCode == "1")
            {
                model.ReportProblemModelLists = Repository.StoredProcedure.SpReportProblem.SpGetTableReportProblem(null,"1").ToList();
                model.ReportProblemStatusModelLists = Repository.StoredProcedure.SpReportProblem.SpGetReportProblemStatus().ToList();
            }
            else if(profileData.PermissionCode == "10") // โอนเรื่องไป กบล เนื่องจาก กบล เป็นแอดมิน (PermissionCode = 10)
            {
                model.ReportProblemModelLists = Repository.StoredProcedure.SpReportProblem.SpGetTableReportProblem(null,"2").ToList();
                model.ReportProblemStatusModelLists = Repository.StoredProcedure.SpReportProblem.SpGetReportProblemStatus("1").ToList();
            }
            else
            {
                model.ReportProblemModelLists = Repository.StoredProcedure.SpReportProblem.SpGetTableReportProblem(profileData.Username,"3");
                model.ReportProblemStatusModelLists = new List<ReportProblemStatusModel>();
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult Insert(ReportProblemModel model)
        {
            // สร้างตัวแปร
            String Message = "";
            String Status = "";
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                try
                {
                    String ID = "";
                    (ID, Status, Message) = Repository.StoredProcedure.SpReportProblem.SpInsertReportProblem(model);
                    (Status, Message) = Repository.StoredProcedure.SpReportProblem.SpInsertReportProblemDetails(ID, model);
                    if(Status == "0")
                    {
                        var myData3 = new
                        {
                            Status = Status, // Update สำเร็จ
                            Message = Message,
                        };
                        return Json(myData3);
                    }
                    

                    string token = "Um8vecrhRQJ2tSmHddHr7QYloWOsT8stWyxu9aodLXi";
                    string msg = "";
                    msg = "ชื่อ : " + model.Name + "\n";
                    msg += "รหัสพนักงาน : " + model.EmployeeID + "\n";
                    msg += "สังกัด : " + model.BAName + "\n";
                    msg += "แผนก : " + model.Department + "\n";
                    msg += "เบอร์ติดต่อ : " + model.Tel + "\n";
                    msg += "เรื่อง : " + model.TitleText;
                    msg += "รายละเอียด : " + model.Details + "\n";
                    String Datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg += "วันที่และเวลา : " + Datetime + "\n";

                    model.Title = Regex.Replace(model.Title, @"\s+", "");

                    int stickerPackageID = 6325;
                    int stickerID = 10979905;
                    var request = (HttpWebRequest)WebRequest.Create("https://notify-api.line.me/api/notify");
                    var postData = string.Format("message={0}", msg);
                    if (stickerPackageID > 0 && stickerID > 0)
                    {
                        var stickerPackageId = string.Format("stickerPackageId={0}", stickerPackageID);
                        var stickerId = string.Format("stickerId={0}", stickerID);
                        postData += "&" + stickerPackageId.ToString() + "&" + stickerId.ToString();
                    }
                    var data = Encoding.UTF8.GetBytes(postData);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;
                    request.Headers.Add("Authorization", "Bearer " + token);

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    var myData2 = new
                    {
                        Status = Status, // Update สำเร็จ
                        Message = Message,
                    };
                    return Json(myData2);
                }
                catch (Exception ex)
                {
                    var myData3 = new
                    {
                        Status = "0", // Update สำเร็จ
                        Message = ex.ToString(),
                    };
                    return Json(myData3);
                }
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult UpdateStatus(String ID,String Status,String IsTransfer, String Note)
        {
            // สร้างตัวแปร
            String Message = "";
            String StatusError = "";
            // สร้างตัวแปร
            try
            {
                // เช็ค session expire
                var profileData = this.Session["UserProfile"] as UserProfileSessionData;
                if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
                {
                    MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                    var myData = new
                    {
                        Status = "-99", // Session หมด
                    };
                    return Json(myData);
                    //return Json("ไม่พบรหัสพนักงานคนดังกล่าว");
                }
                // เช็ค session expire

                List<TableReportProblemModel> checks = Repository.StoredProcedure.SpReportProblem.SpGetTableReportProblem(null,"99",ID).ToList();
                if(checks[0].IsTransfer == "1")
                {
                    (StatusError, Message) = Repository.StoredProcedure.SpReportProblem.SpUpdateReportProblemDetails(ID, Status, "1", Note, profileData.Username);
                }
                else
                {
                    (StatusError, Message) = Repository.StoredProcedure.SpReportProblem.SpUpdateReportProblemDetails(ID, Status, IsTransfer, Note, profileData.Username);
                }

                var myData3 = new
                {
                    Status = StatusError, // Update สำเร็จ
                    Message = Message,
                    User = profileData.Username
                };
                return Json(myData3);
            }
            catch (Exception ex) // กรณีมี Error ต่างๆ ให้ส่ง Error กลับไปที่ Ajax
            {
                var myData = new
                {
                    Status = "0", // Update สำเร็จ
                    Message = ex.Message,
                };
                return Json(myData);
            }
        }
    }
}