﻿using iPMS.Helper;
using iPMS.Models.SessionModel;
using iPMS.ServiceReference2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class DeveloperController : BaseController
    {
        private Guid guidOutput;

        // GET: Delveoper
        public ActionResult Index()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            return View();
        }

        [HttpPost]
        public ActionResult DeleteSection(String Key,String Key2,String EmployeeID)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            String Status = "", Message = "";
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
            }
            else if (profileData.PermissionCode != "1") // เช็คสิทธิ์ผู้พัฒนาระบบ
            {
                MessageManager.SetErrorMessage("คุณไม่มีสิทธิ์ในการใช้ฟังก์ชันนี้ กรุณาล็อคอินใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // เตะให้ไปล็อคอินใหม่
                };
                return Json(myData);
            }
            //EmployeeProfile employeeProfile = IdmManager.GetByEmployeeNo(EmployeeID);

            UserProfileSessionData ResponseprofileData = new UserProfileSessionData();
            (ResponseprofileData, Status, Message) = getHrPlatform(EmployeeID);

            if (ResponseprofileData == null)
            {
                var myData = new
                {
                    Status = "0", // เตะให้ไปล็อคอินใหม่
                    Message = "ไม่มีรหัสพนักงานดังกล่าว"
                };
                return Json(myData);
            }
            // เช็ค session expire
            bool isValidKey1 = Guid.TryParse(Key, out guidOutput);
            bool isValidKey2 = Guid.TryParse(Key2, out guidOutput);
            if (!isValidKey1 || !isValidKey2)
            {
                var myData3 = new
                {
                    Status = 0,
                    Message = "กรุณาตรวจสอบคีย์ถูกต้องหรือไม่",
                };
                return Json(myData3);
            }
            int row;

            Message = EmployeeID + " " + "ได้แจ้งเข้ามาให้ทำการลบ";
            (Status, Message,row) = Repository.StoredProcedure.SpDeveloper.SpDeleteSection(Key, Key2, profileData.Username, controllerName, actionName, "Success", Message);
            if(row <= 1 && Status == "1")
            {
                var myData3 = new
                {
                    Status = 0,
                    Message = "กรุณาตรวจสอบคีย์ถูกต้องหรือไม่",
                    row = row,
                };
                return Json(myData3);
            }
            var myData2 = new
            {
                Status = Status,
                Message = Message,
                row = row,
            };
            return Json(myData2);
        }
    }
}