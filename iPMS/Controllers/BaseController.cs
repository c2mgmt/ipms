﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using iPMS.Utility;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static iPMS.Models.HrPlatform.EmployeeModel;

namespace iPMS.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        public static int CheckSession(UserProfileSessionData profile)
        {
            if (profile == null)
            {
                return 0;
            }
            else if (profile.IsLogin == "0")
            {
                return 0;
            }
            return 1;
        }
        public static bool CheckPermission(String Code,int Level)
        {
            //ผู้พัฒนาและผู้ดูแล
            if (Level == 1 && (Mapper.MapStringToInteger(Code) <= 10 && Mapper.MapStringToInteger(Code) >= 0)) 
            {
                return true;
            }
            //ผู้พัฒนา ผู้ดูแล ผู้เพิ่มหัวข้อ
            else if(Level == 2 && (Mapper.MapStringToInteger(Code) <= 20 && Mapper.MapStringToInteger(Code) >= 0))
            {
                return true;
            }
            //ผู้พัฒนา ผู้ดูแล ผู้เพิ่มหัวข้อ ให้คะแนน
            else if (Level == 3 && (Mapper.MapStringToInteger(Code) <= 30 && Mapper.MapStringToInteger(Code) >= 0))
            {
                return true;
            }
            else if(Level == 4 
                && ( (Mapper.MapStringToInteger(Code) <= 10 && Mapper.MapStringToInteger(Code) >= 0) 
                    || (Mapper.MapStringToInteger(Code) <= 50 && Mapper.MapStringToInteger(Code) >= 40)))
            {
                return true;
            }
            return false;
        }

        public static bool CheckPMSPermission(String Code, int Level)
        {
            //ผู้พัฒนาและผู้ดูแล
            if (Level == 1 && (Mapper.MapStringToInteger(Code) <= 10 && Mapper.MapStringToInteger(Code) >= 0))
            {
                return true;
            }
            //ผู้พัฒนา ผู้ดูแล คณะทำงาน
            else if (Level == 2 && (Mapper.MapStringToInteger(Code) <= 20 && Mapper.MapStringToInteger(Code) >= 0))
            {
                return true;
            }
            //ผู้พัฒนา ผู้ดูแล คณะทำงาน ให้คะแนน
            else if (Level == 3 && (Mapper.MapStringToInteger(Code) <= 30 && Mapper.MapStringToInteger(Code) >= 0))
            {
                return true;
            }
            return false;
        }
        public static bool CheckPermissionExecutive(String PositionDescShort)
        {
            if (PositionDescShort == "ชก."
               || PositionDescShort == "รก."
               || PositionDescShort == "อก."
               || PositionDescShort == "รฝ."
               || PositionDescShort == "อฝ."
               || PositionDescShort == "ผชก.")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static int checkUpdatePMS(PMSUpdateAssistantModel model)
        {
            int countcheckData = 0;
            if (model.Levels == "" || model.Levels == null)
            {
                countcheckData++;
            }
            if (model.Score == "" || model.Score == null)
            {
                countcheckData++;
            }
            if (model.PredictLevels == "" || model.PredictLevels == null)
            {
                countcheckData++;
            }
            if (model.PredictScore == "" || model.PredictScore == null)
            {
                countcheckData++;
            }
            return countcheckData;
        }

        public (String, String) HrPlatform(string EmployeeID)
        {
            String Status = "", Message = "";
            //RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
            //    };
            //var client = new RestClient(options
            //    , configureSerialization: s => s.UseNewtonsoftJson());

            //        , configureSerialization: s => s.UseNewtonsoftJson());

            var options = new RestClientOptions("https://servicehub.pea.co.th:8443/")
            {
                RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
            };
            var client = new RestClient(options);

            var request = new RestRequest("get-employee-detail-m", Method.Get);
            request.AddParameter("emp_id", EmployeeID);
            request.AddHeader("apikey", "4p5pbs0ITasWFEqSyFfauqLez2juSPFk");

            var response = client.Execute(request);
            var data = JsonConvert.DeserializeObject<HRServicesResult>(response.Content);

            //if(response.IsSuccessStatusCode)
            //var data = JsonSerializer.Deserialize<JsonNode>(response.Content);
            //var datass = JsonSerializer.Deserialize<object>(response.Content);

            if (response.IsSuccessStatusCode)
            {
                //EmployeeProfile employeeProfile = IdmManager.GetByEmployeeNo(EmployeeID);

                //int EmployeeId = Int32.Parse(employeeProfile.EmployeeId);
                if (data.data.ServiceStatus == "true")
                {
                    //admin
                    String Description = "ผู้ใช้งานทั่วไป";
                    String PMSDescription = "ผู้ใช้งานทั่วไป";
                    String isAdmin = "0";
                    String isLogin = "1";
                    String permissionCode = "9999";
                    String PMSpermissionCode = "9999";
                    String Mast_TRSG = "0";
                    String TRSG = "";
                    String IDPEA = "";
                    List<PermissionModel> admin = new List<PermissionModel>();
                    admin = Repository.StoredProcedure.SpBase.SpCheckPermission(EmployeeID.ToString());
                    if (admin.Count != 0)
                    {
                        if (admin[0].PermissionCode != "")
                        {
                            isAdmin = "1";
                            Description = admin[0].Description;
                            permissionCode = admin[0].PermissionCode;
                        }
                        if (admin[0].PMSPermissionCode != "")
                        {
                            isAdmin = "1";
                            PMSDescription = admin[0].PMSDescription;
                            PMSpermissionCode = admin[0].PMSPermissionCode;
                        }
                        if (permissionCode == "40") // อก.
                        {
                            List<CostCenterModel> Cost = new List<CostCenterModel>();
                            Cost = Repository.StoredProcedure.SpBase.SpCheckCostCenter(data.data.dataDetail[0].cost_center);
                            if (Cost.Count == 0)
                            {
                                Status = "false";
                                Message = "กรุณาตรวจสอบสิทธิ์การใช้งานให้ถูกต้อง หรือ ตรวจสอบศูนย์ต้นทุน";
                                return (Status, Message);
                            }
                            else
                            {
                                Mast_TRSG = Cost[0].TRSG;
                            }
                            //Mast_TRSG = Cost[0].TRSG;
                        }
                        else if (permissionCode == "50") // ผจก.
                        {
                            List<CostCenterModel> Cost = new List<CostCenterModel>();
                            Cost = Repository.StoredProcedure.SpBase.SpCheckCostCenterPEA(data.data.dataDetail[0].cost_center);
                            if (Cost.Count == 0)
                            {
                                Status = "false";
                                Message = "กรุณาตรวจสอบสิทธิ์การใช้งานให้ถูกต้อง หรือ ตรวจสอบศูนย์ต้นทุน";
                                return (Status, Message);
                            }
                            else
                            {
                                Mast_TRSG = Cost[0].TRSG;
                            }
                        }
                    }
                    else
                    {
                        isAdmin = "0";
                    }

                    if (data.data.dataDetail[0].business_area != "H000")
                    {
                        (Status, TRSG, IDPEA) = Repository.StoredProcedure.SpBase.spGetTRSG_By_CostCenterCode(data.data.dataDetail[0].cost_center.Substring(0, 7));
                        if (Status == "0")
                        {
                            Status = "false";
                            Message = TRSG;
                            return (Status, Message);
                        }
                    }

                    // เซตค่า Session

                    var profileData = new UserProfileSessionData
                    {
                        Username = data.data.dataDetail[0].emp_id, // employeeProfile.Username
                        EmployeeId = "00" + data.data.dataDetail[0].emp_id, // employeeProfile.EmployeeId,
                        TitleFullName = data.data.dataDetail[0].title_s_desc, // employeeProfile.TitleFullName,
                        FirstName = data.data.dataDetail[0].first_name, // employeeProfile.FirstName,
                        LastName = data.data.dataDetail[0].last_name, // employeeProfile.LastName,
                        GroupId = data.data.dataDetail[0].posi_status, // employeeProfile.GroupId, -> ไม่มี
                        Group = data.data.dataDetail[0].posi_status_desc, // employeeProfile.Group,
                        GenderCode = data.data.dataDetail[0].sex, // employeeProfile.GenderCode,
                        Gender = data.data.dataDetail[0].sex_desc, // employeeProfile.Gender, -> ไม่มี
                        PositionCode = data.data.dataDetail[0].posi_code, // employeeProfile.PositionCode,
                        PositionDescShort = data.data.dataDetail[0].posi_text_short, // employeeProfile.PositionDescShort,
                        Position = data.data.dataDetail[0].posi_text, // employeeProfile.Position,
                        LevelCode = data.data.dataDetail[0].level_code, // employeeProfile.LevelCode,
                        LevelDesc = data.data.dataDetail[0].level_desc, // employeeProfile.LevelDesc,
                        NewOrganizationalCode = data.data.dataDetail[0].dept_change_code, // employeeProfile.NewOrganizationalCode,
                        DepartmentShortName = data.data.dataDetail[0].dept_short, // employeeProfile.DepartmentShortName,
                        DepartmentFullName = data.data.dataDetail[0].dept_full, // employeeProfile.DepartmentFullName,
                        DepartmentSap = data.data.dataDetail[0].dept_sap, // employeeProfile.DepartmentSap,
                        DepartmentShort = data.data.dataDetail[0].dept_sap_short, // employeeProfile.DepartmentShort,
                        Department = data.data.dataDetail[0].dept_sap_full, // employeeProfile.Department,
                        RegionCode = data.data.dataDetail[0].region, // employeeProfile.RegionCode,
                        Region = data.data.dataDetail[0].region_name, // employeeProfile.Region,
                        SubRegionCode = data.data.dataDetail[0].sub_region, // employeeProfile.SubRegionCode,
                        SubRegion = data.data.dataDetail[0].sub_region_name, // employeeProfile.SubRegion,
                        CostCenterCode = data.data.dataDetail[0].cost_center, // employeeProfile.CostCenterCode,
                        CostCenterName = data.data.dataDetail[0].cost_center_name, // employeeProfile.CostCenterName,
                        Peacode = data.data.dataDetail[0].pea_code, // employeeProfile.Peacode,
                        BaCode = data.data.dataDetail[0].business_area, // employeeProfile.BaCode,
                        BaName = data.data.dataDetail[0].business_area_name, // employeeProfile.BaName,
                        Email = data.data.dataDetail[0].email,// employeeProfile.Email,

                        Description = Description,
                        PMSDescription = PMSDescription,

                        IsAdmin = isAdmin,
                        PermissionCode = permissionCode,
                        PMSPermissionCode = PMSpermissionCode,
                        Mast_TRSG = Mast_TRSG,
                        IsLogin = isLogin,

                        TRSG = TRSG,
                        IDPEA = IDPEA,
                    };

                    this.Session["UserProfile"] = profileData;
                }

                Status = data.data.ServiceStatus;
                Message = data.data.ServiceMessage;
            }
            else
            {
                Status = response.IsSuccessStatusCode.ToString();
                Message = response.ErrorException.Message;
            }

            return (Status, Message);
            //return Json(myData2);
        }

        public (UserProfileSessionData, String, String) getHrPlatform(string EmployeeID)
        {
            String Status = "", Message = "";
            var options = new RestClientOptions("https://servicehub.pea.co.th:8443/")
            {
                RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
            };
            var client = new RestClient(options);

            var request = new RestRequest("get-employee-detail-m", Method.Get);
            request.AddParameter("emp_id", EmployeeID);
            request.AddHeader("apikey", "4p5pbs0ITasWFEqSyFfauqLez2juSPFk");

            var response = client.Execute(request);
            var data = JsonConvert.DeserializeObject<HRServicesResult>(response.Content);

            //if(response.IsSuccessStatusCode)
            //var data = JsonSerializer.Deserialize<JsonNode>(response.Content);
            //var datass = JsonSerializer.Deserialize<object>(response.Content);
            UserProfileSessionData profileData = null;
            if (response.IsSuccessStatusCode)
            {
                if (data.data.ServiceStatus == "true")
                {
                    profileData = new UserProfileSessionData
                    {
                        Username = data.data.dataDetail[0].emp_id, // employeeProfile.Username
                        EmployeeId = "00" + data.data.dataDetail[0].emp_id, // employeeProfile.EmployeeId,
                        TitleFullName = data.data.dataDetail[0].title_s_desc, // employeeProfile.TitleFullName,
                        FirstName = data.data.dataDetail[0].first_name, // employeeProfile.FirstName,
                        LastName = data.data.dataDetail[0].last_name, // employeeProfile.LastName,
                        GroupId = data.data.dataDetail[0].posi_status, // employeeProfile.GroupId, -> ไม่มี
                        Group = data.data.dataDetail[0].posi_status_desc, // employeeProfile.Group,
                        GenderCode = data.data.dataDetail[0].sex, // employeeProfile.GenderCode,
                        Gender = data.data.dataDetail[0].sex_desc, // employeeProfile.Gender, -> ไม่มี
                        PositionCode = data.data.dataDetail[0].posi_code, // employeeProfile.PositionCode,
                        PositionDescShort = data.data.dataDetail[0].posi_text_short, // employeeProfile.PositionDescShort,
                        Position = data.data.dataDetail[0].posi_text, // employeeProfile.Position,
                        LevelCode = data.data.dataDetail[0].level_code, // employeeProfile.LevelCode,
                        LevelDesc = data.data.dataDetail[0].level_desc, // employeeProfile.LevelDesc,
                        NewOrganizationalCode = data.data.dataDetail[0].dept_change_code, // employeeProfile.NewOrganizationalCode,
                        DepartmentShortName = data.data.dataDetail[0].dept_short, // employeeProfile.DepartmentShortName,
                        DepartmentFullName = data.data.dataDetail[0].dept_full, // employeeProfile.DepartmentFullName,
                        DepartmentSap = data.data.dataDetail[0].dept_sap, // employeeProfile.DepartmentSap,
                        DepartmentShort = data.data.dataDetail[0].dept_sap_short, // employeeProfile.DepartmentShort,
                        Department = data.data.dataDetail[0].dept_sap_full, // employeeProfile.Department,
                        RegionCode = data.data.dataDetail[0].region, // employeeProfile.RegionCode,
                        Region = data.data.dataDetail[0].region_name, // employeeProfile.Region,
                        SubRegionCode = data.data.dataDetail[0].sub_region, // employeeProfile.SubRegionCode,
                        SubRegion = data.data.dataDetail[0].sub_region_name, // employeeProfile.SubRegion,
                        CostCenterCode = data.data.dataDetail[0].cost_center, // employeeProfile.CostCenterCode,
                        CostCenterName = data.data.dataDetail[0].cost_center_name, // employeeProfile.CostCenterName,
                        Peacode = data.data.dataDetail[0].pea_code, // employeeProfile.Peacode,
                        BaCode = data.data.dataDetail[0].business_area, // employeeProfile.BaCode,
                        BaName = data.data.dataDetail[0].business_area_name, // employeeProfile.BaName,
                        Email = data.data.dataDetail[0].email,// employeeProfile.Email,
                    };
                }

                Status = data.data.ServiceStatus;
                Message = data.data.ServiceMessage;
            }
            else
            {
                Status = response.IsSuccessStatusCode.ToString();
                Message = response.ErrorException.Message;
            }

            return (profileData, Status, Message);
            //return Json(myData2);
        }
    }
}