﻿using iPMS.Helper;
using iPMS.Models;
using iPMS.Models.SessionModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iPMS.Controllers
{
    public class ActionPlanController : BaseController
    {
        // GET: ActionPlan
        public ActionResult Index()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire
            return View();
        }
        //public ActionResult SocietyAndEnvironment()
        //{
        //    // เช็ค session expire
        //    var profileData = this.Session["UserProfile"] as UserProfileSessionData;
        //    if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
        //    {
        //        MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
        //        return RedirectToAction("Index", "Login");
        //    }
        //    // เช็ค session expire

        //    String ID = null;
        //    String TypeID = "3"; // แผนปฏิบัติ
        //    String CabinetID = "1"; // นิยามแผนปฏิบัติการ
        //    String TopicID = "1"; // ด้านสังคม
        //    String Year = null; // ปี
        //    String PEAID = null;
        //    String TimesAYearID = null;
        //    String Quarter = null; 
        //    String Status = "";
        //    String Message = "";
        //    ViewActionPlanModel viewModel = new ViewActionPlanModel();

        //    viewModel.YearsModelLists = Repository.StoredProcedure.SpPerformanceAnalysis.SpGetTimesAYear(CabinetID,TopicID).ToList();
        //    Year = viewModel.YearsModelLists[0].Year;
        //    (viewModel.ArchiveModelLists, Status, Message) = Repository.StoredProcedure.SpActionPlan
        //        .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, PEAID, TimesAYearID, Quarter);

        //    ViewData["Year"] = viewModel.YearsModelLists[0].Year;
        //    ViewData["TopicName"] = "ด้านสังคมและสิ่งแวดล้อม";

        //    return View(viewModel);
        //}
        //public ActionResult Finance()
        //{
        //    // เช็ค session expire
        //    var profileData = this.Session["UserProfile"] as UserProfileSessionData;
        //    if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
        //    {
        //        MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
        //        return RedirectToAction("Index", "Login");
        //    }
        //    // เช็ค session expire

        //    String ID = null;
        //    String TypeID = "3"; // แผนปฏิบัติ
        //    String CabinetID = "1"; // นิยามแผนปฏิบัติการ
        //    String TopicID = "2"; // ด้านการเงิน
        //    String Year = null; // ปี
        //    String PEAID = null;
        //    String TimesAYearID = null;
        //    String Quarter = null;
        //    String Status = "";
        //    String Message = "";
        //    ViewActionPlanModel viewModel = new ViewActionPlanModel();

        //    viewModel.YearsModelLists = Repository.StoredProcedure.SpPerformanceAnalysis.SpGetTimesAYear(CabinetID).ToList();
        //    Year = viewModel.YearsModelLists[0].Year;
        //    (viewModel.ArchiveModelLists, Status, Message) = Repository.StoredProcedure.SpActionPlan
        //        .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, PEAID, TimesAYearID, Quarter);

        //    ViewData["Year"] = viewModel.YearsModelLists[0].Year;
        //    ViewData["TopicName"] = "ด้านการเงิน";

        //    return View(viewModel);
        //}
        public ActionResult Target()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            String ID = null;
            String TypeID = "3"; // แผนปฏิบัติ
            String CabinetID = "1"; // นิยามแผนปฏิบัติการ
            String TopicID = "1"; // ด้านเป้าหมาย
            String Year = null; // ปี
            String PEAID = null;
            String TimesAYearID = null;
            String Quarter = null;
            String Status = "";
            String Message = "";
            ViewActionPlanModel viewModel = new ViewActionPlanModel();

            viewModel.YearsModelLists = Repository.StoredProcedure.SpPerformanceAnalysis.SpGetTimesAYear(CabinetID,TopicID).ToList();

            (viewModel.ArchiveModelLists, Status, Message) = Repository.StoredProcedure.SpActionPlan
                .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, PEAID, TimesAYearID, Quarter);

            if(viewModel.YearsModelLists.Count == 0)
            {
                Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
                ViewData["Year"] = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
            }
            else
            {
                Year = viewModel.YearsModelLists[0].Year;
                ViewData["Year"] = viewModel.YearsModelLists[0].Year;
            }
            ViewData["TopicName"] = "ด้านเป้าหมาย";
            ViewData["TopicID"] = TopicID;

            return View(viewModel);
        }
        public ActionResult Customer()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            String ID = null;
            String TypeID = "3"; // แผนปฏิบัติ
            String CabinetID = "1"; // นิยามแผนปฏิบัติการ
            String TopicID = "2"; // ด้านลูกค้า
            String Year = null; // ปี
            String PEAID = null;
            String TimesAYearID = null;
            String Quarter = null;
            String Status = "";
            String Message = "";
            ViewActionPlanModel viewModel = new ViewActionPlanModel();

            viewModel.YearsModelLists = Repository.StoredProcedure.SpPerformanceAnalysis.SpGetTimesAYear(CabinetID, TopicID).ToList();

            (viewModel.ArchiveModelLists, Status, Message) = Repository.StoredProcedure.SpActionPlan
                .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, PEAID, TimesAYearID, Quarter);

            if (viewModel.YearsModelLists.Count == 0)
            {
                Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
                ViewData["Year"] = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
            }
            else
            {
                Year = viewModel.YearsModelLists[0].Year;
                ViewData["Year"] = viewModel.YearsModelLists[0].Year;
            }
            ViewData["TopicName"] = "ด้านลูกค้า";
            ViewData["TopicID"] = TopicID;

            return View(viewModel);
        }
        public ActionResult Internal()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            String ID = null;
            String TypeID = "3"; // แผนปฏิบัติ
            String CabinetID = "1"; // นิยามแผนปฏิบัติการ
            String TopicID = "3"; // ด้านกระบวนการภายใน
            String Year = null; // ปี
            String PEAID = null;
            String TimesAYearID = null;
            String Quarter = null;
            String Status = "";
            String Message = "";
            ViewActionPlanModel viewModel = new ViewActionPlanModel();

            viewModel.YearsModelLists = Repository.StoredProcedure.SpPerformanceAnalysis.SpGetTimesAYear(CabinetID, TopicID).ToList();

            (viewModel.ArchiveModelLists, Status, Message) = Repository.StoredProcedure.SpActionPlan
                .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, PEAID, TimesAYearID, Quarter);

            if (viewModel.YearsModelLists.Count == 0)
            {
                Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
                ViewData["Year"] = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
            }
            else
            {
                Year = viewModel.YearsModelLists[0].Year;
                ViewData["Year"] = viewModel.YearsModelLists[0].Year;
            }
            ViewData["TopicName"] = "ด้านกระบวนการภายใน";
            ViewData["TopicID"] = TopicID;

            return View(viewModel);
        }
        public ActionResult LearningAndDevelopment()
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                return RedirectToAction("Index", "Login");
            }
            // เช็ค session expire

            String ID = null;
            String TypeID = "3"; // แผนปฏิบัติ
            String CabinetID = "1"; // นิยามแผนปฏิบัติการ
            String TopicID = "4"; // ด้านเรียนรู้และพัฒนา
            String Year = null; // ปี
            String PEAID = null;
            String TimesAYearID = null;
            String Quarter = null;
            String Status = "";
            String Message = "";
            ViewActionPlanModel viewModel = new ViewActionPlanModel();

            viewModel.YearsModelLists = Repository.StoredProcedure.SpPerformanceAnalysis.SpGetTimesAYear(CabinetID, TopicID).ToList();

            (viewModel.ArchiveModelLists, Status, Message) = Repository.StoredProcedure.SpActionPlan
                .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, PEAID, TimesAYearID, Quarter);

            if (viewModel.YearsModelLists.Count == 0)
            {
                Year = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
                ViewData["Year"] = DateTime.Now.Year.ToString(); // ปีปัจจุบัน;
            }
            else
            {
                Year = viewModel.YearsModelLists[0].Year;
                ViewData["Year"] = viewModel.YearsModelLists[0].Year;
            }
            ViewData["TopicName"] = "ด้านเรียนรู้และพัฒนา";
            ViewData["TopicID"] = TopicID;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult FilterYear(String Year,String TopicID)
        {
            // เช็ค session expire
            var profileData = this.Session["UserProfile"] as UserProfileSessionData;
            if (CheckSession(profileData) == 0)//// ยังไม่ได้ล็อคอิน
            {
                MessageManager.SetErrorMessage("โปรดล็อคอินเพื่อเข้าสู่ระบบใหม่อีกครั้ง");
                var myData = new
                {
                    Status = "-99", // Session หมด
                };
                return Json(myData);
            }
            // เช็ค session expire

            String ID = null;
            String TypeID = "3"; // แผนปฏิบัติ
            String CabinetID = "1"; // นิยามแผนปฏิบัติการ
            //String TopicID = "1"; // ด้านต่างๆ
            //String Year = null; // ปี
            String PEAID = null;
            String TimesAYearID = null;
            String Quarter = null;
            String Status = "";
            String Message = "";
            List<ArchiveModel> model;

            (model, Status, Message) = Repository.StoredProcedure.SpActionPlan
                .SpGetArchive(ID, TypeID, CabinetID, TopicID, Year, PEAID, TimesAYearID, Quarter);

            if (model.Count == 0)
            {
                var myData3 = new
                {
                    Status = "2", // Get สำเร็จ แต่หัวข้อกิจกรรมไม่มีในฐานข้อมูล
                    Message = "ไม่พบข้อมูลในฐานข้อมูล"
                };
                return Json(myData3);
            }

            var myData2 = new
            {
                Status = "1", // Get สำเร็จ
                Data = model,
                Message = Message
            };
            return Json(myData2);
        }
    }
}