﻿using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpPerformanceAnalysis
    {
        public static List<YearsModel> SpGetTimesAYear(String CabinetID = null,String TopicID = null)
        {
            List<YearsModel> result = new List<YearsModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetDistinctYear]", connection);
                command.Parameters.Add("CabinetID", SqlDbType.NVarChar).Value = CabinetID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = TopicID;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        YearsModel model = new YearsModel();
                        model.Year = reader["Year"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<YearsModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<YearsModel> SpGetDistinctYearAnalysis(String CabinetID = null)
        {
            List<YearsModel> result = new List<YearsModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetDistinctYearAnalysis]", connection);
                command.Parameters.Add("CabinetID", SqlDbType.NVarChar).Value = CabinetID;
                //command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = TopicID;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        YearsModel model = new YearsModel();
                        model.Year = reader["Year"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<YearsModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (List<RawDataArchiveModel>,String,String) SpGetArchive(String ID = null, String TypeID = null
            , String CabinetID = null, String TopicID = null, String Year = null
            , String PEAID = null, String TimesAYearID = null, String Quarter = null)
        {
            List<RawDataArchiveModel> result = new List<RawDataArchiveModel>();

            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetArchive]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.Parameters.Add("CabinetID", SqlDbType.NVarChar).Value = CabinetID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = TopicID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("PEAID", SqlDbType.NVarChar).Value = PEAID;
                command.Parameters.Add("TimesAYearID", SqlDbType.NVarChar).Value = TimesAYearID;
                command.Parameters.Add("Quarter", SqlDbType.NVarChar).Value = Quarter;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    int Start = 0;
                    String KeyID = "";

                    RawDataArchiveModel model = new RawDataArchiveModel();

                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        if (Start == 0)
                        {
                            Start++;
                            KeyID = reader["PEA_OfficeNAME"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.TypeID = reader["TypeID"].ToString();
                            model.TypeName = reader["TypeName"].ToString();
                            model.CabinetID = reader["CabinetID"].ToString();
                            model.Name = reader["Name"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.TopicName = reader["TopicName"].ToString();
                            model.Year = reader["Year"].ToString();
                            model.PEAID = reader["PEAID"].ToString();
                            model.NamePEAID = reader["NamePEAID"].ToString();
                            model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            ArchiveFilesModel store = new ArchiveFilesModel();
                            
                            store.TimesAYearID = reader["TimesAYearID"].ToString();
                            store.NameTimesAYear = reader["NameTimesAYear"].ToString();
                            store.TimesAYear = reader["TimesAYear"].ToString();
                            store.Quarter = reader["Quarter"].ToString();
                            store.FileName = reader["FileName"].ToString();
                            store.CreateBy = reader["CreateBy"].ToString();
                            store.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                            store.UpdateBy = reader["UpdateBy"].ToString();
                            store.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            model.store.Add(store);
                        }
                        else if (KeyID != reader["PEA_OfficeNAME"].ToString()) // ขึ้นหัวข้อใหม่
                        {
                            result.Add(model); // เก็บข้อมูลเก่าก่อน
                            model = new RawDataArchiveModel(); // reset ข้อมูลเก่า

                            KeyID = reader["PEA_OfficeNAME"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.TypeID = reader["TypeID"].ToString();
                            model.TypeName = reader["TypeName"].ToString();
                            model.CabinetID = reader["CabinetID"].ToString();
                            model.Name = reader["Name"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.TopicName = reader["TopicName"].ToString();
                            model.Year = reader["Year"].ToString();
                            model.PEAID = reader["PEAID"].ToString();
                            model.NamePEAID = reader["NamePEAID"].ToString();
                            model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();

                            ArchiveFilesModel store = new ArchiveFilesModel();
                            store.TimesAYearID = reader["TimesAYearID"].ToString();
                            store.NameTimesAYear = reader["NameTimesAYear"].ToString();
                            store.TimesAYear = reader["TimesAYear"].ToString();
                            store.Quarter = reader["Quarter"].ToString();
                            store.FileName = reader["FileName"].ToString();
                            store.CreateBy = reader["CreateBy"].ToString();
                            store.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                            store.UpdateBy = reader["UpdateBy"].ToString();
                            store.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            model.store.Add(store);
                        }
                        else // หัวข้อเดิม แต่เปลี่ยน ไตรมาส
                        {
                            ArchiveFilesModel store = new ArchiveFilesModel();
                            store.TimesAYearID = reader["TimesAYearID"].ToString();
                            store.NameTimesAYear = reader["NameTimesAYear"].ToString();
                            store.TimesAYear = reader["TimesAYear"].ToString();
                            store.Quarter = reader["Quarter"].ToString();
                            store.FileName = reader["FileName"].ToString();
                            store.CreateBy = reader["CreateBy"].ToString();
                            store.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                            store.UpdateBy = reader["UpdateBy"].ToString();
                            store.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            model.store.Add(store);
                        }
                    }
                    result.Add(model);
                    return (result,"1","ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<RawDataArchiveModel>(),"0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}