﻿using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpReportProblem
    {
        public static List<ReportProblemSectionModel> SpGetReportProblemSection()
        {
            List<ReportProblemSectionModel> result = new List<ReportProblemSectionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetReportProblemSection]", connection);

                //command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ReportProblemSectionModel model = new ReportProblemSectionModel();
                        model.ID = reader["ID"].ToString();
                        model.Title = reader["Title"].ToString();
                        model.Cancel = reader["Cancel"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<ReportProblemSectionModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<ReportProblemStatusModel> SpGetReportProblemStatus(String Process = null)
        {
            List<ReportProblemStatusModel> result = new List<ReportProblemStatusModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetReportProblemStatus]", connection);

                command.Parameters.Add("Process", SqlDbType.NVarChar).Value = Process;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ReportProblemStatusModel model = new ReportProblemStatusModel();
                        model.ID = reader["ID"].ToString();
                        model.Describetion = reader["Describetion"].ToString();
                        model.IsTransfer = reader["IsTransfer"].ToString();
                        model.Complete = reader["Complete"].ToString();
                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<ReportProblemStatusModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<TableReportProblemModel> SpGetTableReportProblem(String EmployeeID = null,String Process = null,String ID = null)
        {
            List<TableReportProblemModel> result = new List<TableReportProblemModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetTableReportProblem]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                command.Parameters.Add("Process", SqlDbType.NVarChar).Value = Process;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TableReportProblemModel model = new TableReportProblemModel();
                        model.ID = reader["ID"].ToString();
                        model.Name = reader["Name"].ToString();
                        model.EmployeeID = reader["EmployeeID"].ToString();
                        model.BAName = reader["BAName"].ToString();
                        model.Department = reader["Department"].ToString();
                        model.Tel = reader["Tel"].ToString();
                        model.Title = reader["Title"].ToString();
                        model.TitleDesc = reader["TitleDesc"].ToString();
                        model.Details = reader["Details"].ToString();
                        model.DateTime = Mapper.MapStringToDateTime(reader["DateTime"].ToString());
                        model.ReportProblemDetailsID = reader["ReportProblemDetailsID"].ToString();
                        model.Status = reader["Status"].ToString();
                        model.IsTransfer = reader["IsTransfer"].ToString();
                        model.Describetion = reader["Describetion"].ToString();
                        model.Complete = reader["Complete"].ToString();
                        model.Note = reader["Note"].ToString();
                        model.Responsible = reader["Responsible"].ToString();
                        model.ReportProblemDetailsDateTime = Mapper.MapStringToDateTime(reader["ReportProblemDetailsDateTime"].ToString());

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<TableReportProblemModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String,String,String) SpInsertReportProblem(ReportProblemModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertReportProblem]", connection);
                command.Parameters.Add("Name", SqlDbType.NVarChar).Value = model.Name;
                command.Parameters.Add("EmployeeID", SqlDbType.NVarChar).Value = model.EmployeeID;
                command.Parameters.Add("BAName", SqlDbType.NVarChar).Value = model.BAName;
                command.Parameters.Add("Department", SqlDbType.NVarChar).Value = model.Department;
                command.Parameters.Add("Tel", SqlDbType.NVarChar).Value = model.Tel;
                command.Parameters.Add("Title", SqlDbType.NVarChar).Value = model.Title;
                command.Parameters.Add("Details", SqlDbType.NVarChar).Value = model.Details;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    /*command.Connection.Open();
                    command.ExecuteNonQuery();
                    return ("1", "แจ้งปัญหาสำเร็จ");*/

                    command.Connection.Open();
                    String ID = "";
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ID = reader["ID"].ToString();
                        break;
                    }
                    return (ID, "1","แจ้งปัญหาสำเร็จ");

                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("","0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) SpInsertReportProblemDetails(String ID,ReportProblemModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertReportProblemDetails]", connection);
                command.Parameters.Add("ForeignKeyID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("Status", SqlDbType.NVarChar).Value = "1";

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return ("1", "แจ้งปัญหาสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String,String) SpUpdateReportProblemDetails(String ID,String Status,String IsTransfer, String Note,String UserId)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateReportProblemDetails]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("Status", SqlDbType.NVarChar).Value = Status;
                command.Parameters.Add("IsTransfer", SqlDbType.NVarChar).Value = IsTransfer;
                command.Parameters.Add("Note", SqlDbType.NVarChar).Value = Note;
                command.Parameters.Add("Responsible", SqlDbType.NVarChar).Value = UserId;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1","แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}