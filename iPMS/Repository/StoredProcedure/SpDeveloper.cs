﻿using iPMS.DBConfig;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpDeveloper
    {
        public static (String, String,int) SpDeleteSection(String ID, String GroupSection, String DeleteBy, String ControlName, String ActionName, String Status, String Detail)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateCancelSection]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("GroupSection", SqlDbType.NVarChar).Value = GroupSection;
                command.Parameters.Add("DeleteBy", SqlDbType.Int).Value = DeleteBy;
                command.Parameters.Add("ControlName", SqlDbType.NVarChar).Value = ControlName;
                command.Parameters.Add("ActionName", SqlDbType.NVarChar).Value = ActionName;
                command.Parameters.Add("Status", SqlDbType.NVarChar).Value = Status;
                command.Parameters.Add("Detail", SqlDbType.NVarChar).Value = Detail;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    int numberOfRecords = command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "ลบข้อมูลสำเร็จ", numberOfRecords);
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message,0);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}