﻿using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpBase
    {
        public static List<PermissionModel> SpCheckPermission(String UserID)
        {
            List<PermissionModel> result = new List<PermissionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetAdminPermission]", connection);
                command.Parameters.Add("UserId", SqlDbType.NVarChar).Value = UserID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        PermissionModel model = new PermissionModel();
                        model.Id = reader["Id"].ToString();
                        model.UserId = reader["UserId"].ToString();
                        model.TitleName = reader["TitleName"].ToString();
                        model.FirstName = reader["FirstName"].ToString();
                        model.LastName = reader["LastName"].ToString();
                        model.BA = reader["BA"].ToString();
                        model.CostCenterCode = reader["CostCenterCode"].ToString();
                        model.PermissionCode = reader["PermissionCode"].ToString();
                        model.PMSPermissionCode = reader["PMSPermissionCode"].ToString();
                        model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                        model.CreateBy = reader["CreateBy"].ToString();
                        model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                        model.UpdateBy = reader["UpdateBy"].ToString();
                        model.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());
                        model.DeleteBy = reader["DeleteBy"].ToString();
                        model.Cancel = reader["Cancel"].ToString();
                        model.Description = reader["Description"].ToString();
                        model.PMSDescription = reader["PMSDescription"].ToString();
                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<PermissionModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static String SpGetLazy(String Mac)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetLazy]", connection);
                command.Parameters.Add("MacAddress", SqlDbType.NVarChar).Value = Mac;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    String MacAddress = "";
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MacAddress = reader["MACADDRESS"].ToString();
                    }
                    return MacAddress;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return "";
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static List<CostCenterModel> SpCheckCostCenter(String CostCenterCode)
        {
            List<CostCenterModel> result = new List<CostCenterModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spCheckMgtCostCenter]", connection);
                command.Parameters.Add("CostCenterCode", SqlDbType.NVarChar).Value = CostCenterCode;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        CostCenterModel model = new CostCenterModel();
                        model.ID = Mapper.MapStringToInteger(reader["ID"].ToString());
                        model.CostCenterCode = reader["CostCenterCode"].ToString();
                        model.CostCenterName = reader["CostCenterName"].ToString();
                        model.TRSG = reader["TRSG"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<CostCenterModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<CostCenterModel> SpCheckCostCenterPEA(String CostCenterCode)
        {
            List<CostCenterModel> result = new List<CostCenterModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spCheckMgtCostCenterPEA]", connection);
                command.Parameters.Add("CostCenterCode", SqlDbType.NVarChar).Value = CostCenterCode;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        CostCenterModel model = new CostCenterModel();
                        model.ID = Mapper.MapStringToInteger(reader["ID"].ToString());
                        model.CostCenterCode = reader["CostCenterCode"].ToString();
                        model.CostCenterName = reader["CostCenterName"].ToString();
                        model.TRSG = reader["TRSG"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<CostCenterModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String,String,String) spGetTRSG_By_CostCenterCode(String CostCenterCode)
        {
            String Status = "", Message = "";
            String TRSG = "", IDPEA = "";
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetTRSG_By_CostCenterCode]", connection);
                command.Parameters.Add("CostCenterCode", SqlDbType.NVarChar).Value = CostCenterCode;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TRSG = reader["TRSG"].ToString();
                        IDPEA = reader["IDPEA"].ToString();
                        return ("1", TRSG, IDPEA);
                    }
                    return ("0", "ไม่พบศูนย์ต้นทุนในฐานข้อมูล โปรดติดต่อผู้ดูแลเพื่อตรวจสอบ", "");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message,"");
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        // Insert Log
        public static String SpInsertLog(LogModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertLog]", connection);
                command.Parameters.Add("UserId", SqlDbType.NVarChar).Value = model.UserId;
                command.Parameters.Add("ControllerName", SqlDbType.NVarChar).Value = model.ControllerName;
                command.Parameters.Add("FunctionName", SqlDbType.NVarChar).Value = model.FunctionName;
                command.Parameters.Add("Status", SqlDbType.NVarChar).Value = model.Status;
                command.Parameters.Add("Details", SqlDbType.NVarChar).Value = model.Details;
                command.Parameters.Add("ActionBy", SqlDbType.NVarChar).Value = model.ActionBy;
                command.Parameters.Add("PermissionCode", SqlDbType.NVarChar).Value = model.PermissionCode;
                command.Parameters.Add("PMSPermissionCode", SqlDbType.NVarChar).Value = model.PMSPermissionCode;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return "เพิ่มข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (List<RawDataSectionModel>, String Status, String Details) SpGetNotification(String BA)
        {
            List<RawDataSectionModel> result = new List<RawDataSectionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetNotificationUser]", connection);
                command.Parameters.Add("BA", SqlDbType.NVarChar).Value = BA;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        RawDataSectionModel model = new RawDataSectionModel();

                        model.ID = reader["ID"].ToString();
                        model.GroupSection = reader["GroupSection"].ToString();
                        model.Running = Mapper.MapStringToDouble(reader["Running"].ToString());
                        model.Year = reader["Year"].ToString();
                        model.PEAGroup = reader["PEAGroup"].ToString();
                        model.Segment = reader["Segment"].ToString();
                        model.SegmentName = reader["SegmentName"].ToString();
                        model.NameTimesAYear = Mapper.MapStringToDouble(reader["NameTimesAYear"].ToString());
                        model.Round = Mapper.MapStringToInteger(reader["Round"].ToString());
                        model.CostCenterID = reader["CostCenterID"].ToString();
                        model.TypeID = reader["TypeID"].ToString();
                        model.TopicID = reader["TopicID"].ToString();
                        model.Plans = Regex.Replace(reader["Plans"].ToString(), @"\n|\r", "");
                        model.SectionName = reader["SectionName"].ToString();
                        model.Target = Regex.Replace(reader["Target"].ToString(), @"\n|\r", "");
                        model.TargetFile = reader["TargetFile"].ToString();
                        model.Define = reader["Define"].ToString();
                        model.Frequency = reader["Frequency"].ToString();
                        model.CuttingCycle = reader["CuttingCycle"].ToString();
                        model.AssessorDepartment = reader["AssessorDepartment"].ToString();
                        model.AssessedDivision = reader["AssessedDivision"].ToString();
                        model.AssessedPEA = reader["AssessedPEA"].ToString();
                        ResultModel score = new ResultModel();
                        score.ID = reader["RID"].ToString();
                        score.ForeignSection = reader["RForeignSection"].ToString();
                        score.Round = Mapper.MapStringToInteger(reader["RRound"].ToString());
                        score.TRSG = reader["RTRSG"].ToString();
                        score.PerformanceText = Regex.Replace(reader["RPerformanceText"].ToString(), @"\n|\r", "");
                        score.PerformanceNum = Mapper.MapStringToDouble(reader["RPerformanceNum"].ToString());
                        score.Result = reader["RResult"].ToString();
                        score.ResultScore = Mapper.MapStringToDouble(reader["RResultScore"].ToString());
                        score.Report = Regex.Replace(reader["RReport"].ToString(), @"\n|\r", "");
                        score.ReportFile = reader["RReportFile"].ToString();
                        score.PIDPEA = reader["PIDPEA"].ToString();
                        score.POFFICE = reader["POFFICE"].ToString();
                        score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                        score.PBA = reader["PBA"].ToString();

                        score.Target = reader["Target"].ToString();
                        score.PerformanceText2 = Regex.Replace(reader["PerformanceText2"].ToString(), @"\n|\r", "");
                        score.UsernameAdmin = reader["UsernameAdmin"].ToString();
                        score.UsernameUser = reader["UsernameUser"].ToString();
                        score.CheckBox = reader["CheckBox"].ToString();

                        model.PEA.Add(score);
                        result.Add(model);
                    }
                    return (result, "1", "ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<RawDataSectionModel>(), "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}