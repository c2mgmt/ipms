﻿using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpAdmin
    {
        public static List<PermissionModel> SpGetAdminPermission()
        {
            List<PermissionModel> result = new List<PermissionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetAdminPermission]", connection);
                //command.Parameters.Add("UserId", SqlDbType.NVarChar).Value = UserID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        PermissionModel model = new PermissionModel();
                        model.Id = reader["Id"].ToString();
                        model.UserId = reader["UserId"].ToString();
                        model.TitleName = reader["TitleName"].ToString();
                        model.FirstName = reader["FirstName"].ToString();
                        model.LastName = reader["LastName"].ToString();
                        model.BA = reader["BA"].ToString();
                        model.CostCenterCode = reader["CostCenterCode"].ToString();
                        model.PermissionCode = reader["PermissionCode"].ToString();
                        model.PMSPermissionCode = reader["PMSPermissionCode"].ToString();
                        model.CreateBy = reader["CreateBy"].ToString();
                        model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                        model.UpdateBy = reader["UpdateBy"].ToString();
                        model.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());
                        model.DeleteBy = reader["DeleteBy"].ToString();
                        model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                        model.Cancel = reader["Cancel"].ToString();
                        model.Description = reader["Description"].ToString();
                        model.PMSDescription = reader["PMSDescription"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<PermissionModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<PermissionRoleModel> SpGetAdminPermissionRole(String PermissionCode)
        {
            List<PermissionRoleModel> result = new List<PermissionRoleModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetAdminPermissionRole]", connection);
                command.Parameters.Add("PermissionCode", SqlDbType.NVarChar).Value = PermissionCode;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        PermissionRoleModel model = new PermissionRoleModel();
                        model.PermissionCode = Mapper.MapStringToInteger(reader["PermissionCode"].ToString());
                        model.Description = reader["Description"].ToString();
                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<PermissionRoleModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<PMSPermissionRoleModel> SpGetAdminPMSPermissionRole(String PermissionCode)
        {
            List<PMSPermissionRoleModel> result = new List<PMSPermissionRoleModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetAdminPMSPermissionRole]", connection);
                command.Parameters.Add("PermissionCode", SqlDbType.NVarChar).Value = PermissionCode;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        PMSPermissionRoleModel model = new PMSPermissionRoleModel();
                        model.PermissionCode = Mapper.MapStringToInteger(reader["PMSPermissionCode"].ToString());
                        model.Description = reader["Description"].ToString();
                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<PMSPermissionRoleModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static String SpInsertAdminPermission(PermissionModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertAdminPermission]", connection);
                command.Parameters.Add("UserId", SqlDbType.NVarChar).Value = model.UserId;
                command.Parameters.Add("TitleName", SqlDbType.NVarChar).Value = model.TitleName;
                command.Parameters.Add("FirstName", SqlDbType.NVarChar).Value = model.FirstName;
                command.Parameters.Add("LastName", SqlDbType.NVarChar).Value = model.LastName;
                command.Parameters.Add("BA", SqlDbType.NVarChar).Value = model.BA;
                command.Parameters.Add("CostCenterCode", SqlDbType.NVarChar).Value = model.CostCenterCode;
                command.Parameters.Add("PermissionCode", SqlDbType.NVarChar).Value = model.PermissionCode;
                command.Parameters.Add("PMSPermissionCode", SqlDbType.NVarChar).Value = model.PMSPermissionCode;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return "เพิ่มข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static String SpUpdateAdminPermission(PermissionModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateAdminPermission]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("Id", SqlDbType.NVarChar).Value = model.Id;
                command.Parameters.Add("UserId", SqlDbType.NVarChar).Value = model.UserId;
                command.Parameters.Add("FirstName", SqlDbType.NVarChar).Value = model.FirstName;
                command.Parameters.Add("LastName", SqlDbType.NVarChar).Value = model.LastName;
                command.Parameters.Add("BA", SqlDbType.NVarChar).Value = model.BA;
                command.Parameters.Add("CostCenterCode", SqlDbType.NVarChar).Value = model.CostCenterCode;
                command.Parameters.Add("PermissionCode", SqlDbType.NVarChar).Value = model.PermissionCode;
                command.Parameters.Add("PMSPermissionCode", SqlDbType.NVarChar).Value = model.PMSPermissionCode;
                command.Parameters.Add("UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return "แก้ไขข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static String SpDeleteAdminPermission(PermissionModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spDeleteAdminPermission]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("Id", SqlDbType.NVarChar).Value = model.Id;
                command.Parameters.Add("DeleteBy", SqlDbType.NVarChar).Value = model.DeleteBy;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return "ลบข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        // Get Year ของ PMS, OKR , แผนปฏิบัติ
        public static List<SectionYearModel> SpGetSectionYear(String TypeID = null)
        {
            List<SectionYearModel> result = new List<SectionYearModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetSectionYear]", connection);
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        SectionYearModel model = new SectionYearModel();
                        model.Year = reader["Year"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<SectionYearModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<TableMastLockModel> SpGetTableMastLock()
        {
            List<TableMastLockModel> result = new List<TableMastLockModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetTableMastLock]", connection);
                //command.Parameters.Add("UserId", SqlDbType.NVarChar).Value = UserID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TableMastLockModel model = new TableMastLockModel();
                        model.ID = reader["ID"].ToString();
                        model.TypeID = reader["TypeID"].ToString();
                        model.TypeName = reader["TypeName"].ToString();
                        model.TimesAYear = reader["TimesAYear"].ToString();
                        model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                        model.Year = reader["Year"].ToString();
                        model.Quarter = reader["Quarter"].ToString();
                        model.CreateBy = reader["CreateBy"].ToString();
                        model.CreateDate = Mapper.MapStringToDateTime(reader["CreateDate"].ToString());
                        model.DeleteBy = reader["DeleteBy"].ToString();
                        model.DeleteDate = Mapper.MapStringToDateTime(reader["DeleteDate"].ToString());
                        model.Cancel = reader["Cancel"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<TableMastLockModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String ID,String Cancel,String Status,String Message) SpGetCheckMastLock(String TypeID, String Year, String TimesAYear, String Round)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetMastLock]", connection);
                command.Parameters.Add("TypeID", SqlDbType.Int).Value = TypeID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("TimesAYear", SqlDbType.NVarChar).Value = TimesAYear;
                command.Parameters.Add("Quarter", SqlDbType.NVarChar).Value = Round;
                command.CommandType = CommandType.StoredProcedure;

                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    String ID = "";
                    String Cancel = "";
                    String Status = "2"; // ไม่มีข้อมูล
                    String Message = "ไม่พบข้อมูล";
                    while (reader.Read())
                    {
                        ID = reader["ID"].ToString();
                        Cancel = reader["Cancel"].ToString();
                        Status = "1"; // พบข้อมูล
                        Message = "พบข้อมูล";
                    }
                    return (ID,Cancel,Status,Message);
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("","","0",e.Message); // error
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String,String) SpInsertMastLock(String TypeID, String Year, String TimesAYear, String Round,String EmployeeID)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsMastLock]", connection);
                command.Parameters.Add("TypeID", SqlDbType.Int).Value = TypeID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("TimesAYear", SqlDbType.NVarChar).Value = TimesAYear;
                command.Parameters.Add("Quarter", SqlDbType.NVarChar).Value = Round;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = EmployeeID;
                command.CommandType = CommandType.StoredProcedure;

                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return ("1","เพิ่มข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String,String) SpUpdateMastLock(String ID,String EmployeeID)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdMastLockF]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = EmployeeID;

                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1","บันทึกข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String, String) SpDeleteMastLock(String ID, String EmployeeID)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdMastLockD]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("DeleteBy", SqlDbType.NVarChar).Value = EmployeeID;

                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "บันทึกข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}