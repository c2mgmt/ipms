﻿using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpDataManagement
    {
        public static List<CostCenterModel> SpGetCostCenter()
        {
            List<CostCenterModel> result = new List<CostCenterModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetCostCenter]", connection);
                //command.Parameters.Add("UserId", SqlDbType.NVarChar).Value = UserID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        CostCenterModel model = new CostCenterModel();
                        model.ID = Mapper.MapStringToInteger(reader["ID"].ToString());
                        model.CostCenterCode = reader["CostCenterCode"].ToString();
                        model.CostCenterName = reader["CostCenterName"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<CostCenterModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<TypeModel> SpGetType(String TypeID = null)
        {
            List<TypeModel> result = new List<TypeModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetType]", connection);
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TypeModel model = new TypeModel();
                        model.ID = Mapper.MapStringToInteger(reader["ID"].ToString());
                        model.TypeName = reader["TypeName"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<TypeModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<TypeModel> spGetType_2()
        {
            List<TypeModel> result = new List<TypeModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetType_2]", connection);
                //command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TypeModel model = new TypeModel();
                        model.ID = Mapper.MapStringToInteger(reader["ID"].ToString());
                        model.TypeName = reader["TypeName"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<TypeModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<TopicModel> SpGetTopic(String ForeignKeyID)
        {
            List<TopicModel> result = new List<TopicModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetTopic]", connection);
                command.Parameters.Add("ForeignKeyID", SqlDbType.NVarChar).Value = ForeignKeyID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TopicModel model = new TopicModel();
                        model.ID = reader["ID"].ToString();
                        model.ForeignKeyID = Mapper.MapStringToInteger(reader["ForeignKeyID"].ToString());
                        model.TopicName = reader["TopicName"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<TopicModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<CabinetCategoryModel> SpGetCabinetCategory(String ForeignKeyTypeID = null)
        {
            List<CabinetCategoryModel> result = new List<CabinetCategoryModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetCabinetCategory]", connection);
                command.Parameters.Add("ForeignKeyTypeID", SqlDbType.NVarChar).Value = ForeignKeyTypeID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        CabinetCategoryModel model = new CabinetCategoryModel();
                        model.ID = reader["ID"].ToString();
                        model.ForeignKeyTypeID = reader["ForeignKeyTypeID"].ToString();
                        model.Name = reader["Name"].ToString();
                        model.FileExtension = reader["FileExtension"].ToString();
                        model.Cancel = reader["Cancel"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<CabinetCategoryModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<PEAAnalyticsModel> SpGetPEAAnalytics()
        {
            List<PEAAnalyticsModel> result = new List<PEAAnalyticsModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetAnalytics]", connection);
                //command.Parameters.Add("ForeignKeyTypeID", SqlDbType.NVarChar).Value = ForeignKeyTypeID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        PEAAnalyticsModel model = new PEAAnalyticsModel();
                        model.ID = reader["ID"].ToString();
                        model.PEAID = reader["PEAID"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<PEAAnalyticsModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<ActivityModel> SpGetActivity(String TypeID = null,String Topic = null, String ID = null)
        {
            List<ActivityModel> result = new List<ActivityModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetActivity]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = Topic;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ActivityModel model = new ActivityModel();
                        model.ID = reader["ID"].ToString();
                        model.Running = Mapper.MapStringToDouble(reader["Running"].ToString());
                        model.CostCenterID = reader["CostCenterID"].ToString();
                        model.CostCenterCode = reader["CostCenterCode"].ToString();
                        model.CostCenterName = reader["CostCenterName"].ToString();
                        model.TypeID = reader["TypeID"].ToString();
                        model.TypeName = reader["TypeName"].ToString();
                        model.TopicID = reader["TopicID"].ToString();
                        model.TopicName = reader["TopicName"].ToString();
                        model.Plans = reader["Plans"].ToString();
                        model.SectionName = reader["SectionName"].ToString();
                        model.Target = reader["Target"].ToString();
                        model.TargetFile = reader["TargetFile"].ToString();
                        model.Define = reader["Define"].ToString();
                        model.Frequency = reader["Frequency"].ToString();
                        model.CuttingCycle = reader["CuttingCycle"].ToString();
                        model.AssessorDepartment = reader["AssessorDepartment"].ToString();
                        model.AssessedDivision = reader["AssessedDivision"].ToString();
                        model.AssessedPEA = reader["AssessedPEA"].ToString();
                        model.CreateBy = reader["CreateBy"].ToString();
                        model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                        model.UpdateBy = reader["UpdateBy"].ToString();
                        model.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());
                        model.DeleteBy = reader["DeleteBy"].ToString();
                        model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                        model.Cancel = reader["Cancel"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<ActivityModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static List<ArchiveModel> SpGetArchive(String ID = null,String TypeID = null
            , String CabinetID = null, String TopicID = null,String Year = null
            , String PEAID = null, String PEAOfficeID = null, String TimesAYearID = null
            , String Quarter = null)
        {
            List<ArchiveModel> result = new List<ArchiveModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetArchive]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.Parameters.Add("CabinetID", SqlDbType.NVarChar).Value = CabinetID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = TopicID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("PEAID", SqlDbType.NVarChar).Value = PEAID;
                command.Parameters.Add("PEAOfficeID", SqlDbType.NVarChar).Value = PEAOfficeID;
                command.Parameters.Add("TimesAYearID", SqlDbType.NVarChar).Value = TimesAYearID;
                command.Parameters.Add("Quarter", SqlDbType.NVarChar).Value = Quarter;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ArchiveModel model = new ArchiveModel();
                        model.ID = reader["ID"].ToString();
                        model.TypeID = reader["TypeID"].ToString();
                        model.TypeName = reader["TypeName"].ToString();
                        model.CabinetID = reader["CabinetID"].ToString();
                        model.Name = reader["Name"].ToString();
                        model.TopicID = reader["TopicID"].ToString();
                        model.TopicName = reader["TopicName"].ToString();
                        model.Year = reader["Year"].ToString();
                        model.PEAID = reader["PEAID"].ToString();
                        model.PEAOfficeID = reader["PEAOfficeID"].ToString();
                        model.NamePEAID = reader["NamePEAID"].ToString();
                        model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                        model.TimesAYearID = reader["TimesAYearID"].ToString();
                        model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                        model.TimesAYear = reader["TimesAYear"].ToString();
                        model.Quarter = reader["Quarter"].ToString();
                        model.FileName = reader["FileName"].ToString();
                        model.CreateBy = reader["CreateBy"].ToString();
                        model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                        model.UpdateBy = reader["UpdateBy"].ToString();
                        model.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());
                        model.Cancel = reader["Cancel"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<ArchiveModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static List<TimesAYearsModel> SpGetTimesAYear(String ID = null)
        {
            List<TimesAYearsModel> result = new List<TimesAYearsModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetTimesAYear]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TimesAYearsModel model = new TimesAYearsModel();
                        model.ID = reader["ID"].ToString();
                        model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                        model.TimesAYear = reader["TimesAYear"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<TimesAYearsModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<MastPEAModel> SpGetMastPEA()
        {
            List<MastPEAModel> result = new List<MastPEAModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetMastPEA]", connection);
                //command.Parameters.Add("UserId", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MastPEAModel model = new MastPEAModel();
                        model.ID = reader["ID"].ToString();
                        model.PEAGroup = reader["PEAGroup"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<MastPEAModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<PEAOfficeModel> SpGetPEAOffice(String ID = null)
        {
            List<PEAOfficeModel> result = new List<PEAOfficeModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetPEAOffice]", connection);
                command.Parameters.Add("IDPEA", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        PEAOfficeModel model = new PEAOfficeModel();
                        model.ID = Mapper.MapStringToInteger(reader["ID"].ToString());
                        model.IDPEA = Mapper.MapStringToInteger(reader["IDPEA"].ToString());
                        model.TRSG = reader["TRSG"].ToString();
                        model.OFFICE = reader["OFFICE"].ToString();
                        model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                        model.BA = reader["BA"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<PEAOfficeModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<PEAOfficeModel> SpGetProvincialPEAOffice()
        {
            List<PEAOfficeModel> result = new List<PEAOfficeModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetProvincialPEAOffice]", connection);
                //command.Parameters.Add("IDPEA", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        PEAOfficeModel model = new PEAOfficeModel();
                        model.ID = Mapper.MapStringToInteger(reader["ID"].ToString());
                        model.IDPEA = Mapper.MapStringToInteger(reader["IDPEA"].ToString());
                        model.TRSG = reader["TRSG"].ToString();
                        model.OFFICE = reader["OFFICE"].ToString();
                        model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                        model.BA = reader["BA"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<PEAOfficeModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<SegmentModel> SpGetSegment()
        {
            List<SegmentModel> result = new List<SegmentModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetMastSegment]", connection);
                //command.Parameters.Add("IDPEA", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        SegmentModel model = new SegmentModel();
                        model.ID = reader["ID"].ToString();
                        model.SegmentID = reader["SegmentID"].ToString();
                        model.SegmentName = reader["SegmentName"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<SegmentModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static List<SectionModel> SpGetDoubleCheckSection(SectionModel model,String PEAGroup = null)
        {
            List<SectionModel> result = new List<SectionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spDoubleCheckSection]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = model.Year;
                command.Parameters.Add("Segment", SqlDbType.Int).Value = model.SegmentID;
                command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = PEAGroup;
                //command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = model.PEAGroup;
                command.Parameters.Add("NameTimesAYear", SqlDbType.Decimal).Value = model.NameTimesAYear;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        SectionModel checkmodel = new SectionModel();
                        checkmodel.ID = reader["MastID"].ToString();
                        result.Add(checkmodel);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<SectionModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String,String,List<MgtConfirmModel>) SpCheckMgtConfirm(String Year)
        {
            List<MgtConfirmModel> result = new List<MgtConfirmModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spCheckMgtConfirm]", connection);
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MgtConfirmModel checkmodel = new MgtConfirmModel();
                        checkmodel.Year = reader["Year"].ToString();
                        result.Add(checkmodel);
                    }
                    return ("1","",result);
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message,new List<MgtConfirmModel>());
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<PEAOfficeModel> SpGetPEAGroupOffice(String Min ,String Max)
        {
            List<PEAOfficeModel> result = new List<PEAOfficeModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetGroupPEAOffice]", connection);
                command.Parameters.Add("Min", SqlDbType.NVarChar).Value = Min;
                command.Parameters.Add("Max", SqlDbType.NVarChar).Value = Max;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        PEAOfficeModel model = new PEAOfficeModel();
                        model.ID = Mapper.MapStringToInteger(reader["ID"].ToString());
                        model.IDPEA = Mapper.MapStringToInteger(reader["IDPEA"].ToString());
                        model.TRSG = reader["TRSG"].ToString();
                        model.OFFICE = reader["OFFICE"].ToString();
                        model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                        model.BA = reader["BA"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<PEAOfficeModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static SectionModel SpSetSection(SectionModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetActivity]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        model.ID = reader["ID"].ToString();
                        model.Running = Mapper.MapStringToDouble(reader["Running"].ToString());
                        model.CostCenterID = reader["CostCenterID"].ToString();
                        model.TypeID = reader["TypeID"].ToString();
                        model.TopicID = reader["TopicID"].ToString();
                        model.Plans = reader["Plans"].ToString();
                        model.SectionName = reader["SectionName"].ToString();
                        model.Target = reader["Target"].ToString();
                        model.TargetFile = reader["TargetFile"].ToString();
                        model.Define = reader["Define"].ToString();
                        model.Frequency = reader["Frequency"].ToString();
                        model.CuttingCycle = reader["CuttingCycle"].ToString();
                        model.AssessorDepartment = reader["AssessorDepartment"].ToString();
                        model.AssessedDivision = reader["AssessedDivision"].ToString();
                        model.AssessedPEA = reader["AssessedPEA"].ToString();
                    }
                    return model;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return model;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }


        public static String SpInsertMastActivity(ActivityModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertMastActivity]", connection);
                command.Parameters.Add("Running", SqlDbType.Decimal).Value = model.Running;
                command.Parameters.Add("CostCenterID", SqlDbType.NVarChar).Value = model.CostCenterID;
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = model.TypeID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = model.TopicID;
                command.Parameters.Add("Plans", SqlDbType.NVarChar).Value = model.Plans;
                command.Parameters.Add("SectionName", SqlDbType.NVarChar).Value = model.SectionName;
                command.Parameters.Add("Target", SqlDbType.NVarChar).Value = model.Target;
                command.Parameters.Add("TargetFile", SqlDbType.NVarChar).Value = model.TargetFile;
                command.Parameters.Add("Define", SqlDbType.NVarChar).Value = model.Define;
                command.Parameters.Add("Frequency", SqlDbType.NVarChar).Value = model.Frequency;
                command.Parameters.Add("CuttingCycle", SqlDbType.NVarChar).Value = model.CuttingCycle;
                command.Parameters.Add("AssessorDepartment", SqlDbType.NVarChar).Value = model.AssessorDepartment;
                command.Parameters.Add("AssessedDivision", SqlDbType.NVarChar).Value = model.AssessedDivision;
                command.Parameters.Add("AssessedPEA", SqlDbType.NVarChar).Value = model.AssessedPEA;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    //command.ExecuteNonQuery();

                    String result = "";
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        result = reader["ID"].ToString();
                        break;
                    }
                    return result;

                    //return "เพิ่มข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static String SpUpdateMastActivity(ActivityModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateMastActivity]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("Running", SqlDbType.Decimal).Value = model.Running;
                command.Parameters.Add("CostCenterID", SqlDbType.NVarChar).Value = model.CostCenterID;
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = model.TypeID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = model.TopicID;
                command.Parameters.Add("Plans", SqlDbType.NVarChar).Value = model.Plans;
                command.Parameters.Add("SectionName", SqlDbType.NVarChar).Value = model.SectionName ;
                command.Parameters.Add("Target", SqlDbType.NVarChar).Value = model.Target;
                // กรณีไม่เพิ่มไฟล์
                //if (model.PostedFile != null)
                //{
                //    command.Parameters.Add("TargetFile", SqlDbType.NVarChar).Value = model.TargetFile;
                //}
                command.Parameters.Add("TargetFile", SqlDbType.NVarChar).Value = model.TargetFile;
                command.Parameters.Add("Define", SqlDbType.NVarChar).Value = model.Define;
                command.Parameters.Add("Frequency", SqlDbType.NVarChar).Value = model.Frequency ;
                command.Parameters.Add("CuttingCycle", SqlDbType.NVarChar).Value = model.CuttingCycle;
                command.Parameters.Add("AssessorDepartment", SqlDbType.NVarChar).Value = model.AssessorDepartment;
                command.Parameters.Add("AssessedDivision", SqlDbType.NVarChar).Value = model.AssessedDivision;
                command.Parameters.Add("AssessedPEA", SqlDbType.NVarChar).Value = model.AssessedPEA;
                command.Parameters.Add("UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return "แก้ไขข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static String SpDeleteMastActivity(String ID,String UserID)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spDeleteMastActivity]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.VarChar).Value = ID;
                command.Parameters.Add("DeleteBy", SqlDbType.NVarChar).Value = UserID;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();

                    return "ลบข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static String SpDeleteArchive(String ID, String UserID)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spDeleteArchive]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.VarChar).Value = ID;
                command.Parameters.Add("DeleteBy", SqlDbType.NVarChar).Value = UserID;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();

                    return "ลบข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String,String,String) SpInsertSection(SectionModel model,String PEAGroup = null)
        {
            String Status = "1";
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertSection]", connection);
                command.Parameters.Add("MastID", SqlDbType.NVarChar).Value = model.ID;

                command.Parameters.Add("GroupSection", SqlDbType.NVarChar).Value = model.GroupSection;
                command.Parameters.Add("Running", SqlDbType.Decimal).Value = model.Running;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = model.Year;
                command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = PEAGroup;
                //command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = model.PEAGroup;
                command.Parameters.Add("Segment", SqlDbType.Int).Value = model.SegmentID;
                command.Parameters.Add("NameTimesAYear", SqlDbType.Decimal).Value = model.NameTimesAYear;
                command.Parameters.Add("Round", SqlDbType.Int).Value = model.Round;
                command.Parameters.Add("CostCenterID", SqlDbType.NVarChar).Value = model.CostCenterID;
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = model.TypeID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = model.TopicID;
                command.Parameters.Add("Plans", SqlDbType.NVarChar).Value = model.Plans;
                command.Parameters.Add("SectionName", SqlDbType.NVarChar).Value = model.SectionName;
                command.Parameters.Add("Target", SqlDbType.NVarChar).Value = model.Target;
                command.Parameters.Add("TargetFile", SqlDbType.NVarChar).Value = model.TargetFile;
                command.Parameters.Add("Define", SqlDbType.NVarChar).Value = model.Define;
                command.Parameters.Add("Frequency", SqlDbType.NVarChar).Value = model.Frequency;
                command.Parameters.Add("CuttingCycle", SqlDbType.NVarChar).Value = model.CuttingCycle;
                command.Parameters.Add("AssessorDepartment", SqlDbType.NVarChar).Value = model.AssessorDepartment;
                command.Parameters.Add("AssessedDivision", SqlDbType.NVarChar).Value = model.AssessedDivision;
                command.Parameters.Add("AssessedPEA", SqlDbType.NVarChar).Value = model.AssessedPEA;
                command.Parameters.Add("ModeScore", SqlDbType.NVarChar).Value = model.ModeScore;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    //command.ExecuteNonQuery();

                    String ID = "";
                    String Group = "";
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ID = reader["ID"].ToString();
                        Group = reader["GroupSection"].ToString(); 

                        break;
                    }
                    return (Status, ID,Group);

                    //return "เพิ่มข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    Status = "0";
                    return (Status,e.Message,"");
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static String SpInsertResult(ResultModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertResult]", connection);
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                command.Parameters.Add("Round", SqlDbType.Int).Value = model.Round;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return "เพิ่มข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }


        public static (String,String) SpInsertArchiveAP(ArchiveModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertArchiveActionplan]", connection);
                command.Parameters.Add("TypeID", SqlDbType.Int).Value = model.TypeID;
                command.Parameters.Add("CabinetID", SqlDbType.NVarChar).Value = model.CabinetID;
                command.Parameters.Add("Topic", SqlDbType.NVarChar).Value = model.TopicID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = model.Year;
                command.Parameters.Add("FileName", SqlDbType.NVarChar).Value = model.FileName;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return ("1","เพิ่มข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) SpInsertArchiveAnalytics(ArchiveModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertArchiveAnalytics]", connection);
                command.Parameters.Add("TypeID", SqlDbType.Int).Value = model.TypeID;
                command.Parameters.Add("CabinetID", SqlDbType.NVarChar).Value = model.CabinetID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = model.Year;
                command.Parameters.Add("PEAID", SqlDbType.NVarChar).Value = model.PEAID;
                command.Parameters.Add("PEAOfficeID", SqlDbType.NVarChar).Value = model.PEAOfficeID;
                command.Parameters.Add("TimesAYearID", SqlDbType.NVarChar).Value = model.TimesAYearID;
                command.Parameters.Add("Quarter", SqlDbType.NVarChar).Value = model.Quarter;
                command.Parameters.Add("FileName", SqlDbType.NVarChar).Value = model.FileName;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return ("1", "เพิ่มข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) SpInsertMgtConfirm(MgtConfirmModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spInsertMgtConfirm]", connection);
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = model.Year;
                command.Parameters.Add("TimesAYearID", SqlDbType.NVarChar).Value = model.TimesAYearID;
                command.Parameters.Add("Round", SqlDbType.Int).Value = model.Round;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return ("1","เพิ่มข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }


        public static (String,String) SpUpdateArchiveAP(ArchiveModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spupdateArchiveActionplan]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("FileName", SqlDbType.NVarChar).Value = model.FileName;
                command.Parameters.Add("UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("2","แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) SpUpdateArchiveAnalytics(ArchiveModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateArchiveAnalytics]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("FileName", SqlDbType.NVarChar).Value = model.FileName;
                command.Parameters.Add("UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("2", "แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}