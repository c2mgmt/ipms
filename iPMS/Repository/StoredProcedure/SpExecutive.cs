﻿using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpExecutive
    {
        public static List<MgtConfirmModel> SpGetMgtConfirmByID(String ID)
        {
            List<MgtConfirmModel> result = new List<MgtConfirmModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetMgtConfirmByID]", connection);

                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MgtConfirmModel model = new MgtConfirmModel();
                        model.ID = reader["ID"].ToString();
                        model.Year = reader["Year"].ToString();
                        model.TimesAYearID = reader["TimesAYearID"].ToString();
                        model.Round = Mapper.MapStringToInteger(reader["Round"].ToString());
                        model.TRSG = reader["TRSG"].ToString();
                        model.Manager = reader["Manager"].ToString();
                        model.Managertime = Mapper.MapStringToDateTime(reader["Managertime"].ToString());
                        model.Refuse = reader["Refuse"].ToString();
                        model.Message = Regex.Replace(reader["Message"].ToString(), @"\n|\r", "");
                        model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<MgtConfirmModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String,String) SpDetCountingResultNull(String Year, String TimesAYearID, String Round,String MastTRSG)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spDetCountingResultNull]", connection);

                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("NameTimesAYear", SqlDbType.NVarChar).Value = TimesAYearID;
                command.Parameters.Add("Round", SqlDbType.NVarChar).Value = Round;
                command.Parameters.Add("MastTRSG", SqlDbType.NVarChar).Value = MastTRSG;

                String Count="0",Message="0";
                
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        Count = reader["ResultNull"].ToString();
                    }
                    return (Count, Message);
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (Count,e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<MgtDirectorModel> SpGetMgtDirector(String Year,String TimesAYearID,String Round)
        {
            List<MgtDirectorModel> result = new List<MgtDirectorModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetMgtManager]", connection);

                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("TimesAYearID", SqlDbType.NVarChar).Value = TimesAYearID;
                command.Parameters.Add("Round", SqlDbType.NVarChar).Value = Round;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MgtDirectorModel model = new MgtDirectorModel();
                        model.ID = reader["ID"].ToString();
                        model.Year = reader["Year"].ToString();
                        model.TimesAYearID = reader["TimesAYearID"].ToString();
                        model.Round = reader["Round"].ToString();
                        model.TRSG = reader["TRSG"].ToString();
                        model.Manager = reader["Manager"].ToString();
                        model.Managertime = Mapper.MapStringToDateTime(reader["Managertime"].ToString());
                        model.Refuse = reader["Refuse"].ToString();
                        model.Message = Regex.Replace(reader["Message"].ToString(), @"\n|\r", "");
                        model.CostCenterID = reader["CostCenterID"].ToString();
                        model.CostCenterCode = reader["CostCenterCode"].ToString();
                        model.CostCenterName = reader["CostCenterName"].ToString();
                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<MgtDirectorModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<MgtConfirmModel> SpGetMgtConfirm(String Year, String TimesAYearID, String Round)
        {
            List<MgtConfirmModel> result = new List<MgtConfirmModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetMgtConfirm]", connection);

                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("TimesAYearID", SqlDbType.NVarChar).Value = TimesAYearID;
                command.Parameters.Add("Round", SqlDbType.NVarChar).Value = Round;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MgtConfirmModel model = new MgtConfirmModel();
                        model.ID = reader["ID"].ToString();
                        model.Year = reader["Year"].ToString();
                        model.TimesAYearID = reader["TimesAYearID"].ToString();
                        model.Round = Mapper.MapStringToInteger(reader["Round"].ToString());
                        model.TRSG = reader["TRSG"].ToString();
                        model.Manager = reader["Manager"].ToString();
                        model.Managertime = Mapper.MapStringToDateTime(reader["Managertime"].ToString());
                        model.Refuse = reader["Refuse"].ToString();
                        model.Message = Regex.Replace(reader["Message"].ToString(), @"\n|\r", "");
                        model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<MgtConfirmModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static List<MgtCountingResultModel> SpGetCountingResult99(String Year, String TimesAYearID, String Round)
        {
            List<MgtCountingResultModel> result = new List<MgtCountingResultModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetCountingResult99]", connection);

                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("NameTimesAYear", SqlDbType.NVarChar).Value = TimesAYearID;
                command.Parameters.Add("Round", SqlDbType.NVarChar).Value = Round;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MgtCountingResultModel model = new MgtCountingResultModel();
                        model.ID = reader["ID"].ToString();
                        model.CostCenterName = reader["CostCenterName"].ToString();
                        model.MastTRSG = reader["MastTRSG"].ToString();
                        model.BA = reader["BA"].ToString();
                        model.TRSG = reader["TRSG"].ToString();
                        model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                        model.ResultNull = reader["ResultNull"].ToString();
                        model.Counting99 = reader["Counting"].ToString();
                        model.Total = reader["Total"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<MgtCountingResultModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<MgtCountingResultModel> SpGetCountingResult1(String Year, String TimesAYearID, String Round)
        {
            List<MgtCountingResultModel> result = new List<MgtCountingResultModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetCountingResult1]", connection);

                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("NameTimesAYear", SqlDbType.NVarChar).Value = TimesAYearID;
                command.Parameters.Add("Round", SqlDbType.NVarChar).Value = Round;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MgtCountingResultModel model = new MgtCountingResultModel();
                        model.ID = reader["ID"].ToString();
                        model.CostCenterName = reader["CostCenterName"].ToString();
                        model.MastTRSG = reader["MastTRSG"].ToString();
                        model.BA = reader["BA"].ToString();
                        model.TRSG = reader["TRSG"].ToString();
                        model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                        model.Counting = reader["Counting"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<MgtCountingResultModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (List<ExecutiveSummaryModel>,String,String) SpGetExecutiveSummary(String Year, String NameTimesAYear
            , String Round,String TRSG,String CostCenterID)
        {
            List<ExecutiveSummaryModel> result = new List<ExecutiveSummaryModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetExecutiveSummary]", connection);

                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("NameTimesAYear", SqlDbType.NVarChar).Value = NameTimesAYear;
                command.Parameters.Add("CostCenterID", SqlDbType.NVarChar).Value = CostCenterID;
                command.Parameters.Add("Round", SqlDbType.NVarChar).Value = Round;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = TRSG;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ExecutiveSummaryModel model = new ExecutiveSummaryModel();
                        model.ID = reader["ID"].ToString();
                        model.GroupSection = reader["GroupSection"].ToString();
                        model.Running = Mapper.MapStringToDouble(reader["Running"].ToString());
                        model.Year = reader["Year"].ToString();
                        model.PEAGroup = reader["PEAGroup"].ToString();
                        model.Segment = reader["Segment"].ToString();
                        model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                        model.Round = reader["Round"].ToString();
                        model.CostCenterID = reader["CostCenterID"].ToString();
                        model.TypeID = reader["TypeID"].ToString();
                        model.TopicID = reader["TopicID"].ToString();
                        model.Plans = reader["Plans"].ToString();
                        model.SectionName = reader["SectionName"].ToString();
                        model.Target = reader["Target"].ToString();
                        model.TargetFile = reader["TargetFile"].ToString();
                        model.Define = reader["Define"].ToString();
                        model.Frequency = reader["Frequency"].ToString();
                        model.CuttingCycle = reader["CuttingCycle"].ToString();
                        model.AssessorDepartment = reader["AssessorDepartment"].ToString();
                        model.AssessedDivision = reader["AssessedDivision"].ToString();
                        model.AssessedPEA = reader["AssessedPEA"].ToString();
                        model.ModeScore = reader["ModeScore"].ToString();
                        model.Openscore = reader["Openscore"].ToString();
                        model.CreateBy = reader["CreateBy"].ToString();
                        model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                        model.UpdateBy = reader["UpdateBy"].ToString();
                        model.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());
                        model.MastID = reader["MastID"].ToString();

                        model.ResultID = reader["ResultID"].ToString();
                        model.ForeignSection = reader["ForeignSection"].ToString();
                        model.TRSG = reader["TRSG"].ToString();
                        model.PerformanceText = reader["PerformanceText"].ToString();
                        model.PerformanceNum = reader["PerformanceNum"].ToString();
                        model.Result = reader["Result"].ToString(); 
                        model.ResultScore = reader["ResultScore"].ToString();
                        model.Report = reader["Report"].ToString();
                        model.ReportFile = reader["ReportFile"].ToString();
                        model.UpdateAdmin = reader["UpdateAdmin"].ToString();
                        model.DatetimeADmin = Mapper.MapStringToDateTime(reader["DatetimeADmin"].ToString());
                        model.UpdateUser = reader["UpdateUser"].ToString();
                        model.DatetimeUser = Mapper.MapStringToDateTime(reader["DatetimeUser"].ToString());
                        model.ResultTarget = reader["ResultTarget"].ToString();
                        model.PerformanceText2 = reader["PerformanceText2"].ToString();
                        model.UsernameAdmin = reader["UsernameAdmin"].ToString();
                        model.UsernameUser = reader["UsernameUser"].ToString();
                        model.CheckBox = reader["CheckBox"].ToString();

                        result.Add(model);
                    }
                    return (result,"1","ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<ExecutiveSummaryModel>(),"0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }


        public static (String, String) SpUpdateMgtManager(String ID,String UserID,String Refuse = null,String Message = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateMgtManager]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("Manager", SqlDbType.NVarChar).Value = UserID;
                command.Parameters.Add("Refuse", SqlDbType.NVarChar).Value = Refuse;
                command.Parameters.Add("Message", SqlDbType.NVarChar).Value = Message;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "ยืนยันสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String, String) SpUpdateMgtByAdmin(String ID)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateMgtByAdmin]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;

                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "ยืนยันสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}