﻿using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpReportResult
    {
        public static List<SectionYearModel> SpGetSectionYear(String TypeID = null)
        {
            List<SectionYearModel> result = new List<SectionYearModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetSectionYear]", connection);
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        SectionYearModel model = new SectionYearModel();
                        model.Year = reader["Year"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<SectionYearModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<MastPEAModel> SpGetMastPEA(String ID = null)
        {
            List<MastPEAModel> result = new List<MastPEAModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetMastPEA]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MastPEAModel model = new MastPEAModel();
                        model.ID = reader["ID"].ToString();
                        model.PEAGroup = reader["PEAGroup"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<MastPEAModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<TimesAYearsModel> SpGetTimesAYear(String ID = null)
        {
            List<TimesAYearsModel> result = new List<TimesAYearsModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetTimesAYear]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TimesAYearsModel model = new TimesAYearsModel();
                        model.ID = reader["ID"].ToString();
                        model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                        model.TimesAYear = reader["TimesAYear"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<TimesAYearsModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (List<RawDataSectionModel>, String Status, String Details) SpGetRawDataSection(
            String Year = null, String TypeID = null,
            String TopicID = null, String PEAGroup = null,
            String NameTimesAYear = null, String Round = null)
        {
            List<RawDataSectionModel> result = new List<RawDataSectionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetRawDataSection]", connection);
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = PEAGroup;
                command.Parameters.Add("NameTimesAYear", SqlDbType.NVarChar).Value = NameTimesAYear;
                command.Parameters.Add("Round", SqlDbType.NVarChar).Value = Round;
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = TopicID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    int Start = 0;
                    String KeyID = "";
                    RawDataSectionModel model = new RawDataSectionModel();
                    while (reader.Read())
                    {
                        if(Start == 0) //หัวข้อแรก
                        {
                            Start++;
                            KeyID = reader["ID"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.GroupSection = reader["GroupSection"].ToString();
                            model.Running = Mapper.MapStringToDouble(reader["Running"].ToString());
                            model.Year = reader["Year"].ToString();
                            model.PEAGroup = reader["PEAGroup"].ToString();
                            model.Segment = reader["Segment"].ToString();
                            model.SegmentName = "";//reader["SegmentName"].ToString();
                            model.NameTimesAYear = Mapper.MapStringToDouble(reader["NameTimesAYear"].ToString());
                            model.Round = Mapper.MapStringToInteger(reader["Round"].ToString());
                            model.CostCenterID = reader["CostCenterID"].ToString();
                            model.TypeID = reader["TypeID"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.Plans = Regex.Replace(reader["Plans"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");

                            model.SectionName = reader["SectionName"].ToString();
                            model.Target = Regex.Replace(reader["Target"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.TargetFile = reader["TargetFile"].ToString();
                            model.Define = reader["Define"].ToString();
                            model.Frequency = reader["Frequency"].ToString();
                            model.CuttingCycle = reader["CuttingCycle"].ToString();
                            model.AssessorDepartment = reader["AssessorDepartment"].ToString();
                            model.AssessedDivision = reader["AssessedDivision"].ToString();
                            model.AssessedPEA = reader["AssessedPEA"].ToString();
                            ResultModel score = new ResultModel();
                            score.ID = reader["RID"].ToString();
                            score.ForeignSection = reader["RForeignSection"].ToString();
                            score.Round = Mapper.MapStringToInteger(reader["RRound"].ToString());
                            score.TRSG = reader["RTRSG"].ToString();
                            score.PerformanceText = Regex.Replace(reader["RPerformanceText"].ToString(), @"\n|\r", "");
                            score.PerformanceNum = Mapper.MapStringToDouble(reader["RPerformanceNum"].ToString());
                            score.Result = reader["RResult"].ToString();
                            score.ResultScore = Mapper.MapStringToDouble(reader["RResultScore"].ToString());
                            score.Report = Regex.Replace(reader["RReport"].ToString(), @"\n|\r", "");
                            score.ReportFile = reader["RReportFile"].ToString();
                            score.PIDPEA = reader["PIDPEA"].ToString();
                            score.POFFICE = reader["POFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.PBA = reader["PBA"].ToString();

                            score.UpdateUser = reader["RUpdateUser"].ToString();
                            score.Target = reader["Target"].ToString();
                            score.PerformanceText2 = Regex.Replace(reader["PerformanceText2"].ToString(), @"\n|\r", "");
                            score.UsernameAdmin = reader["UsernameAdmin"].ToString();
                            score.UsernameUser = reader["UsernameUser"].ToString();
                            score.CheckBox = reader["CheckBox"].ToString();

                            model.PEA.Add(score);
                        }
                        else if(KeyID != reader["ID"].ToString()) // ขึ้นหัวข้อใหม่
                        {
                            result.Add(model); // เก็บข้อมูลเก่าก่อน
                            model = new RawDataSectionModel(); // reset ข้อมูลเก่า
                            KeyID = reader["ID"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.GroupSection = reader["GroupSection"].ToString();
                            model.Running = Mapper.MapStringToDouble(reader["Running"].ToString());
                            model.Year = reader["Year"].ToString();
                            model.PEAGroup = reader["PEAGroup"].ToString();
                            model.Segment = reader["Segment"].ToString();
                            model.SegmentName = "";//reader["SegmentName"].ToString();
                            model.NameTimesAYear = Mapper.MapStringToDouble(reader["NameTimesAYear"].ToString());
                            model.Round = Mapper.MapStringToInteger(reader["Round"].ToString());
                            model.CostCenterID = reader["CostCenterID"].ToString();
                            model.TypeID = reader["TypeID"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.Plans = Regex.Replace(reader["Plans"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.SectionName = reader["SectionName"].ToString();
                            model.Target = Regex.Replace(reader["Target"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.TargetFile = reader["TargetFile"].ToString();
                            model.Define = reader["Define"].ToString();
                            model.Frequency = reader["Frequency"].ToString();
                            model.CuttingCycle = reader["CuttingCycle"].ToString();
                            model.AssessorDepartment = reader["AssessorDepartment"].ToString();
                            model.AssessedDivision = reader["AssessedDivision"].ToString();
                            model.AssessedPEA = reader["AssessedPEA"].ToString();
                            ResultModel score = new ResultModel();
                            score.ID = reader["RID"].ToString();
                            score.ForeignSection = reader["RForeignSection"].ToString();
                            score.Round = Mapper.MapStringToInteger(reader["RRound"].ToString());
                            score.TRSG = reader["RTRSG"].ToString();
                            score.PerformanceText = Regex.Replace(reader["RPerformanceText"].ToString(), @"\n|\r", "");
                            score.PerformanceNum = Mapper.MapStringToDouble(reader["RPerformanceNum"].ToString());
                            score.Result = reader["RResult"].ToString();
                            score.ResultScore = Mapper.MapStringToDouble(reader["RResultScore"].ToString());
                            score.Report = Regex.Replace(reader["RReport"].ToString(), @"\n|\r", "");
                            score.ReportFile = reader["RReportFile"].ToString();
                            score.PIDPEA = reader["PIDPEA"].ToString();
                            score.POFFICE = reader["POFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.PBA = reader["PBA"].ToString();

                            score.UpdateUser = reader["RUpdateUser"].ToString();
                            score.Target = reader["Target"].ToString();
                            score.PerformanceText2 = Regex.Replace(reader["PerformanceText2"].ToString(), @"\n|\r", "");
                            score.UsernameAdmin = reader["UsernameAdmin"].ToString();
                            score.UsernameUser = reader["UsernameUser"].ToString();
                            score.CheckBox = reader["CheckBox"].ToString();

                            model.PEA.Add(score);
                        }
                        else // หัวข้อเดิม แต่เปลี่ยน การไฟฟ้า
                        {
                            ResultModel score = new ResultModel();
                            score.ID = reader["RID"].ToString();
                            score.ForeignSection = reader["RForeignSection"].ToString();
                            score.Round = Mapper.MapStringToInteger(reader["RRound"].ToString());
                            score.TRSG = reader["RTRSG"].ToString();
                            score.PerformanceText = Regex.Replace(reader["RPerformanceText"].ToString(), @"\n|\r", "");
                            score.PerformanceNum = Mapper.MapStringToDouble(reader["RPerformanceNum"].ToString());
                            score.Result = reader["RResult"].ToString();
                            score.ResultScore = Mapper.MapStringToDouble(reader["RResultScore"].ToString());
                            score.Report = Regex.Replace(reader["RReport"].ToString(), @"\n|\r", "");
                            score.ReportFile = reader["RReportFile"].ToString();
                            score.PIDPEA = reader["PIDPEA"].ToString();
                            score.POFFICE = reader["POFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.PBA = reader["PBA"].ToString();

                            score.UpdateUser = reader["RUpdateUser"].ToString();
                            score.Target = reader["Target"].ToString();
                            score.PerformanceText2 = Regex.Replace(reader["PerformanceText2"].ToString(), @"\n|\r", "");
                            score.UsernameAdmin = reader["UsernameAdmin"].ToString();
                            score.UsernameUser = reader["UsernameUser"].ToString();
                            score.CheckBox = reader["CheckBox"].ToString();

                            model.PEA.Add(score);
                        }
                    }
                    result.Add(model);
                    return (result,"1","ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<RawDataSectionModel>(),"0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (DataSectionModel, String Status, String Details) SpGetDataSectionResult(ResultModel model)
        {
            DataSectionModel result = new DataSectionModel();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spSelSectionResult]", connection);
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        result.ID = reader["ID"].ToString();
                        result.UpdateUser = reader["UpdateUser"].ToString();
                        result.ForeignSection = reader["ForeignSection"].ToString();
                        result.TRSG = reader["TRSG"].ToString();
                        result.Result = reader["Result"].ToString();
                        result.Plans = Regex.Replace(reader["Plans"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                        result.Target = Regex.Replace(reader["Target"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                        result.TargetFile = reader["TargetFile"].ToString();
                        result.CheckBox = reader["CheckBox"].ToString();

                    }
                    return (result, "1", "อัปเดตคะแนนสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new DataSectionModel(), "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (List<DataSectionModel>, String Status, String Details) SpGetDataSectionResultAll(ResultModel model)
        {
            List<DataSectionModel> result = new List<DataSectionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spSelSectionResultAll]", connection);
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                //command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        DataSectionModel result2 = new DataSectionModel();
                        result2.ID = reader["ID"].ToString();
                        result2.UpdateUser = reader["UpdateUser"].ToString();
                        result2.ForeignSection = reader["ForeignSection"].ToString();
                        result2.TRSG = reader["TRSG"].ToString();
                        result2.Result = reader["Result"].ToString();
                        result2.Plans = Regex.Replace(reader["Plans"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                        result2.Target = Regex.Replace(reader["Target"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                        result2.TargetFile = reader["TargetFile"].ToString();
                        result2.CheckBox = reader["CheckBox"].ToString();
                        result.Add(result2);
                    }
                    return (result, "1", "อัปเดตคะแนนสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<DataSectionModel>(), "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (List<ResultModel>,String Status,String Details) SpGetDataResult(String ID)
        {
            List<ResultModel> result = new List<ResultModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetDataResult]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ResultModel model = new ResultModel();
                        model.ID = reader["ID"].ToString();
                        model.ForeignSection = reader["ForeignSection"].ToString();
                        model.Round = Mapper.MapStringToInteger(reader["Round"].ToString());
                        model.TRSG = reader["TRSG"].ToString();
                        model.PerformanceText = Regex.Replace(reader["PerformanceText"].ToString(), @"\n|\r", "");
                        model.PerformanceNum = Mapper.MapStringToDouble(reader["PerformanceNum"].ToString());
                        model.Result = reader["Result"].ToString();
                        model.ResultScore = Mapper.MapStringToDouble(reader["ResultScore"].ToString());
                        model.Report = Regex.Replace(reader["Report"].ToString(), @"\n|\r", "");
                        model.ReportFile = reader["ReportFile"].ToString();
                        model.UpdateAdmin = reader["UpdateAdmin"].ToString();
                        model.DatetimeADmin = Mapper.MapStringToDateTime(reader["DatetimeADmin"].ToString());
                        model.UpdateUser = reader["UpdateUser"].ToString();
                        model.DatetimeUser = Mapper.MapStringToDateTime(reader["DatetimeUser"].ToString());

                        model.Target = reader["Target"].ToString();
                        model.PerformanceText2 = Regex.Replace(reader["PerformanceText2"].ToString(), @"\n|\r", "");
                        model.UsernameAdmin = reader["UsernameAdmin"].ToString();
                        model.UsernameUser = reader["UsernameUser"].ToString();
                        model.CheckBox = reader["CheckBox"].ToString();

                        result.Add(model);
                    }
                    return (result,"1","ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<ResultModel>(), "0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String,String) SpGetGetCheckCostCenter(String ID)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetCheckCostCenter]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    String CostCenterCode = "";
                    while (reader.Read())
                    {
                        CostCenterCode = reader["CostCenterCode"].ToString();
                        
                    }
                    return ("1",CostCenterCode);
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String,String) SpGetGetCheckTRSG(String ID)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetCheckTRSG]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    String BA = "";
                    String TRSG = "";
                    while (reader.Read())
                    {
                        BA = reader["BA"].ToString();
                        TRSG = reader["TRSG"].ToString();
                    }
                    return ("1", BA, TRSG);
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message,"");
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) SpUpdateResultsByAdmin(ResultModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateResultAdmin]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                command.Parameters.Add("Round", SqlDbType.Int).Value = model.Round;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;
                command.Parameters.Add("PerformanceText", SqlDbType.NVarChar).Value = model.PerformanceText;
                command.Parameters.Add("PerformanceNum", SqlDbType.Decimal).Value = model.PerformanceNum;
                command.Parameters.Add("Result", SqlDbType.NVarChar).Value = model.Result;
                command.Parameters.Add("ResultScore", SqlDbType.Decimal).Value = model.ResultScore;
                command.Parameters.Add("UpdateAdmin", SqlDbType.NVarChar).Value = model.UpdateAdmin;

                command.Parameters.Add("Target", SqlDbType.NVarChar).Value = model.Target;
                command.Parameters.Add("PerformanceText2", SqlDbType.NVarChar).Value = model.PerformanceText2;
                command.Parameters.Add("UsernameAdmin", SqlDbType.NVarChar).Value = model.UsernameAdmin;

                if(model.CheckBox != "undefined")
                {
                    command.Parameters.Add("CheckBox", SqlDbType.Int).Value = model.CheckBox;
                }

                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1","แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) SpUpdateResultsByAdminAll(ResultModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateResultAdmin_All]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                //command.Parameters.Add("Round", SqlDbType.Int).Value = model.Round;
                //command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;
                command.Parameters.Add("PerformanceText", SqlDbType.NVarChar).Value = model.PerformanceText;
                command.Parameters.Add("PerformanceNum", SqlDbType.Decimal).Value = model.PerformanceNum;
                command.Parameters.Add("Result", SqlDbType.NVarChar).Value = model.Result;
                command.Parameters.Add("ResultScore", SqlDbType.Decimal).Value = model.ResultScore;
                command.Parameters.Add("UpdateAdmin", SqlDbType.NVarChar).Value = model.UpdateAdmin;

                command.Parameters.Add("Target", SqlDbType.NVarChar).Value = model.Target;
                //command.Parameters.Add("PerformanceText2", SqlDbType.NVarChar).Value = model.PerformanceText2;
                command.Parameters.Add("UsernameAdmin", SqlDbType.NVarChar).Value = model.UsernameAdmin;

                //if (model.CheckBox != "undefined")
                //{
                //    command.Parameters.Add("CheckBox", SqlDbType.Int).Value = model.CheckBox;
                //}

                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String, String) SpUpdateResultsByAdminLog(ResultModel model,String ControlName,String ActionName,String Status,String Detail)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateResultAdminLog]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                command.Parameters.Add("Round", SqlDbType.Int).Value = model.Round;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;
                command.Parameters.Add("PerformanceText", SqlDbType.NVarChar).Value = model.PerformanceText;
                command.Parameters.Add("PerformanceNum", SqlDbType.Decimal).Value = model.PerformanceNum;
                command.Parameters.Add("Result", SqlDbType.NVarChar).Value = model.Result;
                command.Parameters.Add("ResultScore", SqlDbType.Decimal).Value = model.ResultScore;
                command.Parameters.Add("UpdateAdmin", SqlDbType.NVarChar).Value = model.UpdateAdmin;

                command.Parameters.Add("ControlName", SqlDbType.NVarChar).Value = ControlName;
                command.Parameters.Add("ActionName", SqlDbType.NVarChar).Value = ActionName;
                command.Parameters.Add("Status", SqlDbType.NVarChar).Value = Status;
                command.Parameters.Add("Detail", SqlDbType.NVarChar).Value = Detail;

                command.Parameters.Add("Target", SqlDbType.NVarChar).Value = model.Target;
                command.Parameters.Add("PerformanceText2", SqlDbType.NVarChar).Value = model.PerformanceText2;
                command.Parameters.Add("UsernameAdmin", SqlDbType.NVarChar).Value = model.UsernameAdmin;

                if (model.CheckBox != "undefined")
                {
                    command.Parameters.Add("CheckBox", SqlDbType.Int).Value = model.CheckBox;
                }

                //command.Parameters.Add("CheckBox", SqlDbType.Int).Value = model.CheckBox;

                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "อัปเดตข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) SpUpdateResultsByAdminLogAll(ResultModel model, String ControlName, String ActionName, String Status, String Detail)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateResultAdminLog_All]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                command.Parameters.Add("Round", SqlDbType.Int).Value = model.Round;
                //command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;
                command.Parameters.Add("PerformanceText", SqlDbType.NVarChar).Value = model.PerformanceText;
                command.Parameters.Add("PerformanceNum", SqlDbType.Decimal).Value = model.PerformanceNum;
                command.Parameters.Add("Result", SqlDbType.NVarChar).Value = model.Result;
                command.Parameters.Add("ResultScore", SqlDbType.Decimal).Value = model.ResultScore;
                command.Parameters.Add("UpdateAdmin", SqlDbType.NVarChar).Value = model.UpdateAdmin;

                command.Parameters.Add("ControlName", SqlDbType.NVarChar).Value = ControlName;
                command.Parameters.Add("ActionName", SqlDbType.NVarChar).Value = ActionName;
                command.Parameters.Add("Status", SqlDbType.NVarChar).Value = Status;
                command.Parameters.Add("Detail", SqlDbType.NVarChar).Value = Detail;

                command.Parameters.Add("Target", SqlDbType.NVarChar).Value = model.Target;
                //command.Parameters.Add("PerformanceText2", SqlDbType.NVarChar).Value = model.PerformanceText2;
                command.Parameters.Add("UsernameAdmin", SqlDbType.NVarChar).Value = model.UsernameAdmin;

                //if (model.CheckBox != "undefined")
                //{
                //    command.Parameters.Add("CheckBox", SqlDbType.Int).Value = model.CheckBox;
                //}

                //command.Parameters.Add("CheckBox", SqlDbType.Int).Value = model.CheckBox;

                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "อัปเดตข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String, String) SpUpdateResultsByUser(ResultModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateResultUser]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                command.Parameters.Add("Round", SqlDbType.Int).Value = model.Round;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;
                command.Parameters.Add("Report", SqlDbType.NVarChar).Value = model.Report;
                command.Parameters.Add("ReportFile", SqlDbType.Decimal).Value = model.ReportFile;
                command.Parameters.Add("UpdateUser", SqlDbType.NVarChar).Value = model.UpdateUser;

                command.Parameters.Add("UsernameUser", SqlDbType.NVarChar).Value = model.UsernameUser;

                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) SpUpdateResultsByUserLog(ResultModel model, String ControlName, String ActionName, String Status, String Detail)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateResultUserLog]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                command.Parameters.Add("Round", SqlDbType.Int).Value = model.Round;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;
                command.Parameters.Add("Report", SqlDbType.NVarChar).Value = model.Report;
                command.Parameters.Add("ReportFile", SqlDbType.Decimal).Value = model.ReportFile;
                command.Parameters.Add("UpdateUser", SqlDbType.NVarChar).Value = model.UpdateUser;

                command.Parameters.Add("ControlName", SqlDbType.NVarChar).Value = ControlName;
                command.Parameters.Add("ActionName", SqlDbType.NVarChar).Value = ActionName;
                command.Parameters.Add("Status", SqlDbType.NVarChar).Value = Status;
                command.Parameters.Add("Detail", SqlDbType.NVarChar).Value = Detail;

                command.Parameters.Add("UsernameUser", SqlDbType.NVarChar).Value = model.UsernameUser;

                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "อัปเดตข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static (String, String) SpGetCheckSection(String ID,String GroupSection)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetCheckSection]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("GroupSection", SqlDbType.NVarChar).Value = GroupSection;

                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    String Checksection = "";
                    while (reader.Read())
                    {
                        Checksection = reader["Checksection"].ToString();
                    }
                    return ("1", Checksection);
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) SpDeleteSection(String ID,String GroupSection,String DeleteBy, String ControlName, String ActionName, String Status, String Detail)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spUpdateCancelSection]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("GroupSection", SqlDbType.NVarChar).Value = GroupSection;
                command.Parameters.Add("DeleteBy", SqlDbType.Int).Value = DeleteBy;
                command.Parameters.Add("ControlName", SqlDbType.NVarChar).Value = ControlName;
                command.Parameters.Add("ActionName", SqlDbType.NVarChar).Value = ActionName;
                command.Parameters.Add("Status", SqlDbType.NVarChar).Value = Status;
                command.Parameters.Add("Detail", SqlDbType.NVarChar).Value = Detail;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "ลบข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

    }
}