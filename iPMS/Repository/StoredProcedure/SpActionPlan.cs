﻿using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpActionPlan
    {
        public static (List<ArchiveModel>, String, String) SpGetArchive(String ID = null, String TypeID = null
            , String CabinetID = null, String TopicID = null, String Year = null
            , String PEAID = null, String TimesAYearID = null, String Quarter = null)
        {
            List<ArchiveModel> result = new List<ArchiveModel>();

            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[spGetArchive]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.Parameters.Add("CabinetID", SqlDbType.NVarChar).Value = CabinetID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = TopicID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("PEAID", SqlDbType.NVarChar).Value = PEAID;
                command.Parameters.Add("TimesAYearID", SqlDbType.NVarChar).Value = TimesAYearID;
                command.Parameters.Add("Quarter", SqlDbType.NVarChar).Value = Quarter;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ArchiveModel model = new ArchiveModel();
                        model.ID = reader["ID"].ToString();
                        model.TypeID = reader["TypeID"].ToString();
                        model.TypeName = reader["TypeName"].ToString();
                        model.CabinetID = reader["CabinetID"].ToString();
                        model.Name = reader["Name"].ToString();
                        model.TopicID = reader["TopicID"].ToString();
                        model.TopicName = reader["TopicName"].ToString();
                        model.Year = reader["Year"].ToString();
                        model.PEAID = reader["PEAID"].ToString();
                        model.NamePEAID = reader["NamePEAID"].ToString();
                        model.TimesAYearID = reader["TimesAYearID"].ToString();
                        model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                        model.TimesAYear = reader["TimesAYear"].ToString();
                        model.Quarter = reader["Quarter"].ToString();
                        model.FileName = reader["FileName"].ToString();
                        model.CreateBy = reader["CreateBy"].ToString();
                        model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                        model.UpdateBy = reader["UpdateBy"].ToString();
                        model.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                        result.Add(model); 
                    }
                    return (result, "1", "ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<ArchiveModel>(), "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}