﻿using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpPMSDataManagement
    {
        public static List<TimesAYearsModel> zPMS_spGetTimesAYear()
        {
            List<TimesAYearsModel> result = new List<TimesAYearsModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetTimesAYear]", connection);
                //command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TimesAYearsModel model = new TimesAYearsModel();
                        model.ID = reader["ID"].ToString();
                        model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                        model.TimesAYear = reader["TimesAYear"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<TimesAYearsModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<MastPEAModel> zPMS_spGetMastPEA()
        {
            List<MastPEAModel> result = new List<MastPEAModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetMastPEA]", connection);
                //command.Parameters.Add("UserId", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        MastPEAModel model = new MastPEAModel();
                        model.ID = reader["ID"].ToString();
                        model.IDPEA = reader["IDPEA"].ToString();
                        model.PEAGroup = reader["PEAGroup"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<MastPEAModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        public static List<zPMS_Mast_ActivityModel> zPMS_spGetActivity(String TopicID = null, String ID = null)
        {
            List<zPMS_Mast_ActivityModel> result = new List<zPMS_Mast_ActivityModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetActivity]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = TopicID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        zPMS_Mast_ActivityModel model = new zPMS_Mast_ActivityModel();
                        model.ID = reader["ID"].ToString();
                        model.Running = reader["Running"].ToString();
                        model.CostCenterID = reader["CostCenterID"].ToString();
                        model.CostCenterName = reader["CostCenterName"].ToString();
                        model.TopicID = reader["TopicID"].ToString();
                        model.TopicName = reader["TopicName"].ToString();
                        model.Cri_work = reader["Cri_work"].ToString();
                        model.Cri_Unit = reader["Cri_Unit"].ToString();
                        model.Cri_lv1 = reader["Cri_lv1"].ToString();
                        model.Cri_lv2 = reader["Cri_lv2"].ToString();
                        model.Cri_lv3 = reader["Cri_lv3"].ToString();
                        model.Cri_lv4 = reader["Cri_lv4"].ToString();
                        model.Cri_lv5 = reader["Cri_lv5"].ToString();
                        model.Cri_res = reader["Cri_res"].ToString();
                        model.Cri_Bi = reader["Cri_Bi"].ToString();
                        model.CreateBy = reader["CreateBy"].ToString();
                        model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                        model.UpdateBy = reader["UpdateBy"].ToString();
                        model.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());
                        model.DeleteBy = reader["DeleteBy"].ToString();
                        model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                        model.Active = reader["Active"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<zPMS_Mast_ActivityModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<ArchiveModel> zPMS_spGetArchive(String ID = null, String TypeID = null
            , String CabinetID = null, String Year = null
            , String PEAOfficeID = null, String TimesAYearID = null
            , String Quarter = null)
        {
            List<ArchiveModel> result = new List<ArchiveModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetArchive]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.Parameters.Add("CabinetID", SqlDbType.NVarChar).Value = CabinetID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("PEAOfficeID", SqlDbType.NVarChar).Value = PEAOfficeID;
                command.Parameters.Add("TimesAYearID", SqlDbType.NVarChar).Value = TimesAYearID;
                command.Parameters.Add("Quarter", SqlDbType.NVarChar).Value = Quarter;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ArchiveModel model = new ArchiveModel();
                        model.ID = reader["ID"].ToString();
                        model.TypeID = reader["TypeID"].ToString();
                        model.TypeName = reader["TypeName"].ToString();
                        model.CabinetID = reader["CabinetID"].ToString();
                        model.Name = reader["Name"].ToString();
                        model.TopicID = reader["TopicID"].ToString();
                        model.TopicName = reader["TopicName"].ToString();
                        model.Year = reader["Year"].ToString();
                        model.PEAID = reader["PEAID"].ToString();
                        model.PEAOfficeID = reader["PEAOfficeID"].ToString();
                        model.NamePEAID = reader["NamePEAID"].ToString();
                        model.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                        model.TimesAYearID = reader["TimesAYearID"].ToString();
                        model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                        model.TimesAYear = reader["TimesAYear"].ToString();
                        model.Quarter = reader["Quarter"].ToString();
                        model.FileName = reader["FileName"].ToString();
                        model.CreateBy = reader["CreateBy"].ToString();
                        model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                        model.UpdateBy = reader["UpdateBy"].ToString();
                        model.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());
                        model.Cancel = reader["Cancel"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<ArchiveModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        //Insert
        public static String zPMS_spInsertMastActivity(zPMS_Mast_ActivityModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spInsertMastActivity]", connection);
                command.Parameters.Add("CostCenterID", SqlDbType.NVarChar).Value = model.CostCenterID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = model.TopicID;
                command.Parameters.Add("Running", SqlDbType.Decimal).Value = model.Running;
                command.Parameters.Add("Cri_work", SqlDbType.NVarChar).Value = model.Cri_work;
                command.Parameters.Add("Cri_Unit", SqlDbType.NVarChar).Value = model.Cri_Unit;
                command.Parameters.Add("Cri_lv1", SqlDbType.NVarChar).Value = model.Cri_lv1;
                command.Parameters.Add("Cri_lv2", SqlDbType.NVarChar).Value = model.Cri_lv2;
                command.Parameters.Add("Cri_lv3", SqlDbType.NVarChar).Value = model.Cri_lv3;
                command.Parameters.Add("Cri_lv4", SqlDbType.NVarChar).Value = model.Cri_lv4;
                command.Parameters.Add("Cri_lv5", SqlDbType.NVarChar).Value = model.Cri_lv5;
                command.Parameters.Add("Cri_res", SqlDbType.NVarChar).Value = model.Cri_res;
                command.Parameters.Add("Cri_Bi", SqlDbType.NVarChar).Value = model.Cri_Bi;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    //command.ExecuteNonQuery();

                    String result = "";
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        result = reader["ID"].ToString();
                        break;
                    }
                    return result;

                    //return "เพิ่มข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String,String, String, String) zPMS_spInsertSection(zPMS_SectionModel model, String PEAGroup = null)
        {
            String Status = "1";
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spInsertSection]", connection);

                command.Parameters.Add("MastID", SqlDbType.NVarChar).Value = model.MastID;
                command.Parameters.Add("GroupSection", SqlDbType.NVarChar).Value = model.GroupSection;
                command.Parameters.Add("Running", SqlDbType.Decimal).Value = model.Running;
                command.Parameters.Add("NameTimesAYear", SqlDbType.Decimal).Value = model.NameTimesAYear;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = model.Year;
                command.Parameters.Add("Month", SqlDbType.Int).Value = model.Month;
                command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = PEAGroup;
                command.Parameters.Add("CostCenterID", SqlDbType.NVarChar).Value = model.CostCenterID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = model.TopicID;
                command.Parameters.Add("Cri_work", SqlDbType.NVarChar).Value = model.Cri_work;
                command.Parameters.Add("Cri_Unit", SqlDbType.NVarChar).Value = model.Cri_Unit;
                command.Parameters.Add("Cri_lv1", SqlDbType.NVarChar).Value = model.Cri_lv1;
                command.Parameters.Add("Cri_lv2", SqlDbType.NVarChar).Value = model.Cri_lv2;
                command.Parameters.Add("Cri_lv3", SqlDbType.NVarChar).Value = model.Cri_lv3;
                command.Parameters.Add("Cri_lv4", SqlDbType.NVarChar).Value = model.Cri_lv4;
                command.Parameters.Add("Cri_lv5", SqlDbType.NVarChar).Value = model.Cri_lv5;
                command.Parameters.Add("IsAllTheSame", SqlDbType.NVarChar).Value = model.IsAllTheSame;
                command.Parameters.Add("Cri_res", SqlDbType.NVarChar).Value = model.Cri_res;
                command.Parameters.Add("Cri_Bi", SqlDbType.NVarChar).Value = model.Cri_Bi;
                command.Parameters.Add("IsEstimate", SqlDbType.NVarChar).Value = model.IsEstimate;
                command.Parameters.Add("Weight", SqlDbType.NVarChar).Value = model.Weight;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    //command.ExecuteNonQuery();

                    String ID = "";
                    String Group = "";
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        ID = reader["ID"].ToString();
                        Group = reader["GroupSection"].ToString();

                        break;
                    }
                    return (Status, "เพิ่มข้อมูลสำเร็จ", ID, Group);

                    //return "เพิ่มข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    Status = "0";
                    return (Status, e.Message,"", "");
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String,String) zPMS_spInsertResult(zPMS_ResultModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spInsertResult]", connection);
                command.Parameters.Add("ForeignSection", SqlDbType.NVarChar).Value = model.ForeignSection;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return ("1","เพิ่มข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) zPMS_spInsertArchiveAnalytics(ArchiveModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spInsertArchiveAnalytics]", connection);
                command.Parameters.Add("TypeID", SqlDbType.Int).Value = model.TypeID;
                command.Parameters.Add("CabinetID", SqlDbType.NVarChar).Value = model.CabinetID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = model.Year;
                command.Parameters.Add("PEAOfficeID", SqlDbType.NVarChar).Value = model.PEAOfficeID;
                command.Parameters.Add("TimesAYearID", SqlDbType.NVarChar).Value = model.TimesAYearID;
                command.Parameters.Add("Quarter", SqlDbType.NVarChar).Value = model.Quarter;
                command.Parameters.Add("FileName", SqlDbType.NVarChar).Value = model.FileName;
                command.Parameters.Add("CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return ("1", "เพิ่มข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        //Update
        public static String zPMS_spUpdateMastActivity(zPMS_Mast_ActivityModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spUpdateMastActivity]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("CostCenterID", SqlDbType.NVarChar).Value = model.CostCenterID;
                command.Parameters.Add("TopicID", SqlDbType.NVarChar).Value = model.TopicID;
                command.Parameters.Add("Running", SqlDbType.Decimal).Value = model.Running;
                command.Parameters.Add("Cri_work", SqlDbType.NVarChar).Value = model.Cri_work;
                command.Parameters.Add("Cri_Unit", SqlDbType.NVarChar).Value = model.Cri_Unit;
                command.Parameters.Add("Cri_lv1", SqlDbType.NVarChar).Value = model.Cri_lv1;
                command.Parameters.Add("Cri_lv2", SqlDbType.NVarChar).Value = model.Cri_lv2;
                command.Parameters.Add("Cri_lv3", SqlDbType.NVarChar).Value = model.Cri_lv3;
                command.Parameters.Add("Cri_lv4", SqlDbType.NVarChar).Value = model.Cri_lv4;
                command.Parameters.Add("Cri_lv5", SqlDbType.NVarChar).Value = model.Cri_lv5;
                command.Parameters.Add("Cri_res", SqlDbType.NVarChar).Value = model.Cri_res;
                command.Parameters.Add("Cri_Bi", SqlDbType.NVarChar).Value = model.Cri_Bi;
                command.Parameters.Add("UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return "แก้ไขข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        //Delete
        public static String zPMS_spDeleteMastActivity(String ID, String UserID)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spDeleteMastActivity]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.VarChar).Value = ID;
                command.Parameters.Add("DeleteBy", SqlDbType.NVarChar).Value = UserID;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();

                    return "ลบข้อมูลสำเร็จ";
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return e.Message;
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        //Check 
        public static List<zPMS_SectionModel> zPMS_spDoubleCheckSection(zPMS_SectionModel model, String PEAGroup = null)
        {
            List<zPMS_SectionModel> result = new List<zPMS_SectionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spDoubleCheckSection]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = model.Year;
                command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = PEAGroup;
                //command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = model.PEAGroup;
                command.Parameters.Add("NameTimesAYear", SqlDbType.Decimal).Value = model.NameTimesAYear;

                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        zPMS_SectionModel checkmodel = new zPMS_SectionModel();
                        checkmodel.ID = reader["MastID"].ToString();
                        result.Add(checkmodel);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<zPMS_SectionModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        // Set values
        public static (zPMS_SectionModel, String,String) zPMS_spGetActivity_set(zPMS_SectionModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetActivity]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        model.ID = reader["ID"].ToString();
                        model.MastID = reader["ID"].ToString();
                        model.Running = Mapper.MapStringToDouble(reader["Running"].ToString());
                        model.CostCenterID = reader["CostCenterID"].ToString();
                        model.CostCenterName = reader["CostCenterName"].ToString();
                        model.TopicID = reader["TopicID"].ToString();
                        model.TopicName = reader["TopicName"].ToString();
                        model.Cri_work = reader["Cri_work"].ToString();
                        model.Cri_Unit = reader["Cri_Unit"].ToString();
                        model.Cri_lv1 = reader["Cri_lv1"].ToString();
                        model.Cri_lv2 = reader["Cri_lv2"].ToString();
                        model.Cri_lv3 = reader["Cri_lv3"].ToString();
                        model.Cri_lv4 = reader["Cri_lv4"].ToString();
                        model.Cri_lv5 = reader["Cri_lv5"].ToString();
                        model.Cri_res = reader["Cri_res"].ToString();
                        model.Cri_Bi = reader["Cri_Bi"].ToString();
                        model.Active = reader["Active"].ToString();
                        return (model,"1","เซ็ทข้อมูลสำเร็จ");
                    }
                    return (model,"0","ไม่พบข้อมูล");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (model,"0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}