﻿
using iPMS.DBConfig;
using iPMS.Models;
using iPMS.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace iPMS.Repository.StoredProcedure
{
    public class SpPMSReportResult
    {
        public static List<SectionYearModel> zPMS_spGetSectionYear()
        {
            List<SectionYearModel> result = new List<SectionYearModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetSectionYear]", connection);
                //command.Parameters.Add("TypeID", SqlDbType.NVarChar).Value = TypeID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        SectionYearModel model = new SectionYearModel();
                        model.Year = reader["Year"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<SectionYearModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static List<TimesAYearsModel> zPMS_spGetTimesAYear()
        {
            List<TimesAYearsModel> result = new List<TimesAYearsModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetTimesAYear]", connection);
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        TimesAYearsModel model = new TimesAYearsModel();
                        model.ID = reader["ID"].ToString();
                        model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                        model.TimesAYear = reader["TimesAYear"].ToString();

                        result.Add(model);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return new List<TimesAYearsModel>();
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (List<PMSRawDataSectionModel>, String Status, String Details) zPMS_spGetRawDataSection(
            String Year = null, String PEAGroup = null,
            String NameTimesAYear = null, String Month = null)
        {
            List<PMSRawDataSectionModel> result = new List<PMSRawDataSectionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetRawDataSection]", connection);
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = PEAGroup;
                command.Parameters.Add("NameTimesAYear", SqlDbType.NVarChar).Value = NameTimesAYear;
                command.Parameters.Add("Month", SqlDbType.NVarChar).Value = Month;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    int Start = 0;
                    String KeyID = "";
                    PMSRawDataSectionModel model = new PMSRawDataSectionModel();
                    while (reader.Read())
                    {
                        if (Start == 0) //หัวข้อแรก
                        {
                            Start++;
                            KeyID = reader["ID"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.GroupSection = reader["GroupSection"].ToString();
                            model.MastID = reader["MastID"].ToString();
                            model.Running = reader["Running"].ToString();
                            model.NameTimesAYearID = reader["NameTimesAYearID"].ToString();
                            model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                            model.TimesAYear = reader["TimesAYear"].ToString();
                            model.Year = reader["Year"].ToString();
                            model.Month = reader["Month"].ToString();
                            model.PEAGroupID = reader["PEAGroupID"].ToString();
                            model.PEAGroup = reader["PEAGroup"].ToString();
                            model.CostCenterID = reader["CostCenterID"].ToString();
                            model.CostCenterCode = reader["CostCenterCode"].ToString();
                            model.CostCenterName = reader["CostCenterName"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.TopicName = reader["TopicName"].ToString();
                            model.Cri_work = Regex.Replace(reader["Cri_work"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_Unit = Regex.Replace(reader["Cri_Unit"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv1 = Regex.Replace(reader["Cri_lv1"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv2 = Regex.Replace(reader["Cri_lv2"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv3 = Regex.Replace(reader["Cri_lv3"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv4 = Regex.Replace(reader["Cri_lv4"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv5 = Regex.Replace(reader["Cri_lv5"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_res = Regex.Replace(reader["Cri_res"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.IsAllTheSame = reader["IsAllTheSame"].ToString();
                            model.IsEstimate = reader["IsEstimate"].ToString();
                            model.Weight = reader["Weight"].ToString();
                            model.CreateBy = reader["CreateBy"].ToString();
                            model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                            model.DeleteBy = reader["DeleteBy"].ToString();
                            model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                            model.Active = reader["Active"].ToString();

                            PMSResultModel score = new PMSResultModel();
                            score.ResultID = reader["ResultID"].ToString();
                            score.ForeignSection = reader["ForeignSection"].ToString();
                            score.TRSG = reader["TRSG"].ToString();
                            score.OFFICE = reader["OFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.BA = reader["BA"].ToString();
                            score.Levels = reader["Levels"].ToString();
                            score.Score = reader["Score"].ToString();
                            score.PredictLevels = reader["PredictLevels"].ToString();
                            score.PredictScore = reader["PredictScore"].ToString();
                            score.FileName_CSD = reader["FileName_CSD"].ToString();
                            score.FileName_Score = reader["FileName_Score"].ToString();
                            score.FileName_Predict = reader["FileName_Predict"].ToString();
                            score.UpdateName_CSD = reader["UpdateName_CSD"].ToString();
                            score.UpdateBy_CSD = reader["UpdateBy_CSD"].ToString();
                            score.UpdateDateTime_CSD = Mapper.MapStringToDateTime(reader["UpdateDateTime_CSD"].ToString());
                            score.UpdateName = reader["UpdateName"].ToString();
                            score.UpdateBy = reader["UpdateBy"].ToString();
                            score.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            score.viewUpdateDateTime = score.UpdateDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                            score.viewUpdateDateTime_CSD = score.UpdateDateTime_CSD.ToString("dd/MM/yyyy HH:mm:ss");

                            model.PEA.Add(score);
                        }
                        else if (KeyID != reader["ID"].ToString()) // ขึ้นหัวข้อใหม่
                        {
                            result.Add(model); // เก็บข้อมูลเก่าก่อน
                            model = new PMSRawDataSectionModel(); // reset ข้อมูลเก่า
                            KeyID = reader["ID"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.GroupSection = reader["GroupSection"].ToString();
                            model.MastID = reader["MastID"].ToString();
                            model.Running = reader["Running"].ToString();
                            model.NameTimesAYearID = reader["NameTimesAYearID"].ToString();
                            model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                            model.TimesAYear = reader["TimesAYear"].ToString();
                            model.Year = reader["Year"].ToString();
                            model.Month = reader["Month"].ToString();
                            model.PEAGroupID = reader["PEAGroupID"].ToString();
                            model.PEAGroup = reader["PEAGroup"].ToString();
                            model.CostCenterID = reader["CostCenterID"].ToString();
                            model.CostCenterCode = reader["CostCenterCode"].ToString();
                            model.CostCenterName = reader["CostCenterName"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.TopicName = reader["TopicName"].ToString();
                            model.Cri_work = Regex.Replace(reader["Cri_work"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_Unit = Regex.Replace(reader["Cri_Unit"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv1 = Regex.Replace(reader["Cri_lv1"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv2 = Regex.Replace(reader["Cri_lv2"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv3 = Regex.Replace(reader["Cri_lv3"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv4 = Regex.Replace(reader["Cri_lv4"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv5 = Regex.Replace(reader["Cri_lv5"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_res = Regex.Replace(reader["Cri_res"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.IsAllTheSame = reader["IsAllTheSame"].ToString();
                            model.IsEstimate = reader["IsEstimate"].ToString();
                            model.Weight = reader["Weight"].ToString();
                            model.CreateBy = reader["CreateBy"].ToString();
                            model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                            model.DeleteBy = reader["DeleteBy"].ToString();
                            model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                            model.Active = reader["Active"].ToString();

                            PMSResultModel score = new PMSResultModel();
                            score.ResultID = reader["ResultID"].ToString();
                            score.ForeignSection = reader["ForeignSection"].ToString();
                            score.TRSG = reader["TRSG"].ToString();
                            score.OFFICE = reader["OFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.BA = reader["BA"].ToString();
                            score.Levels = reader["Levels"].ToString();
                            score.Score = reader["Score"].ToString();
                            score.PredictLevels = reader["PredictLevels"].ToString();
                            score.PredictScore = reader["PredictScore"].ToString();
                            score.FileName_CSD = reader["FileName_CSD"].ToString();
                            score.FileName_Score = reader["FileName_Score"].ToString();
                            score.FileName_Predict = reader["FileName_Predict"].ToString();
                            score.UpdateName_CSD = reader["UpdateName_CSD"].ToString();
                            score.UpdateBy_CSD = reader["UpdateBy_CSD"].ToString();
                            score.UpdateDateTime_CSD = Mapper.MapStringToDateTime(reader["UpdateDateTime_CSD"].ToString());
                            score.UpdateName = reader["UpdateName"].ToString();
                            score.UpdateBy = reader["UpdateBy"].ToString();
                            score.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            score.viewUpdateDateTime = score.UpdateDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                            score.viewUpdateDateTime_CSD = score.UpdateDateTime_CSD.ToString("dd/MM/yyyy HH:mm:ss");

                            model.PEA.Add(score);
                        }
                        else // หัวข้อเดิม แต่เปลี่ยน การไฟฟ้า
                        {
                            PMSResultModel score = new PMSResultModel();
                            score.ResultID = reader["ResultID"].ToString();
                            score.ForeignSection = reader["ForeignSection"].ToString();
                            score.TRSG = reader["TRSG"].ToString();
                            score.OFFICE = reader["OFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.BA = reader["BA"].ToString();
                            score.Levels = reader["Levels"].ToString();
                            score.Score = reader["Score"].ToString();
                            score.PredictLevels = reader["PredictLevels"].ToString();
                            score.PredictScore = reader["PredictScore"].ToString();
                            score.FileName_CSD = reader["FileName_CSD"].ToString();
                            score.FileName_Score = reader["FileName_Score"].ToString();
                            score.FileName_Predict = reader["FileName_Predict"].ToString();
                            score.UpdateName_CSD = reader["UpdateName_CSD"].ToString();
                            score.UpdateBy_CSD = reader["UpdateBy_CSD"].ToString();
                            score.UpdateDateTime_CSD = Mapper.MapStringToDateTime(reader["UpdateDateTime_CSD"].ToString());
                            score.UpdateName = reader["UpdateName"].ToString();
                            score.UpdateBy = reader["UpdateBy"].ToString();
                            score.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            score.viewUpdateDateTime = score.UpdateDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                            score.viewUpdateDateTime_CSD = score.UpdateDateTime_CSD.ToString("dd/MM/yyyy HH:mm:ss");

                            model.PEA.Add(score);
                        }
                    }
                    result.Add(model);
                    return (result, "1", "ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<PMSRawDataSectionModel>(), "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (List<PMSRawDataSectionModel>, String Status, String Details) zPMS_spGetRawDataSection_ALL_ByID(
            String ID)
        {
            List<PMSRawDataSectionModel> result = new List<PMSRawDataSectionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetRawDataSection_ALL_ByID]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    int Start = 0;
                    String KeyID = "";
                    PMSRawDataSectionModel model = new PMSRawDataSectionModel();
                    while (reader.Read())
                    {
                        if (Start == 0) //หัวข้อแรก
                        {
                            Start++;
                            KeyID = reader["ID"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.GroupSection = reader["GroupSection"].ToString();
                            model.MastID = reader["MastID"].ToString();
                            model.Running = reader["Running"].ToString();
                            model.NameTimesAYearID = reader["NameTimesAYearID"].ToString();
                            model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                            model.TimesAYear = reader["TimesAYear"].ToString();
                            model.Year = reader["Year"].ToString();
                            model.Month = reader["Month"].ToString();
                            model.PEAGroupID = reader["PEAGroupID"].ToString();
                            model.PEAGroup = reader["PEAGroup"].ToString();
                            model.CostCenterID = reader["CostCenterID"].ToString();
                            model.CostCenterCode = reader["CostCenterCode"].ToString();
                            model.CostCenterName = reader["CostCenterName"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.TopicName = reader["TopicName"].ToString();
                            model.Cri_work = Regex.Replace(reader["Cri_work"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_Unit = Regex.Replace(reader["Cri_Unit"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv1 = Regex.Replace(reader["Cri_lv1"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv2 = Regex.Replace(reader["Cri_lv2"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv3 = Regex.Replace(reader["Cri_lv3"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv4 = Regex.Replace(reader["Cri_lv4"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv5 = Regex.Replace(reader["Cri_lv5"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_res = Regex.Replace(reader["Cri_res"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.IsAllTheSame = reader["IsAllTheSame"].ToString();
                            model.IsEstimate = reader["IsEstimate"].ToString();
                            model.Weight = reader["Weight"].ToString();
                            model.CreateBy = reader["CreateBy"].ToString();
                            model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                            model.DeleteBy = reader["DeleteBy"].ToString();
                            model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                            model.Active = reader["Active"].ToString();

                            PMSResultModel score = new PMSResultModel();
                            score.ResultID = reader["ResultID"].ToString();
                            score.ForeignSection = reader["ForeignSection"].ToString();
                            score.TRSG = reader["TRSG"].ToString();
                            score.OFFICE = reader["OFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.BA = reader["BA"].ToString();
                            score.Levels = reader["Levels"].ToString();
                            score.Score = reader["Score"].ToString();
                            score.PredictLevels = reader["PredictLevels"].ToString();
                            score.PredictScore = reader["PredictScore"].ToString();
                            score.FileName_CSD = reader["FileName_CSD"].ToString();
                            score.FileName_Score = reader["FileName_Score"].ToString();
                            score.FileName_Predict = reader["FileName_Predict"].ToString();
                            score.UpdateName_CSD = reader["UpdateName_CSD"].ToString();
                            score.UpdateBy_CSD = reader["UpdateBy_CSD"].ToString();
                            score.UpdateDateTime_CSD = Mapper.MapStringToDateTime(reader["UpdateDateTime_CSD"].ToString());
                            score.UpdateName = reader["UpdateName"].ToString();
                            score.UpdateBy = reader["UpdateBy"].ToString();
                            score.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            score.viewUpdateDateTime = score.UpdateDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                            score.viewUpdateDateTime_CSD = score.UpdateDateTime_CSD.ToString("dd/MM/yyyy HH:mm:ss");

                            model.PEA.Add(score);
                        }
                        else if (KeyID != reader["ID"].ToString()) // ขึ้นหัวข้อใหม่
                        {
                            result.Add(model); // เก็บข้อมูลเก่าก่อน
                            model = new PMSRawDataSectionModel(); // reset ข้อมูลเก่า
                            KeyID = reader["ID"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.GroupSection = reader["GroupSection"].ToString();
                            model.MastID = reader["MastID"].ToString();
                            model.Running = reader["Running"].ToString();
                            model.NameTimesAYearID = reader["NameTimesAYearID"].ToString();
                            model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                            model.TimesAYear = reader["TimesAYear"].ToString();
                            model.Year = reader["Year"].ToString();
                            model.Month = reader["Month"].ToString();
                            model.PEAGroupID = reader["PEAGroupID"].ToString();
                            model.PEAGroup = reader["PEAGroup"].ToString();
                            model.CostCenterID = reader["CostCenterID"].ToString();
                            model.CostCenterCode = reader["CostCenterCode"].ToString();
                            model.CostCenterName = reader["CostCenterName"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.TopicName = reader["TopicName"].ToString();
                            model.Cri_work = Regex.Replace(reader["Cri_work"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_Unit = Regex.Replace(reader["Cri_Unit"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv1 = Regex.Replace(reader["Cri_lv1"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv2 = Regex.Replace(reader["Cri_lv2"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv3 = Regex.Replace(reader["Cri_lv3"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv4 = Regex.Replace(reader["Cri_lv4"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv5 = Regex.Replace(reader["Cri_lv5"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_res = Regex.Replace(reader["Cri_res"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.IsAllTheSame = reader["IsAllTheSame"].ToString();
                            model.IsEstimate = reader["IsEstimate"].ToString();
                            model.Weight = reader["Weight"].ToString();
                            model.CreateBy = reader["CreateBy"].ToString();
                            model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                            model.DeleteBy = reader["DeleteBy"].ToString();
                            model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                            model.Active = reader["Active"].ToString();

                            PMSResultModel score = new PMSResultModel();
                            score.ResultID = reader["ResultID"].ToString();
                            score.ForeignSection = reader["ForeignSection"].ToString();
                            score.TRSG = reader["TRSG"].ToString();
                            score.OFFICE = reader["OFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.BA = reader["BA"].ToString();
                            score.Levels = reader["Levels"].ToString();
                            score.Score = reader["Score"].ToString();
                            score.PredictLevels = reader["PredictLevels"].ToString();
                            score.PredictScore = reader["PredictScore"].ToString();
                            score.FileName_CSD = reader["FileName_CSD"].ToString();
                            score.FileName_Score = reader["FileName_Score"].ToString();
                            score.FileName_Predict = reader["FileName_Predict"].ToString();
                            score.UpdateName_CSD = reader["UpdateName_CSD"].ToString();
                            score.UpdateBy_CSD = reader["UpdateBy_CSD"].ToString();
                            score.UpdateDateTime_CSD = Mapper.MapStringToDateTime(reader["UpdateDateTime_CSD"].ToString());
                            score.UpdateName = reader["UpdateName"].ToString();
                            score.UpdateBy = reader["UpdateBy"].ToString();
                            score.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            score.viewUpdateDateTime = score.UpdateDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                            score.viewUpdateDateTime_CSD = score.UpdateDateTime_CSD.ToString("dd/MM/yyyy HH:mm:ss");

                            model.PEA.Add(score);
                        }
                        else // หัวข้อเดิม แต่เปลี่ยน การไฟฟ้า
                        {
                            PMSResultModel score = new PMSResultModel();
                            score.ResultID = reader["ResultID"].ToString();
                            score.ForeignSection = reader["ForeignSection"].ToString();
                            score.TRSG = reader["TRSG"].ToString();
                            score.OFFICE = reader["OFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.BA = reader["BA"].ToString();
                            score.Levels = reader["Levels"].ToString();
                            score.Score = reader["Score"].ToString();
                            score.PredictLevels = reader["PredictLevels"].ToString();
                            score.PredictScore = reader["PredictScore"].ToString();
                            score.FileName_CSD = reader["FileName_CSD"].ToString();
                            score.FileName_Score = reader["FileName_Score"].ToString();
                            score.FileName_Predict = reader["FileName_Predict"].ToString();
                            score.UpdateName_CSD = reader["UpdateName_CSD"].ToString();
                            score.UpdateBy_CSD = reader["UpdateBy_CSD"].ToString();
                            score.UpdateDateTime_CSD = Mapper.MapStringToDateTime(reader["UpdateDateTime_CSD"].ToString());
                            score.UpdateName = reader["UpdateName"].ToString();
                            score.UpdateBy = reader["UpdateBy"].ToString();
                            score.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            score.viewUpdateDateTime = score.UpdateDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                            score.viewUpdateDateTime_CSD = score.UpdateDateTime_CSD.ToString("dd/MM/yyyy HH:mm:ss");

                            model.PEA.Add(score);
                        }
                    }
                    result.Add(model);
                    return (result, "1", "ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<PMSRawDataSectionModel>(), "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (List<PMSRawDataSectionModel>, String Status, String Details) zPMS_spGetRawDataSection_By_TRSG(
            String Year = null, String PEAGroup = null,
            String NameTimesAYear = null, String Month = null, String TRSG = null)
        {
            List<PMSRawDataSectionModel> result = new List<PMSRawDataSectionModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetRawDataSection_By_TRSG]", connection);
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = PEAGroup;
                command.Parameters.Add("NameTimesAYear", SqlDbType.NVarChar).Value = NameTimesAYear;
                command.Parameters.Add("Month", SqlDbType.NVarChar).Value = Month;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = TRSG;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    int Start = 0;
                    String KeyID = "";
                    PMSRawDataSectionModel model = new PMSRawDataSectionModel();
                    while (reader.Read())
                    {
                        if (Start == 0) //หัวข้อแรก
                        {
                            Start++;
                            KeyID = reader["ID"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.GroupSection = reader["GroupSection"].ToString();
                            model.MastID = reader["MastID"].ToString();
                            model.Running = reader["Running"].ToString();
                            model.NameTimesAYearID = reader["NameTimesAYearID"].ToString();
                            model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                            model.TimesAYear = reader["TimesAYear"].ToString();
                            model.Year = reader["Year"].ToString();
                            model.Month = reader["Month"].ToString();
                            model.PEAGroupID = reader["PEAGroupID"].ToString();
                            model.PEAGroup = reader["PEAGroup"].ToString();
                            model.CostCenterID = reader["CostCenterID"].ToString();
                            model.CostCenterCode = reader["CostCenterCode"].ToString();
                            model.CostCenterName = reader["CostCenterName"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.TopicName = reader["TopicName"].ToString();
                            model.Cri_work = Regex.Replace(reader["Cri_work"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_Unit = Regex.Replace(reader["Cri_Unit"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv1 = Regex.Replace(reader["Cri_lv1"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv2 = Regex.Replace(reader["Cri_lv2"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv3 = Regex.Replace(reader["Cri_lv3"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv4 = Regex.Replace(reader["Cri_lv4"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv5 = Regex.Replace(reader["Cri_lv5"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_res = Regex.Replace(reader["Cri_res"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.IsAllTheSame = reader["IsAllTheSame"].ToString();
                            model.IsEstimate = reader["IsEstimate"].ToString();
                            model.Weight = reader["Weight"].ToString();
                            model.CreateBy = reader["CreateBy"].ToString();
                            model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                            model.DeleteBy = reader["DeleteBy"].ToString();
                            model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                            model.Active = reader["Active"].ToString();

                            PMSResultModel score = new PMSResultModel();
                            score.ResultID = reader["ResultID"].ToString();
                            score.ForeignSection = reader["ForeignSection"].ToString();
                            score.TRSG = reader["TRSG"].ToString();
                            score.OFFICE = reader["OFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.BA = reader["BA"].ToString();
                            score.Levels = reader["Levels"].ToString();
                            score.Score = reader["Score"].ToString();
                            score.PredictLevels = reader["PredictLevels"].ToString();
                            score.PredictScore = reader["PredictScore"].ToString();
                            score.FileName_CSD = reader["FileName_CSD"].ToString();
                            score.FileName_Score = reader["FileName_Score"].ToString();
                            score.FileName_Predict = reader["FileName_Predict"].ToString();
                            score.UpdateName_CSD = reader["UpdateName_CSD"].ToString();
                            score.UpdateBy_CSD = reader["UpdateBy_CSD"].ToString();
                            score.UpdateDateTime_CSD = Mapper.MapStringToDateTime(reader["UpdateDateTime_CSD"].ToString());
                            score.UpdateName = reader["UpdateName"].ToString();
                            score.UpdateBy = reader["UpdateBy"].ToString();
                            score.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            score.viewUpdateDateTime = score.UpdateDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                            score.viewUpdateDateTime_CSD = score.UpdateDateTime_CSD.ToString("dd/MM/yyyy HH:mm:ss");

                            model.PEA.Add(score);
                        }
                        else if (KeyID != reader["ID"].ToString()) // ขึ้นหัวข้อใหม่
                        {
                            result.Add(model); // เก็บข้อมูลเก่าก่อน
                            model = new PMSRawDataSectionModel(); // reset ข้อมูลเก่า
                            KeyID = reader["ID"].ToString();
                            model.ID = reader["ID"].ToString();
                            model.GroupSection = reader["GroupSection"].ToString();
                            model.MastID = reader["MastID"].ToString();
                            model.Running = reader["Running"].ToString();
                            model.NameTimesAYearID = reader["NameTimesAYearID"].ToString();
                            model.NameTimesAYear = reader["NameTimesAYear"].ToString();
                            model.TimesAYear = reader["TimesAYear"].ToString();
                            model.Year = reader["Year"].ToString();
                            model.Month = reader["Month"].ToString();
                            model.PEAGroupID = reader["PEAGroupID"].ToString();
                            model.PEAGroup = reader["PEAGroup"].ToString();
                            model.CostCenterID = reader["CostCenterID"].ToString();
                            model.CostCenterCode = reader["CostCenterCode"].ToString();
                            model.CostCenterName = reader["CostCenterName"].ToString();
                            model.TopicID = reader["TopicID"].ToString();
                            model.TopicName = reader["TopicName"].ToString();
                            model.Cri_work = Regex.Replace(reader["Cri_work"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_Unit = Regex.Replace(reader["Cri_Unit"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv1 = Regex.Replace(reader["Cri_lv1"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv2 = Regex.Replace(reader["Cri_lv2"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv3 = Regex.Replace(reader["Cri_lv3"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv4 = Regex.Replace(reader["Cri_lv4"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_lv5 = Regex.Replace(reader["Cri_lv5"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.Cri_res = Regex.Replace(reader["Cri_res"].ToString(), @"\n|\r", "").Replace(@"'", @"\").Replace(@"""", @"");
                            model.IsAllTheSame = reader["IsAllTheSame"].ToString();
                            model.IsEstimate = reader["IsEstimate"].ToString();
                            model.Weight = reader["Weight"].ToString();
                            model.CreateBy = reader["CreateBy"].ToString();
                            model.CreateDateTime = Mapper.MapStringToDateTime(reader["CreateDateTime"].ToString());
                            model.DeleteBy = reader["DeleteBy"].ToString();
                            model.DeleteDateTime = Mapper.MapStringToDateTime(reader["DeleteDateTime"].ToString());
                            model.Active = reader["Active"].ToString();

                            PMSResultModel score = new PMSResultModel();
                            score.ResultID = reader["ResultID"].ToString();
                            score.ForeignSection = reader["ForeignSection"].ToString();
                            score.TRSG = reader["TRSG"].ToString();
                            score.OFFICE = reader["OFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.BA = reader["BA"].ToString();
                            score.Levels = reader["Levels"].ToString();
                            score.Score = reader["Score"].ToString();
                            score.PredictLevels = reader["PredictLevels"].ToString();
                            score.PredictScore = reader["PredictScore"].ToString();
                            score.FileName_CSD = reader["FileName_CSD"].ToString();
                            score.FileName_Score = reader["FileName_Score"].ToString();
                            score.FileName_Predict = reader["FileName_Predict"].ToString();
                            score.UpdateName_CSD = reader["UpdateName_CSD"].ToString();
                            score.UpdateBy_CSD = reader["UpdateBy_CSD"].ToString();
                            score.UpdateDateTime_CSD = Mapper.MapStringToDateTime(reader["UpdateDateTime_CSD"].ToString());
                            score.UpdateName = reader["UpdateName"].ToString();
                            score.UpdateBy = reader["UpdateBy"].ToString();
                            score.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            score.viewUpdateDateTime = score.UpdateDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                            score.viewUpdateDateTime_CSD = score.UpdateDateTime_CSD.ToString("dd/MM/yyyy HH:mm:ss");

                            model.PEA.Add(score);
                        }
                        else // หัวข้อเดิม แต่เปลี่ยน การไฟฟ้า
                        {
                            PMSResultModel score = new PMSResultModel();
                            score.ResultID = reader["ResultID"].ToString();
                            score.ForeignSection = reader["ForeignSection"].ToString();
                            score.TRSG = reader["TRSG"].ToString();
                            score.OFFICE = reader["OFFICE"].ToString();
                            score.PEA_OfficeNAME = reader["PEA_OfficeNAME"].ToString();
                            score.BA = reader["BA"].ToString();
                            score.Levels = reader["Levels"].ToString();
                            score.Score = reader["Score"].ToString();
                            score.PredictLevels = reader["PredictLevels"].ToString();
                            score.PredictScore = reader["PredictScore"].ToString();
                            score.FileName_CSD = reader["FileName_CSD"].ToString();
                            score.FileName_Score = reader["FileName_Score"].ToString();
                            score.FileName_Predict = reader["FileName_Predict"].ToString();
                            score.UpdateName_CSD = reader["UpdateName_CSD"].ToString();
                            score.UpdateBy_CSD = reader["UpdateBy_CSD"].ToString();
                            score.UpdateDateTime_CSD = Mapper.MapStringToDateTime(reader["UpdateDateTime_CSD"].ToString());
                            score.UpdateName = reader["UpdateName"].ToString();
                            score.UpdateBy = reader["UpdateBy"].ToString();
                            score.UpdateDateTime = Mapper.MapStringToDateTime(reader["UpdateDateTime"].ToString());

                            score.viewUpdateDateTime = score.UpdateDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                            score.viewUpdateDateTime_CSD = score.UpdateDateTime_CSD.ToString("dd/MM/yyyy HH:mm:ss");

                            model.PEA.Add(score);
                        }
                    }
                    result.Add(model);
                    return (result, "1", "ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<PMSRawDataSectionModel>(), "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (List<PMSFilesModel>, String, String) zPMS_spGetDownloadFiles_ByID(String ID)
        {
            List<PMSFilesModel> result = new List<PMSFilesModel>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetDownloadFiles_ByID]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        PMSFilesModel model = new PMSFilesModel();
                        model.TopicName = reader["TopicName"].ToString();
                        model.Cri_work = reader["Cri_work"].ToString();
                        model.Cri_Unit = reader["Cri_Unit"].ToString();
                        model.Cri_lv1 = reader["Cri_lv1"].ToString();
                        model.Cri_lv2 = reader["Cri_lv2"].ToString();
                        model.Cri_lv3 = reader["Cri_lv3"].ToString();
                        model.Cri_lv4 = reader["Cri_lv4"].ToString();
                        model.Cri_lv5 = reader["Cri_lv5"].ToString();
                        model.Cri_res = reader["Cri_res"].ToString();
                        model.TRSG = reader["TRSG"].ToString();
                        model.PEAName = reader["PEA_OfficeNAME"].ToString();

                        result.Add(model);
                    }
                    return (result, "1", "สำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (new List<PMSFilesModel>(), "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String, String, String) zPMS_spGetCheckCostCenter(String ID)
        {
            String CostCenterCode = "",CostCenterName = "", Status = "1", Message = "";
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetCheckCostCenter]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        CostCenterCode = reader["CostCenterCode"].ToString();
                        CostCenterName = reader["CostCenterName"].ToString();
                        Status = "1";
                        Message = "ค้นหาข้อมูลสำเร็จ";
                    }
                    return (CostCenterCode, CostCenterName, Status,Message);
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("","","0",e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (viewChartModel, String, String) zPMS_spGetResult_viewChart(String ID, String Year, String Month, String PEAGroup)
        {
            viewChartModel model = new viewChartModel();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spGetResult_viewChart]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("Year", SqlDbType.NVarChar).Value = Year;
                command.Parameters.Add("Month", SqlDbType.NVarChar).Value = Month;
                command.Parameters.Add("PEAGroup", SqlDbType.NVarChar).Value = PEAGroup;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        model.Cri_Bi = reader["Cri_Bi"].ToString();
                        if (reader["Levels"].ToString() != "N/A")
                        {
                            model.TRSG.Add(reader["TRSG"].ToString());
                            model.PEAName.Add(reader["PEA_OfficeNAME"].ToString());
                            model.Score.Add(reader["Score"].ToString());
                            model.Levels.Add(reader["Levels"].ToString());
                            double checkLevels = (double)Mapper.MapStringToDouble(reader["Levels"].ToString());
                            if (checkLevels >= 5) // 5.00 >> green
                            {
                                model.backgroundColor.Add("rgba(154, 220, 45, 0.44)");
                            }
                            else if(checkLevels >= 3.00 && checkLevels <= 4.99) // 3.00 - 4.99 yellow 
                            {
                                model.backgroundColor.Add("rgba(255, 220, 45, 0.44)");
                            }
                            else // below 3 red
                            {
                                model.backgroundColor.Add("rgba(240, 61, 67, 0.8)");
                            }
                        }
                    }
                    return (model,"1", "ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (model, "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (List<String>, String, String) zPMS_KeyForUpdate_CSD_FileName_1_Get(String ID)
        {
            List<String> model = new List<String>();
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_KeyForUpdate_CSD_FileName_1_Get]", connection);
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        model.Add(reader["ID"].ToString());
                    }
                    return (model, "1", "ดึงข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return (model, "0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }

        //Update
        public static (String, String) zPMS_KeyForUpdate_CSD_FileName_2_Update(String ID, String FileName_CSD)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_KeyForUpdate_CSD_FileName_2_Update]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = ID;
                command.Parameters.Add("FileName_CSD", SqlDbType.NVarChar).Value = FileName_CSD;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) zPMS_spUpdateResult_Assistant_CSD_Only(PMSUpdateAssistantModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spUpdateResult_Assistant_CSD_Only]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("FileName_CSD", SqlDbType.NVarChar).Value = model.FileName_CSD;
                command.Parameters.Add("UpdateName_CSD", SqlDbType.NVarChar).Value = model.UpdateName_CSD;
                command.Parameters.Add("UpdateBy_CSD", SqlDbType.NVarChar).Value = model.UpdateBy_CSD;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) zPMS_spUpdateResult_Assistant_CSD_ALL(PMSUpdateAssistantModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spUpdateResult_Assistant_CSD_ALL]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("Levels", SqlDbType.NVarChar).Value = model.Levels;
                command.Parameters.Add("Score", SqlDbType.NVarChar).Value = model.Score;
                command.Parameters.Add("PredictLevels", SqlDbType.NVarChar).Value = model.PredictLevels;
                command.Parameters.Add("PredictScore", SqlDbType.NVarChar).Value = model.PredictScore;
                command.Parameters.Add("FileName_CSD", SqlDbType.NVarChar).Value = model.FileName_CSD;
                command.Parameters.Add("FileName_Score", SqlDbType.NVarChar).Value = model.FileName_Score;
                command.Parameters.Add("FileName_Predict", SqlDbType.NVarChar).Value = model.FileName_Predict;
                command.Parameters.Add("UpdateName_CSD", SqlDbType.NVarChar).Value = model.UpdateName_CSD;
                command.Parameters.Add("UpdateBy_CSD", SqlDbType.NVarChar).Value = model.UpdateBy_CSD;
                command.Parameters.Add("UpdateName", SqlDbType.NVarChar).Value = model.UpdateName;
                command.Parameters.Add("UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) zPMS_spUpdateResult_Assistant_Assessor(PMSUpdateAssistantModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spUpdateResult_Assistant_Assessor]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ID;
                command.Parameters.Add("Levels", SqlDbType.NVarChar).Value = model.Levels;
                command.Parameters.Add("Score", SqlDbType.NVarChar).Value = model.Score;
                command.Parameters.Add("PredictLevels", SqlDbType.NVarChar).Value = model.PredictLevels;
                command.Parameters.Add("PredictScore", SqlDbType.NVarChar).Value = model.PredictScore;
                command.Parameters.Add("FileName_Score", SqlDbType.NVarChar).Value = model.FileName_Score;
                command.Parameters.Add("FileName_Predict", SqlDbType.NVarChar).Value = model.FileName_Predict;
                command.Parameters.Add("UpdateName", SqlDbType.NVarChar).Value = model.UpdateName;
                command.Parameters.Add("UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                try
                {
                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
        public static (String, String) zPMS_spUpdateResult_By_UploadFiles(UploadFilesModel model)
        {
            using (SqlConnection connection = new SqlConnection(ConnString.ConnectionString))
            {
                SqlCommand command = new SqlCommand(@"dbo.[zPMS_spUpdateResult_By_UploadFiles]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("ID", SqlDbType.NVarChar).Value = model.ForeignSection;
                command.Parameters.Add("TRSG", SqlDbType.NVarChar).Value = model.TRSG;
                command.Parameters.Add("Score", SqlDbType.NVarChar).Value = model.Score;
                command.Parameters.Add("Levels", SqlDbType.NVarChar).Value = model.Levels;
                command.Parameters.Add("UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                command.Parameters.Add("UpdateName", SqlDbType.NVarChar).Value = model.UpdateName;

                try
                {
                    command.Connection.Open();
                    IDataReader reader = (command.ExecuteReader());
                    while (reader.Read())
                    {
                        String status = reader["rowUpdate"].ToString();
                        String message = "อัปเดตสำเร็จ";
                        if (status == "0")
                        {
                            message = "อัพเดทไม่สำเร็จโปรดเช็คข้อมูลให้ถูกต้อง";
                        }
                        return (reader["rowUpdate"].ToString(), message);
                    }

                    command.Connection.Open();
                    command.Transaction = connection.BeginTransaction();
                    int Count = command.ExecuteNonQuery();
                    command.Transaction.Commit();
                    return ("1", "แก้ไขข้อมูลสำเร็จ");
                }
                catch (Exception e)
                {
                    // TODO : Call Logger for Log Error Here
                    return ("0", e.Message);
                }
                finally
                {
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
            }
        }
    }
}