﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ReportProblemModel
    {
        public String Name { get; set; }
        public String EmployeeID { get; set; }
        public String BAName { get; set; }
        public String Department { get; set; }
        public String Tel { get; set; }
        public String Title { get; set; }
        public String TitleText { get; set; }
        public String Details { get; set; }
        public DateTime DateTime { get; set; }
    }
}