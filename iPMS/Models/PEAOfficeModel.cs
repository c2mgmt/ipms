﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class PEAOfficeModel
    {
        public int ID { get; set; }
        public int IDPEA { get; set; }
        public String TRSG { get; set; }
        public String OFFICE { get; set; }
        public String PEA_OfficeNAME { get; set; }
        public String BA { get; set; }

    }
}