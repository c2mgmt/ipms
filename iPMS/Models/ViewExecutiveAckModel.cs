﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewExecutiveAckModel
    {
        public List<SectionYearModel> YearsModelLists { get; set; }
        public List<TimesAYearsModel> TimesAYearsModelLists { get; set; }
        public List<MgtDirectorModel> DirectorModelLists { get; set; }
        public List<MgtConfirmModel> PEAModelLists { get; set; }
    }
}