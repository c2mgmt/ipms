﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Web;

namespace iPMS.Models.HrPlatform
{
    public class EmployeeModel
    {
        public class HRServicesResult
        {
            [JsonPropertyName("data")]
            public HRServicesRootResult data { get; set; }
        }
        public class HRServicesRootResult
        {
            [JsonPropertyName("dataDetail")]
            public dataInner[] dataDetail { get; set; }
            public String ServiceStatus { get; set; }
            public String ServiceMessage { get; set; }
        }
        public class dataInner
        {
            public String emp_id { get; set; }
            public String title_s_desc { get; set; }
            public String first_name { get; set; }
            public String last_name { get; set; }
            public String eng_name_full { get; set; }
            public String sex { get; set; }
            public String sex_desc { get; set; }
            public String posi_code { get; set; }
            public String posi_text_short { get; set; }
            public String posi_text { get; set; }
            public String level_code { get; set; }
            public String level_desc { get; set; }
            public String plans_code { get; set; }
            public String plans_text_short { get; set; }
            public String plans_text_full { get; set; }
            public String posi_status { get; set; }
            public String posi_status_desc { get; set; }
            public String dept_stable_code { get; set; }
            public String dept_change_code { get; set; }
            public String dept_short { get; set; }
            public String dept_full { get; set; }
            public String dept_sap { get; set; }
            public String dept_sap_short { get; set; }
            public String dept_sap_full { get; set; }
            public String dept_upper { get; set; }
            public String region { get; set; }
            public String region_name { get; set; }
            public String sub_region { get; set; }
            public String sub_region_name { get; set; }
            public String cost_center { get; set; }
            public String cost_center_name { get; set; }
            public String pea_code { get; set; }
            public String business_area { get; set; }
            public String business_area_name { get; set; }
            public String payroll_area { get; set; }
            public String payroll_area_name { get; set; }
            public String vendor_code { get; set; }
            public String resource_code { get; set; }
            public String resource_name { get; set; }
            public String stell_code { get; set; }
            public String stell_text_short { get; set; }
            public String stell_text_full { get; set; }
            public String qualification { get; set; }
            public String qualification_desc { get; set; }
            public String qualification_level { get; set; }
            public String qualification_level_desc { get; set; }
            public String qualification_dept { get; set; }
            public String qualification_dept_desc { get; set; }
            public String entry_date { get; set; }
            public String staff_date { get; set; }
            public String posi_date { get; set; }
            public String retired { get; set; }
            public String retired2_date { get; set; }
            public String email { get; set; }
            public String tel_mobile { get; set; }
            public String location_building { get; set; }
            public String location_floor { get; set; }
            public String tel_org_ext { get; set; }
            public String age { get; set; }
            public String sap_update_date { get; set; }
            public String updated_date { get; set; }
        }
    }
}