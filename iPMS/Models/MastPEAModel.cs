﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class MastPEAModel
    {
        public String ID { get; set; }
        public String IDPEA { get; set; }
        public String PEAGroup { get; set; }
    }
}