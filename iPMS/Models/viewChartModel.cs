﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class viewChartModel
    {
        public String Cri_Bi { get; set; }
        public List<String> TRSG { get; set; } = new List<String>();
        public List<String> PEAName { get; set; } = new List<String>();
        public List<String> Levels { get; set; } = new List<String>();
        public List<String> Score { get; set; } = new List<String>();
        public List<String> backgroundColor { get; set; } = new List<String>();
    }
}