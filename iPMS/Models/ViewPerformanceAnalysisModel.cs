﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewPerformanceAnalysisModel
    {
        public List<RawDataArchiveModel> ArchiveModelLists { get; set; }
        public List<RawDataArchiveModel> OverviewArchiveModelLists { get; set; }
        public List<RawDataArchiveModel> PEAArchiveModelLists { get; set; }
        public List<YearsModel> YearsModelLists { get; set; }
    }
}