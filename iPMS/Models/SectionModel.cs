﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class SectionModel
    {
        public String ID { get; set; }
        public String GroupSection { get; set; }
        public String MastID { get; set; }
        public Double? Running { get; set; }
        public String Year { get; set; }
        public String PEAGroup { get; set; }
        public List<String> GroupPEAGroup { get; set; }
        public int SegmentID { get; set; }
        public Double? NameTimesAYear { get; set; }
        public int Round { get; set; }
        public String CostCenterID { get; set; }
        public String TypeID { get; set; }
        public String TopicID { get; set; }
        public String Plans { get; set; }
        public String SectionName { get; set; }
        public String Target { get; set; }
        public String TargetFile { get; set; }
        public String Define { get; set; }
        public String Frequency { get; set; }
        public String CuttingCycle { get; set; }
        public String AssessorDepartment { get; set; }
        public String AssessedDivision { get; set; }
        public String AssessedPEA { get; set; }
        public String ModeScore { get; set; }
        public int Openscore { get; set; }
        public String OpenscoreBy { get; set; }
        public String CreateBy { get; set; }
        public DateTime CreateDateTime { get; set; }
        public String UpdateBy { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public String DeleteBy { get; set; }
        public DateTime DeleteDateTime { get; set; }
        public String Cancel { get; set; }
    }
}