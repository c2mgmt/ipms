﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class TypeModel
    {
        public int ID { get; set; }
        public String TypeName { get; set; }
    }
}