﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewAdmin
    {
        public List<PermissionModel> PermissionAdminModelLists { get; set; }
        public List<PermissionRoleModel> PermissionRoleModelLists { get; set; }
        public List<PMSPermissionRoleModel> PMSPermissionRoleModelLists { get; set; }
    }
}