﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models.SessionModel
{
    public class UserProfileSessionData
    {
		public string Username{ get; set; }
		public string EmployeeId { get; set; }
		public string TitleFullName { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string GroupId { get; set; }
		public string Group { get; set; }
		public string GenderCode { get; set; }
		public string Gender { get; set; }
		public string PositionCode { get; set; }
		public string PositionDescShort { get; set; }
		public string Position { get; set; }
		public string LevelCode { get; set; }
		public string LevelDesc { get; set; }
		public string NewOrganizationalCode { get; set; }
		public string DepartmentShortName { get; set; }
		public string DepartmentFullName { get; set; }
		public string DepartmentSap { get; set; }
		public string DepartmentShort { get; set; }
		public string Department { get; set; }
		public string RegionCode { get; set; }
		public string Region { get; set; }
		public string SubRegionCode { get; set; }
		public string SubRegion { get; set; }
		public string CostCenterCode { get; set; }
		public string CostCenterName { get; set; }
		public string Peacode { get; set; }
		public string BaCode { get; set; }
		public string BaName { get; set; }
		public string Email { get; set; }

		public string Description { get; set; }
		public string PMSDescription { get; set; }
		public string IsAdmin { get; set; }
		public string PermissionCode { get; set; }
		public string PMSPermissionCode { get; set; }
		public string Mast_TRSG { get; set; }
		public string IsLogin { get; set; }

		public String TRSG { get; set; }
		public String IDPEA { get; set; }
	}
}