﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class PMSUpdateAssistantModel
    {
        public String ID { get; set; }
        public String TRSG { get; set; }
        public String Score { get; set; }
        public String Levels { get; set; }
        public String PredictScore { get; set; }
        public String PredictLevels { get; set; }
        public String FileName_CSD { get; set; }
        public String FileName_Score { get; set; }
        public String FileName_Predict { get; set; }
        public String UpdateName_CSD { get; set; }
        public String UpdateBy_CSD { get; set; }
        public String UpdateName { get; set; }
        public String UpdateBy { get; set; }

        public String isCSD { get; set; }
        public String isScore { get; set; }
        public String isPredict { get; set; }
        public String checkCSD { get; set; }
        public String checkScore { get; set; }
        public String checkPredict { get; set; }

        public HttpPostedFileBase PostedFileCSD { get; set; }
        public HttpPostedFileBase PostedFileScore { get; set; }
        public HttpPostedFileBase PostedFilePredict { get; set; }
    }
}