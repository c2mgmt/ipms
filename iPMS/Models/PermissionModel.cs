﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class PermissionModel
    {
        public String Id { get; set; }
        public String UserId { get; set; }
        public String TitleName { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String BA { get; set; }
        public String CostCenterCode { get; set; }
        public String PermissionCode { get; set; }
        public String PMSPermissionCode { get; set; }
        public DateTime CreateDateTime { get; set; }
        public String CreateBy { get; set; }
        public DateTime DeleteDateTime { get; set; }
        public String UpdateBy { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public String DeleteBy { get; set; }
        public String Cancel { get; set; }
        public String Description { get; set; }
        public String PMSDescription { get; set; }
    }
}