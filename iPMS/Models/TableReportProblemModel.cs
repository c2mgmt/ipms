﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class TableReportProblemModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string EmployeeID { get; set; }
        public string BAName { get; set; }
        public string Department { get; set; }
        public string Tel { get; set; }
        public string Title { get; set; }
        public string TitleDesc { get; set; }
        public string Details { get; set; }
        public DateTime DateTime { get; set; }
        public string ReportProblemDetailsID { get; set; }
        public string Status { get; set; }
        public string IsTransfer { get; set; }
        public string Describetion { get; set; }
        public string Complete { get; set; }
        public string Note { get; set; }
        public string Responsible { get; set; }
        public DateTime ReportProblemDetailsDateTime { get; set; }

    }
}