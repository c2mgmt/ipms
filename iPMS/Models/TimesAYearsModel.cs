﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class TimesAYearsModel
    {
        public String ID { get; set; }
        public String NameTimesAYear { get;set;}
        public String TimesAYear { get;set; }
    }
}