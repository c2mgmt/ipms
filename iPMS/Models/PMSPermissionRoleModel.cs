﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class PMSPermissionRoleModel
    {
        public int PermissionCode { get; set; }
        public string Description { get; set; }
    }
}