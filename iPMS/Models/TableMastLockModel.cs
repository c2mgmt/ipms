﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class TableMastLockModel
    {
        public String ID { get; set; }
        public String TypeID { get; set; }
        public String TypeName { get; set; }
        public String TimesAYear { get; set; }
        public String NameTimesAYear { get; set; }
        public String Year { get; set; }
        public String Quarter { get; set; }
        public String CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public String DeleteBy { get; set; }
        public DateTime DeleteDate { get; set; }
        public String Cancel { get; set; }

    }
}