﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class RawDataArchiveModel
    {
        public String ID { get; set; }
        public String TypeID { get; set; }
        public String TypeName { get; set; }
        public String CabinetID { get; set; }
        public String Name { get; set; }
        public String TopicID { get; set; }
        public String TopicName { get; set; }
        public String Year { get; set; }
        public String PEAID { get; set; }
        public String NamePEAID { get; set; }
        public String PEA_OfficeNAME { get; set; }
        public List<ArchiveFilesModel> store { get; set; } = new List<ArchiveFilesModel>();

    }
}