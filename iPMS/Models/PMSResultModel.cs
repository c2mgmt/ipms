﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class PMSResultModel
    {
        public String ResultID { get; set; }
        public String ForeignSection { get; set; }
        public String TRSG { get; set; }
        public String OFFICE { get; set; }
        public String PEA_OfficeNAME { get; set; }
        public String BA { get; set; }
        public String Levels { get; set; }
        public String Score { get; set; }
        public String PredictLevels { get; set; }
        public String PredictScore { get; set; }
        public String FileName_CSD { get; set; }
        public String FileName_Score { get; set; }
        public String FileName_Predict { get; set; }
        public String UpdateName_CSD { get; set; }
        public String UpdateBy_CSD { get; set; }
        public DateTime UpdateDateTime_CSD { get; set; }
        public String UpdateName { get; set; }
        public String UpdateBy { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public String DeleteBy { get; set; }
        public DateTime DeleteDateTime { get; set; }
        public String Active { get; set; }

        public String viewUpdateDateTime { get; set; }
        public String viewUpdateDateTime_CSD { get; set; }
    }
}