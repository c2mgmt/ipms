﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewArchiveDataManagement
    {
        public List<ArchiveModel> ArchiveModelLists { get; set; }
        public List<TypeModel> TypeModelLists { get; set; }
        public List<CabinetCategoryModel> CabinetCategoryModelLists { get; set; }
        public List<TopicModel> TopicModelLists { get; set; }
        public List<YearsModel> YearsModelLists { get; set; }
        public List<PEAAnalyticsModel> PEAAnalyticsModelLists { get; set; }
        public List<PEAOfficeModel> NameProvincePEAModelLists { get; set; }
        public List<TimesAYearsModel> TimesAYearsModelLists { get; set; }
    }
}