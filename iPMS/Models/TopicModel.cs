﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class TopicModel
    {
        public String ID { get; set; }
        public int ForeignKeyID { get; set; }
        public String TopicName { get; set; }

    }
}