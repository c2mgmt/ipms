﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class RawDataSectionModel
    {
        public String ID { get; set; }
        public String GroupSection { get; set; }
        public Double? Running { get; set; }
        public String Year { get; set; }
        public String PEAGroup { get; set; }
        public String Segment { get; set; }
        public String SegmentName { get; set; }
        public Double? NameTimesAYear { get; set; }
        public int Round { get; set; }
        public String CostCenterID { get; set; }
        public String TypeID { get; set; }
        public String TopicID { get; set; }
        public String Plans { get; set; }
        public String SectionName { get; set; }
        public String Target { get; set; }
        public String TargetFile { get; set; }
        public String Define { get; set; }
        public String Frequency { get; set; }
        public String CuttingCycle { get; set; }
        public String AssessorDepartment { get; set; }
        public String AssessedDivision { get; set; }
        public String AssessedPEA { get; set; }
        public String ModeScore { get; set; }

        public List<ResultModel> PEA { get; set; } = new List<ResultModel>(); 

    }
}