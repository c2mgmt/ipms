﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class MgtDirectorModel
    {
        public String ID { get; set; }
        public String Year { get; set; }
        public String TimesAYearID { get; set; }
        public String Round { get; set; }
        public String TRSG { get; set; }
        public String Manager { get; set; }
        public DateTime Managertime { get; set; }
        public String Refuse { get; set; }
        public String Message { get; set; }
        public String CostCenterID { get; set; }
        public String CostCenterCode { get; set; }
        public String CostCenterName { get; set; }
    }
}