﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ArchiveModel
    {
        public String ID { get; set; }
        public String TypeID { get; set; }
        public String TypeName { get; set; }
        public String CabinetID { get; set; }
        public String Name { get; set; }
        public String TopicID { get; set; }
        public String TopicName { get; set; }
        public String Year { get; set; }
        public String PEAID { get; set; }
        public String PEAOfficeID { get; set; }
        public String NamePEAID { get; set; }
        public String PEA_OfficeNAME { get; set; }
        public String TimesAYearID { get; set; }
        public String NameTimesAYear { get; set; }
        public String TimesAYear { get; set; }
        public String Quarter { get; set; }
        public String FileName { get; set; }
        public String CreateBy { get; set; }
        public DateTime CreateDateTime { get; set; }
        public String UpdateBy { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public String Cancel { get; set; }

        public HttpPostedFileBase PostedFile { get; set; }
    }
}