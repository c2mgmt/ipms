﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ActivityModel
    {
        public String ID { get; set; }
        public Double? Running { get; set; }
        public String CostCenterID { get; set; }
        public String CostCenterCode { get; set; }
        public String CostCenterName { get; set; }
        public String TypeID { get; set; }
        public String TypeName { get; set; }
        public String TopicID { get; set; }
        public String TopicName { get; set; }
        public String Plans { get; set; }
        public String SectionName { get; set; }
        public String Target { get; set; }
        public String TargetFile { get; set; }
        public String Define { get; set; }
        public String Frequency { get; set; }
        public String CuttingCycle { get; set; }
        public String AssessorDepartment { get; set; }
        public String AssessedDivision { get; set; }
        public String AssessedPEA { get; set; }
        public String CreateBy { get; set; }
        public DateTime CreateDateTime { get; set; }
        public String UpdateBy { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public String DeleteBy { get; set; }
        public DateTime DeleteDateTime { get; set; }
        public String Cancel { get; set; }
        public HttpPostedFileBase PostedFile { get; set; }

    }
}