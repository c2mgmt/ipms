﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewActivityDataManagement
    {
        public List<CostCenterModel> CostCenterModelLists { get; set; }
        public List<TypeModel> TypeModelLists { get; set; }
        public List<TopicModel> TopicModelLists { get; set; }
        public List<ActivityModel> ActivityModelLists { get; set; }
        public List<TimesAYearsModel> TimesAYearsModelLists { get; set; }
        public List<MastPEAModel> MastPEAModelLists { get; set; }
        public List<SegmentModel> MastSegmentModelLists { get; set; }

        public List<YearsModel> YearsModelLists { get; set; }

    }
}