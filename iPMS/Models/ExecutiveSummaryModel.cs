﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ExecutiveSummaryModel
    {
        public String ID { get; set; }
        public String GroupSection { get; set; }
        public Double? Running { get; set; }
        public String Year { get; set; }
        public String PEAGroup { get; set; }
        public String Segment { get; set; }
        public String NameTimesAYear { get; set; }
        public String Round { get; set; }
        public String CostCenterID { get; set; }
        public String TypeID { get; set; }
        public String TopicID { get; set; }
        public String Plans { get; set; }
        public String SectionName { get; set; }
        public String Target { get; set; }
        public String TargetFile { get; set; }
        public String Define { get; set; }
        public String Frequency { get; set; }
        public String CuttingCycle { get; set; }
        public String AssessorDepartment { get; set; }
        public String AssessedDivision { get; set; }
        public String AssessedPEA { get; set; }
        public String ModeScore { get; set; }
        public String Openscore { get; set; }
        public String CreateBy { get; set; }
        public DateTime CreateDateTime { get; set; }
        public String UpdateBy { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public String MastID { get; set; }

        public String ResultID { get; set; }
        public String ForeignSection { get; set; }
        public String TRSG { get; set; }
        public String PerformanceText { get; set; }
        public String PerformanceNum { get; set; }
        public String Result { get; set; }
        public String ResultScore { get; set; }
        public String Report { get; set; }
        public String ReportFile { get; set; }
        public String UpdateAdmin { get; set; }
        public DateTime DatetimeADmin { get; set; }
        public String UpdateUser { get; set; }
        public DateTime DatetimeUser { get; set; }
        public String ResultTarget { get; set; }
        public String PerformanceText2 { get; set; }
        public String UsernameAdmin { get; set; }
        public String UsernameUser { get; set; }
        public String CheckBox { get; set; }
    }
}