﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ReportProblemStatusModel
    {
        public string ID { get; set; }
        public string Describetion { get; set; }
        public string IsTransfer { get; set; }
        public string Complete { get; set; }
    }
}