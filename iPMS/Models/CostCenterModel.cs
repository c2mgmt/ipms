﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class CostCenterModel
    {
        public int ID { get; set; }
        public String CostCenterCode { get; set; }
        public String CostCenterName { get; set; }
        public String TRSG { get; set; }
    }
}