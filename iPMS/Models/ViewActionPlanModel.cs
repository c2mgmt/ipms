﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewActionPlanModel
    {
        public List<ArchiveModel> ArchiveModelLists { get; set; }
        public List<YearsModel> YearsModelLists { get; set; }
    }
}