﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class MgtCountingResultModel
    {
        public String ID { get; set; }
        public String CostCenterName { get; set; }
        public String MastTRSG { get; set; }
        public String BA { get; set; }
        public String TRSG { get; set; }
        public String PEA_OfficeNAME { get; set; }
        public String Counting { get; set; }
        public String ResultNull { get; set; }
        public String Counting99 { get; set; }
        public String Total { get; set; }
    }
}