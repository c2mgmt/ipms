﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class CabinetCategoryModel
    {
        public String ID { get; set; }
        public String ForeignKeyTypeID { get; set; }
        public String Name { get; set; }
        public String FileExtension { get; set; }
        public String Cancel { get; set; }
    }
}