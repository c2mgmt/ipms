﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewReportResults
    {
        public List<SectionYearModel> YearsModelLists { get; set; }
        public List<MastPEAModel> MastPEAModelLists { get; set; }
        public List<TimesAYearsModel> TimesAYearsModelLists { get; set; }

        public List<RawDataSectionModel> RawDataSectionModelLists { get; set; }

    }
}