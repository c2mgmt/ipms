﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class zPMS_ResultModel
    {
        public String ID { get; set; }
        public String ForeignSection { get; set; }
        public String TRSG { get; set; }
        public String Levels { get; set; }
        public String Score { get; set; }
        public String FileName_CSD { get; set; }
        public String FileName_Score { get; set; }
        public String FileName_Predict { get; set; }
        public String UpdateName { get; set; }
        public String UpdateBy { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}