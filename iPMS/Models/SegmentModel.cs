﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class SegmentModel
    {
        public String ID { get; set; }
        public String SegmentID { get; set; }
        public String SegmentName { get; set; }  
    }
}