﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class UploadFilesModel
    {
        public String ForeignSection { get; set; }
        public String TRSG { get; set; }
        public String Score { get; set; }
        public String Levels { get; set; }
        public String UpdateBy { get; set; }
        public String UpdateName { get; set; }
        public HttpPostedFileBase PostedFile { get; set; }


        public String Year { get; set; }
        public String Month { get; set; }
        public String CheckID { get; set; }
    }
}