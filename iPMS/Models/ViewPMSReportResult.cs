﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewPMSReportResult
    {
        public List<SectionYearModel> YearsModelLists { get; set; }
        public List<TimesAYearsModel> TimesAYearsModelLists { get; set; }

        public List<PMSRawDataSectionModel> RawDataSectionModelLists { get; set; }
    }
}