﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ReportProblemSectionModel
    {
        public String ID { get; set; }
        public String Title { get; set; }
        public String Cancel { get; set; }
    }
}