﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ArchiveFilesModel
    {
        public String TimesAYearID { get; set; }
        public String NameTimesAYear { get; set; }
        public String TimesAYear { get; set; }
        public String Quarter { get; set; }
        public String FileName { get; set; }
        public String CreateBy { get; set; }
        public DateTime CreateDateTime { get; set; }
        public String UpdateBy { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}