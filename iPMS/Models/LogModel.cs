﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class LogModel
    {
        public String UserId { get; set; }
        public String ControllerName { get; set; }
        public String FunctionName { get; set; }
        public String Status { get; set; }
        public String Details { get; set; }
        public String ActionBy { get; set; }
        public DateTime ActionDateTime { get; set; }
        public String PermissionCode { get; set; }
        public String PMSPermissionCode { get; set; }
        public LogModel(String UserId, String ControllerName, String FunctionName, String Status, String Details, String ActionBy,String PermissionCode,String PMSPermissionCode)
        {
            this.UserId = UserId;
            this.ControllerName = ControllerName;
            this.FunctionName = FunctionName;
            this.Status = Status;
            this.Details = Details;
            this.ActionBy = ActionBy;
            this.PermissionCode = PermissionCode;
            this.PMSPermissionCode = PMSPermissionCode;
        }
    }
}