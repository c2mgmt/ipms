﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewManagementSystem
    {
        public List<TypeModel> TypeModelLists { get; set; }
        public List<SectionYearModel> YearsModelLists { get; set; }
        public List<TimesAYearsModel> TimesAYearsModelLists { get; set; }
        public List<TableMastLockModel> MastLockModelLists { get; set; }
    }
}