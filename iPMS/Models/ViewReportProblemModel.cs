﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ViewReportProblemModel
    {
        public List<ReportProblemSectionModel> SectionModelLists { get; set; }
        public List<TableReportProblemModel> ReportProblemModelLists { get; set; }
        public List<ReportProblemStatusModel> ReportProblemStatusModelLists { get; set; }

        
    }
}