﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class MgtConfirmModel
    {
        public String ID { get; set; }
        public String Year { get; set; }
        public String TimesAYearID { get; set; }
        public int Round { get; set; }
        public String TRSG { get; set; }
        public String Manager { get; set; }
        public DateTime Managertime { get; set; }
        public String Refuse { get; set; }
        public String Message { get; set; }
        public String PEA_OfficeNAME { get; set; }
        public List<MgtCountingResultModel> score { get; set; } = new List<MgtCountingResultModel>();
    }
}