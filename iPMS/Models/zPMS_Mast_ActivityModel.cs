﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class zPMS_Mast_ActivityModel
    {
        public String ID { get; set; }
        public String CostCenterID { get; set; }
        public String CostCenterName { get; set; }
        public String TopicID { get; set; }
        public String TopicName { get; set; }
        public String Running { get; set; }
        public String Cri_work { get; set; }
        public String Cri_Unit { get; set; }
        public String Cri_lv1 { get; set; }
        public String Cri_lv2 { get; set; }
        public String Cri_lv3 { get; set; }
        public String Cri_lv4 { get; set; }
        public String Cri_lv5 { get; set; }
        public String Cri_res { get; set; }
        public String Cri_Bi { get; set; }
        public String CreateBy { get; set; }
        public DateTime CreateDateTime { get; set; }
        public String UpdateBy { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public String DeleteBy { get; set; }
        public DateTime DeleteDateTime { get; set; }
        public String Active { get; set; }
    }
}