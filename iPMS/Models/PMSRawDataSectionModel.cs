﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class PMSRawDataSectionModel
    {
        public String ID { get;set;}
        public String GroupSection { get; set; }
        public String MastID { get; set; }
        public String Running { get; set; }
        public String NameTimesAYearID { get; set; }
        public String NameTimesAYear { get; set; }
        public String TimesAYear { get; set; }
        public String Year { get; set; }
        public String Month { get; set; }
        public String PEAGroupID { get; set; }
        public String PEAGroup { get; set; }
        public String CostCenterID { get; set; }
        public String CostCenterCode { get; set; }
        public String CostCenterName { get; set; }
        public String TopicID { get; set; }
        public String TopicName { get; set; }
        public String Cri_work { get; set; }
        public String Cri_Unit { get; set; }
        public String Cri_lv1 { get; set; }
        public String Cri_lv2 { get; set; }
        public String Cri_lv3 { get; set; }
        public String Cri_lv4 { get; set; }
        public String Cri_lv5 { get; set; }
        public String IsAllTheSame { get; set; }
        public String Cri_res { get; set; }
        public String IsEstimate { get; set; }
        public String Weight { get; set; }
        public String CreateBy { get; set; }
        public DateTime CreateDateTime { get; set; }
        public String DeleteBy { get; set; }
        public DateTime DeleteDateTime { get; set; }
        public String Active { get; set; }

        public List<PMSResultModel> PEA { get; set; } = new List<PMSResultModel>(); 
    }
}