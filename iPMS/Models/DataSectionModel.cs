﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class DataSectionModel
    {
        public String ID { get; set; }
        public String GroupSection { get; set; }
        public Double? Running { get; set; }
        public String Year { get; set; }
        public String PEAGroup { get; set; }
        public String Segment { get; set; }
        public String SegmentName { get; set; }
        public Double? NameTimesAYear { get; set; }
        public int Round { get; set; }
        public String CostCenterID { get; set; }
        public String TypeID { get; set; }
        public String TopicID { get; set; }
        public String Plans { get; set; }
        public String SectionName { get; set; }
        public String Target { get; set; }
        public String TargetFile { get; set; }
        public String Define { get; set; }
        public String Frequency { get; set; }
        public String CuttingCycle { get; set; }
        public String AssessorDepartment { get; set; }
        public String AssessedDivision { get; set; }
        public String AssessedPEA { get; set; }
        public String ModeScore { get; set; }

        public String ResultID { get; set; }
        public String ForeignSection { get; set; }
        public int ResultRound { get; set; }
        public String TRSG { get; set; }
        public String PerformanceText { get; set; }
        public Double? PerformanceNum { get; set; }
        public String Result { get; set; }
        public Double? ResultScore { get; set; }
        public String Report { get; set; }
        public String ReportFile { get; set; }
        public String UpdateAdmin { get; set; }
        public DateTime DatetimeADmin { get; set; }
        public String UpdateUser { get; set; }
        public DateTime DatetimeUser { get; set; }

        public String CheckBox { get; set; }

        public String Cancel { get; set; }
    }
}