﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class PMSFilesModel
    {
        public String no { get; set; }
        public String TopicName { get; set; }
        public String Cri_work { get; set; }
        public String Cri_Unit { get; set; }
        public String Cri_lv1 { get; set; }
        public String Cri_lv2 { get; set; }
        public String Cri_lv3 { get; set; }
        public String Cri_lv4 { get; set; }
        public String Cri_lv5 { get; set; }
        public String Cri_res { get; set; }
        public String TRSG { get; set; }
        public String PEAName { get; set; }
        public String Score { get; set; }
        public String Level { get; set; }
    }
}