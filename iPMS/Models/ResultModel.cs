﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Models
{
    public class ResultModel
    {
        public String ID { get; set; }
        public String ForeignSection { get; set; }
        public int Round { get; set; }
        public String TRSG { get; set; }
        public String PerformanceText { get; set; }
        public Double? PerformanceNum { get; set; }
        public String Result { get; set; }
        public Double? ResultScore { get; set; }
        public String Report { get; set; }
        public String ReportFile { get; set; }
        public String UpdateAdmin { get; set; }
        public DateTime DatetimeADmin { get; set; }
        public String UpdateUser { get; set; }
        public DateTime DatetimeUser { get; set; }
        public String Cancel { get; set; }
        public String Target { get; set; }
        public String PerformanceText2 { get; set; }
        public String UsernameAdmin { get; set; }
        public String UsernameUser { get; set; }
        public String CheckBox { get; set; }

        public String PIDPEA { get; set; }
        public String POFFICE { get; set; }
        public String PEA_OfficeNAME { get; set; }
        public String PBA { get; set; }

        public String CheckYear { get; set; }
        public String CheckTimesAYear { get; set; }

        public Boolean CheckInsertAll { get; set; }

    }
}