﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace iPMS.Utility
{
    public class Mapper
    {
        public static DateTime MapStringToDateTime(String datetime)
        {
            try
            {
                //string[] formats = { "MM/dd/yyyy HH:mm", "yyyy/MM/dd HH:mm", "dd/MM/yyyy HH:mm" };
                //var myDate = DateTime.ParseExact(datetime, formats, null, DateTimeStyles.None);
                DateTime myDate;
                //myDate = DateTime.ParseExact(datetime, "dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                myDate = DateTime.Parse(datetime);
                return (DateTime)myDate;
            }
            catch (Exception e)
            {
                return DateTime.Now;
            }

        }
        public static DateTime MapStringToDate(String datetime)
        {
            try
            {
                //string[] formats = { "MM/dd/yyyy HH:mm", "yyyy/MM/dd HH:mm", "dd/MM/yyyy HH:mm" };
                //var myDate = DateTime.ParseExact(datetime, formats, null, DateTimeStyles.None);
                DateTime myDate;
                myDate = DateTime.ParseExact(datetime, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                return (DateTime)myDate;
            }
            catch (Exception e)
            {
                return DateTime.Now;
            }

        }
        public static DateTime SelectMapDateTime(String datetime)
        {
            DateTime myDate = DateTime.ParseExact(datetime, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            return myDate;
        }
        public static int MapCalMinutes(DateTime dateStart, DateTime dateEnd)
        {
            try
            {
                TimeSpan ts = dateEnd - dateStart;
                return (int)ts.TotalMinutes;
            }
            catch (Exception e)
            {
                return 0;
            }

        }
        public static int MapStringToInteger(String str)
        {
            try
            {
                return Int32.Parse(str);
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        public static double? MapStringToDouble(String str)
        {
            try
            {
                return Convert.ToDouble(str);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static TimeSpan MapIntegerToTimeSpan(int time)
        {
            try
            {
                return TimeSpan.FromHours(time);
            }
            catch (Exception e)
            {
                return new TimeSpan();
            }

        }
        public static int MapDoubleToInteger(Double? value)
        {
            try
            {
                return Convert.ToInt32(value);
            }
            catch (Exception e)
            {
                return 0;
            }

        }
        public static TimeSpan MapStringToTimeSpan(String time)
        {
            try
            {
                return TimeSpan.Parse(time);
            }
            catch (Exception e)
            {
                return new TimeSpan();
            }

        }
    }
}