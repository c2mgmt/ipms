﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iPMS.DBConfig
{
    public class ConnString
    {
        public static string ConnectionString
        {
            get
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "c2webdb.pea.co.th,59156";
                builder.InitialCatalog = "iPMSDB";
                builder.UserID = "iPMS";
                builder.Password = "iPMS";
                builder.Encrypt = true;
                builder.TrustServerCertificate = true;
                return builder.ConnectionString;
            }
        }
    }
}