﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using static iPMS.Models.KeyCloak.KEYCLOAK;

namespace iPMS.Helper
{
    public class KEYCLOAKServiceHelper
    {
        private string Endpoint = "https://sso.pea.co.th/";

        private string _CallPostGetToken<T>(string code)
        {
            var request = HttpContext.Current.Request;
            var baseUrl = $"{request.Url.Scheme}://{request.Url.Authority}{request.ApplicationPath.TrimEnd('/')}/";
            var returnUrl = baseUrl + "Login/Authentication";
            using (var client = new HttpClient())
            {
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    //string endPoint = "https://sso.pea.co.th/auth/realms/idm/protocol/openid-connect/token";
                    string endPoint = "https://sso2.pea.co.th/realms/pea-users/protocol/openid-connect/token";

                    var data = new[]
                    {
                        new KeyValuePair<string, string>("code", code),
                        new KeyValuePair<string,string>("client_id","c2-ipms"),
                        new KeyValuePair<string,string>("client_secret","nnqjxqfcuT5RDOGLXPvifT0ldviR8M96"),
                        new KeyValuePair<string,string>("grant_type", "authorization_code"),
                        //new KeyValuePair<string, string>("redirect_uri", "https://localhost:44345/Login/Authentication")
                        new KeyValuePair<string, string>("redirect_uri", returnUrl)
                        //new KeyValuePair<string, string>("code", code),
                        //new KeyValuePair<string,string>("client_id","wesafe-app"),
                        //new KeyValuePair<string,string>("client_secret","7230cb26-4355-44f9-a55e-8e3aec084fee"),
                        //new KeyValuePair<string,string>("grant_type", "authorization_code"),
                        //new KeyValuePair<string, string>("redirect_uri", "https://localhost:44369/Auth/Login")
                    };

                    var res = client.PostAsync(endPoint, new FormUrlEncodedContent(data)).GetAwaiter().GetResult();
                    var result = res.Content.ReadAsStringAsync().Result;

                    String response = result.ToString();

                    return response;
                }
            }
        }


        private string _CallPostKeyCloak<T>(string cmd, string authstring)
        {
            using (var client = new HttpClient())
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authstring);
                var postTask = client.GetAsync("https://sso2.pea.co.th/realms/pea-users/protocol/openid-connect/userinfo");
                var result = postTask.Result;
                var res = result.Content.ReadAsStringAsync().Result;

                return res;
            }
        }


        public UserInfo CallSaveKeyRequest(String strToken)
        {
            var Res = JsonConvert.DeserializeObject<UserInfo>(this._CallPostKeyCloak<UserInfo>("auth/realms/idm/protocol/openid-connect/userinfo/", strToken));
            //String user = Res.name;
            return Res;
        }

        public UserInfo CallGetKeyRequest(String code)
        {// ServiceReply < KeyResponse >
            UserInfo user = new UserInfo();
            var Res = JsonConvert.DeserializeObject<KeyResponse>(this._CallPostGetToken<KeyResponse>(code));
            user = CallSaveKeyRequest(Res.access_token);

            return user;
        }

        public KeyResponse CallGetToken(string code)
        {
            var Res = JsonConvert.DeserializeObject<KeyResponse>(this._CallPostGetToken<KeyResponse>(code));
            return Res;
        }

        public UserInfo CallGetUserInfo(string accessToken)
        {
            var Res = JsonConvert.DeserializeObject<UserInfo>(this._CallPostKeyCloak<UserInfo>("auth/realms/idm/protocol/openid-connect/userinfo/", accessToken));
            //String user = Res.name;
            return Res;
        }
    }
}