﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Helper
{
    public sealed class MessageManager
    {
        private static MessageInfo _messageInfo = null;

        public static void SetErrorMessage(string message)
        {
            _messageInfo = new MessageInfo();
            _messageInfo.IsSuccess = false;
            _messageInfo.Message = message;
        }

        public static void SetSuccessMessage(string message)
        {
            _messageInfo = new MessageInfo();
            _messageInfo.IsSuccess = true;
            _messageInfo.Message = message;
        }

        public static MessageInfo GetMessage()
        {
            MessageInfo m = _messageInfo;
            _messageInfo = null;
            return m;
        }
    }

    public class MessageInfo
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}