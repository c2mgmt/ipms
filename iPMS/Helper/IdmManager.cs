﻿using iPMS.ServiceReference1;
using iPMS.ServiceReference2;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iPMS.Helper
{
    public sealed class IdmManager
    {
        private static IdmServicesSoapClient _idmService;

        // PEA Web API Key
        private const string _idmWsAuthenKey = "e3358fc1-99ad-4b21-8237-7c9c8ba1c5dc";

        private static EmployeeServicesSoapClient _empService;

        // PEA Web API Key
        private const string _empWsAuthenKey = "93567815-dfbb-4727-b4da-ce42c046bfca";


        #region Constructor

        //public IdmManager()
        //{
        //    _idmService = new IdmServicesSoapClient();
        //}
        #endregion

        #region GetLoginResult

        public static ServiceResponseOfLoginResult GetLoginResult(string username, string password)
        {
            try
            {
                _idmService = new IdmServicesSoapClient();
                // 1. Call IDM Service to check login result
                ServiceResponseOfLoginResult loginResult;
                ServiceRequestOfLoginCriteria loginCriteria = new ServiceRequestOfLoginCriteria()
                {
                    InputObject = new LoginCriteria()
                    {
                        Username = username,
                        Password = password
                    },
                    WSAuthenKey = _idmWsAuthenKey
                };

                loginResult = _idmService.Login(loginCriteria);


                return loginResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        private void AfterIDMLoginSuccess(string username, string password)
        {
            // 1. Update AuthenInfo to local database
            //using (var autheninfo = new AuthenInfoManager())
            //{
            //    var result = autheninfo.AddOrUpdate(username, password);
            //}
        }

        #endregion

        #region GetUserAccount

        public UserInfoResult GetUserAccount(Guid userid)
        {
            try
            {
                UserInfoResult result;
                ServiceRequestOfUserInfoCriteria criteria = new ServiceRequestOfUserInfoCriteria()
                {
                    InputObject = new UserInfoCriteria()
                    {
                        UserId = userid
                    },
                    WSAuthenKey = _idmWsAuthenKey
                };
                result = _idmService.GetUserAccount(criteria).ResultObject;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region IDisposable

        //private bool disposed = false;

        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        //protected virtual void Dispose(bool disposing)
        //{
        //    // Check to see if Dispose has already been called.
        //    if (!this.disposed)
        //    {
        //        // If disposing equals true, dispose all managed
        //        // and unmanaged resources.
        //        if (disposing)
        //        {
        //            // Dispose managed resources.
        //            _idmService.Close();
        //            _idmService = null;
        //        }

        //        // Call the appropriate methods to clean up
        //        // unmanaged resources here.
        //        // If disposing is false,
        //        // only the following code is executed.

        //        // Note disposing has been done.
        //        disposed = true;

        //    }
        //}
        #endregion

        #region Get by EmployeeNo

        public static EmployeeProfile GetByEmployeeNo(string empno)
        {

            try
            {
                _empService = new EmployeeServicesSoapClient();
                EmployeeProfile emp;
                ServiceRequestOfEmployeeInfoByEmployeeIdCriteria empInfoCriteria = new ServiceRequestOfEmployeeInfoByEmployeeIdCriteria()
                {
                    InputObject = new EmployeeInfoByEmployeeIdCriteria()
                    {
                        EmployeeId = empno
                    },
                    WSAuthenKey = _empWsAuthenKey
                };

                emp = _empService.GetEmployeeInfoByEmployeeId(empInfoCriteria).ResultObject;



                return emp;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}